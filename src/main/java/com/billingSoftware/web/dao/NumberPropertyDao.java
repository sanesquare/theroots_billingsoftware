package com.billingSoftware.web.dao;

import com.billingSoftware.web.entities.NumberProperty;
import com.billingSoftware.web.vo.NumberPropertyVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 9, 2015
 */
public interface NumberPropertyDao {

	/**
	 * method to find number property for given category and company
	 * 
	 * @param category
	 * @param companyId
	 * @return
	 */
	public NumberPropertyVo updateFindNumberProperty(String category);
	/**
	 * method to find number property for given category and company
	 * 
	 * @param category
	 * @param companyId
	 * @return
	 */
	public NumberProperty findNumberPropertyObject(String category);
	
	
	/**
	 * method to increment number property
	 * 
	 * @param category
	 * 
	 * @param companyId
	 */
	public void incrementNumberProperty(String category);
}
