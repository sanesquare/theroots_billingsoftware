package com.billingSoftware.web.dao;

import java.util.List;

import com.billingSoftware.web.vo.SMSHistoryVo;
import com.billingSoftware.web.vo.SMSVo;

/**
 * 
 * @author Suhail
 * @since Jun 17, 2017
 */
public interface SMSDao {

	/**
	 * Method to save sent SMS
	 */
	public void saveSentSMS(SMSVo vo);
	
	/**
	 * Find all sent sms
	 * @return
	 */
	
	public List<SMSHistoryVo> findAllSMShistory();
}
