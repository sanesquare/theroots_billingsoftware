package com.billingSoftware.web.dao.Impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.jdbc.ReturningWork;
import org.springframework.stereotype.Repository;

import com.billingSoftware.web.dao.AbstractDao;
import com.billingSoftware.web.dao.ProductDao;
import com.billingSoftware.web.entities.Product;
import com.billingSoftware.web.execptions.CustomException;
import com.billingSoftware.web.vo.ProductVo;

@Repository
public class ProductDaoImpl extends AbstractDao implements ProductDao {

	public void saveProduct(ProductVo productVo) {
		Product product = this.findProduct(productVo.getId());
		if (product == null)
			product = new Product();
		product = this.setAttributesToProduct(product, productVo);
		this.sessionFactory.getCurrentSession().saveOrUpdate(product);
	}

	private Product setAttributesToProduct(Product product, ProductVo vo) {
		product.setProduct_name(vo.getProductName());
		product.setDescription(vo.getDescription());
		product.setUnit(vo.getUnit());
		product.setRate(vo.getRate());
		return product;
	}

	public void deleteProduct(Long id) {
		Product product = this.findProduct(id);
		if (product == null)
			throw new CustomException("Product Not Found.");
		this.sessionFactory.getCurrentSession().delete(product);
	}

	public Product findProduct(Long id) {
		return (Product) this.sessionFactory.getCurrentSession()
				.createCriteria(Product.class).add(Restrictions.eq("id", id))
				.uniqueResult();
	}

	private ProductVo createProductVo(Product product) {
		ProductVo vo = new ProductVo();
		vo.setUnit(product.getUnit());
		vo.setId(product.getId());
		vo.setProductName(product.getProduct_name());
		vo.setDescription(product.getDescription());
		vo.setRate(product.getRate());
		return vo;
	}

	public ProductVo findProductById(Long id) {
		Product product = this.findProduct(id);
		if (product == null)
			throw new CustomException("Product Not Found.");
		return createProductVo(product);
	}

	public List<ProductVo> findAllProducts() {
		List<ProductVo> productVos = new ArrayList<ProductVo>();
		productVos = this.sessionFactory.getCurrentSession().doReturningWork(
				new ReturningWork<List<ProductVo>>() {

					public List<ProductVo> execute(Connection connection)
							throws SQLException {
						List<ProductVo> productVos = new ArrayList<ProductVo>();
						Statement statement = connection.createStatement();
						String sql = "SELECT id,product_name,rate,reference_number,unit FROM product";
						ResultSet resultSet = statement.executeQuery(sql);
						while (resultSet.next()) {
							ProductVo vo = new ProductVo();
							vo.setId(resultSet.getLong("id"));
							vo.setProductName(resultSet
									.getString("product_name"));
							vo.setRate(resultSet.getBigDecimal("rate"));
							vo.setUnit(resultSet.getString("unit"));
							productVos.add(vo);
						}
						statement.close();
						return productVos;
					}
				});
		return productVos;
	}

	public List<ProductVo> findProducts(final List<Long> ids) {
		List<ProductVo> productVos = new ArrayList<ProductVo>();
		productVos = this.sessionFactory.getCurrentSession().doReturningWork(
				new ReturningWork<List<ProductVo>>() {

					public List<ProductVo> execute(Connection connection)
							throws SQLException {
						List<ProductVo> productVos = new ArrayList<ProductVo>();
						if (ids != null && !ids.isEmpty()) {
							Statement statement = connection.createStatement();
							String sql = "SELECT id,product_name,rate,reference_number,unit FROM product";
							sql += " WHERE id IN("
									+ ids.toString().replace("[", "")
											.replace("]", "") + ")";
							ResultSet resultSet = statement.executeQuery(sql);
							while (resultSet.next()) {
								ProductVo vo = new ProductVo();
								vo.setId(resultSet.getLong("id"));
								vo.setProductName(resultSet
										.getString("product_name"));
								vo.setRate(resultSet.getBigDecimal("rate"));
								vo.setUnit(resultSet.getString("unit"));
								productVos.add(vo);
							}
							statement.close();
						}
						return productVos;
					}
				});
		return productVos;
	}
}
