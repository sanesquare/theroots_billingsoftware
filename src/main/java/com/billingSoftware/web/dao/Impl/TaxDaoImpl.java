package com.billingSoftware.web.dao.Impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.hibernate.jdbc.ReturningWork;
import org.springframework.stereotype.Repository;

import com.billingSoftware.web.dao.AbstractDao;
import com.billingSoftware.web.dao.TaxDao;
import com.billingSoftware.web.entities.Tax;
import com.billingSoftware.web.execptions.CustomException;
import com.billingSoftware.web.vo.TaxVo;

/**
 * @author Shamsheer
 * @since Jun 16, 2017
 */
@Repository
public class TaxDaoImpl extends AbstractDao implements TaxDao {

	public void saveTax(TaxVo vo) {
		Tax tax = null;
		Tax duplicateTax = this.findTaxObject(vo.getTax());
		if (vo.getId() != null)
			tax = this.findTaxObject(vo.getId());
		if (tax == null) {
			if (duplicateTax != null)
				throw new CustomException("Tax " + vo.getTax() + " exists");
			tax = new Tax();
		} else {
			if (duplicateTax != null && !duplicateTax.equals(tax))
				throw new CustomException("Tax " + vo.getTax() + " exists");
		}
		tax.setTax(vo.getTax());
		tax.setPercentage(vo.getPercentage());
		this.sessionFactory.getCurrentSession().save(tax);
	}

	public void deleteTax(Long id) {
		Tax tax = this.findTaxObject(id);
		if (tax == null)
			throw new CustomException("Tax not found");
		this.sessionFactory.getCurrentSession().delete(tax);
	}

	public Tax findTaxObject(Long id) {
		return (Tax) this.sessionFactory.getCurrentSession()
				.createCriteria(Tax.class).add(Restrictions.eq("id", id))
				.uniqueResult();
	}

	public Tax findTaxObject(String tax) {
		return (Tax) this.sessionFactory.getCurrentSession()
				.createCriteria(Tax.class).add(Restrictions.eq("tax", tax))
				.uniqueResult();
	}

	public TaxVo findTax(Long id) {
		Tax tax = this.findTaxObject(id);
		if (tax == null)
			throw new CustomException("Tax not found");
		TaxVo vo = new TaxVo();
		vo.setId(tax.getId());
		vo.setTax(tax.getTax());
		vo.setPercentage(tax.getPercentage());
		return vo;
	}

	public List<TaxVo> findAllTaxes() {
		List<TaxVo> vos = this.sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<List<TaxVo>>() {

					public List<TaxVo> execute(Connection connection)
							throws SQLException {
						Statement statement = connection.createStatement();
						List<TaxVo> vos = new ArrayList<TaxVo>();
						ResultSet resultSet = statement
								.executeQuery("SELECT id,tax,percentage FROM tax");
						while (resultSet.next()) {
							TaxVo vo = new TaxVo();
							vo.setId(resultSet.getLong("id"));
							vo.setTax(resultSet.getString("tax"));
							vo.setPercentage(resultSet
									.getBigDecimal("percentage"));
							vos.add(vo);
						}
						statement.close();
						return vos;
					}
				});
		return vos;
	}

	public List<TaxVo> findTaxes(final List<Long> taxIds) {
		List<TaxVo> vos = this.sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<List<TaxVo>>() {

					public List<TaxVo> execute(Connection connection)
							throws SQLException {
						Statement statement = connection.createStatement();
						List<TaxVo> vos = new ArrayList<TaxVo>();
						if (taxIds != null && !taxIds.isEmpty()) {
							ResultSet resultSet = statement
									.executeQuery("SELECT id,tax,percentage FROM tax WHERE id IN("
											+ taxIds.toString()
													.replace("[", "")
													.replace("]", "") + ")");
							while (resultSet.next()) {
								TaxVo vo = new TaxVo();
								vo.setId(resultSet.getLong("id"));
								vo.setTax(resultSet.getString("tax"));
								vo.setPercentage(resultSet
										.getBigDecimal("percentage"));
								vos.add(vo);
							}
						}
						statement.close();
						return vos;
					}
				});
		return vos;
	}

}
