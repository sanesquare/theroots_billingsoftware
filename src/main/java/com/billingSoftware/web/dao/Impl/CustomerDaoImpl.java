package com.billingSoftware.web.dao.Impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.hibernate.jdbc.ReturningWork;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.billingSoftware.web.dao.AbstractDao;
import com.billingSoftware.web.dao.CustomerDao;
import com.billingSoftware.web.dao.NumberPropertyDao;
import com.billingSoftware.web.entities.Customer;
import com.billingSoftware.web.execptions.CustomException;
import com.billingSoftware.web.vo.CustomerVo;

/**
 * @author Shamsheer
 * @since Jun 15, 2017
 */
@Repository
public class CustomerDaoImpl extends AbstractDao implements CustomerDao {

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	public Long saveCustomer(CustomerVo vo) {
		Customer customer = null;
		Boolean isFresh = false;
		Long id = vo.getId();
		if (vo.getId() != null)
			customer = this.findCustomerObject(vo.getId());
		if (customer == null) {
			customer = new Customer();
			isFresh = true;
			customer.setReferenceNumber(numberPropertyDao.updateFindNumberProperty("CUST").getCompleteString());
		}
		customer.setName(vo.getName());
		customer.setEmail(vo.getEmail());
		customer.setMobile(vo.getMobile());
		customer.setAddress(vo.getAddress());
		id = (Long) this.sessionFactory.getCurrentSession().save(customer);
		if (isFresh)
			numberPropertyDao.incrementNumberProperty("CUST");
		return id;
	}

	public void deleteCustomer(Long id) {
		Customer customer = this.findCustomerObject(id);
		if (customer == null)
			throw new CustomException("Customer not found");
		customer.setIsDeleted(true);
		//this.sessionFactory.getCurrentSession().save(customer);
		this.sessionFactory.getCurrentSession().delete(customer);
	}

	public Customer findCustomerObject(Long id) {
		return (Customer) this.sessionFactory.getCurrentSession().createCriteria(Customer.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public CustomerVo findCustomer(Long id) {
		Customer customer = this.findCustomerObject(id);
		if (customer == null)
			throw new CustomException("Customer not found");
		CustomerVo vo = new CustomerVo();
		vo.setId(customer.getId());
		vo.setName(customer.getName());
		vo.setReferenceNumber(customer.getReferenceNumber());
		vo.setAddress(customer.getAddress());
		vo.setMobile(customer.getMobile());
		vo.setEmail(customer.getEmail());
		return vo;
	}

	public List<CustomerVo> findAllCustomers() {
		List<CustomerVo> vos = this.sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<List<CustomerVo>>() {
					public List<CustomerVo> execute(Connection connection) throws SQLException {
						Statement statement = connection.createStatement();
						List<CustomerVo> vos = new ArrayList<CustomerVo>();
						ResultSet resultSet = statement.executeQuery(
								"SELECT id,customer_name,reference_number,address,email,mobile FROM customer WHERE (is_deleted IS NULL OR is_deleted=FALSE)");
						while (resultSet.next()) {
							CustomerVo vo = new CustomerVo();
							vo.setId(resultSet.getLong("id"));
							vo.setName(resultSet.getString("customer_name"));
							vo.setReferenceNumber(resultSet.getString("reference_number"));
							vo.setAddress(resultSet.getString("address"));
							vo.setMobile(resultSet.getString("mobile"));
							vo.setEmail(resultSet.getString("email"));
							vos.add(vo);
						}
						statement.close();
						return vos;
					}
				});
		return vos;
	}

}
