package com.billingSoftware.web.dao.Impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.hibernate.jdbc.ReturningWork;
import org.springframework.stereotype.Repository;

import com.billingSoftware.web.dao.AbstractDao;
import com.billingSoftware.web.dao.PaymentModeDao;
import com.billingSoftware.web.entities.PaymentMode;
import com.billingSoftware.web.vo.PaymentModeVo;

/**
 * @author Shamsheer
 * @since Jul 13, 2017
 */
@Repository
public class PaymentModeDaoImpl extends AbstractDao implements PaymentModeDao {

	public Long savePaymentMode(PaymentModeVo vo) {
		// TODO Auto-generated method stub
		return null;
	}

	public PaymentMode findPaymentMode(Long id) {
		return (PaymentMode) this.sessionFactory.getCurrentSession()
				.createCriteria(PaymentMode.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

	public List<PaymentModeVo> findAllPaymentModes() {
		List<PaymentModeVo> vos = this.sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<List<PaymentModeVo>>() {
					public List<PaymentModeVo> execute(Connection connection)
							throws SQLException {
						List<PaymentModeVo> vos = new ArrayList<PaymentModeVo>();
						Statement statement = connection.createStatement();
						ResultSet resultSet = statement.executeQuery("SELECT id,payment_mode FROM payment_mode");
						while (resultSet.next()) {
							PaymentModeVo vo = new PaymentModeVo();
							vo.setId(resultSet.getLong("id"));
							vo.setPaymentMode(resultSet
									.getString("payment_mode"));
							vos.add(vo);
						}
						statement.close();
						return vos;
					}
				});
		return vos;
	}

	public PaymentMode findPaymentMode(String paymentMode) {
		return (PaymentMode) this.sessionFactory.getCurrentSession()
				.createCriteria(PaymentMode.class)
				.add(Restrictions.eq("paymentMode", paymentMode))
				.uniqueResult();
	}

}
