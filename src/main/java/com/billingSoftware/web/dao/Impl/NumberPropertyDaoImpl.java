package com.billingSoftware.web.dao.Impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.jdbc.Work;
import org.springframework.stereotype.Repository;

import com.billingSoftware.web.dao.AbstractDao;
import com.billingSoftware.web.dao.NumberPropertyDao;
import com.billingSoftware.web.entities.NumberProperty;
import com.billingSoftware.web.execptions.CustomException;
import com.billingSoftware.web.util.StringUtil;
import com.billingSoftware.web.vo.NumberPropertyVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 9, 2015
 */
@Repository
public class NumberPropertyDaoImpl extends AbstractDao implements
		NumberPropertyDao {

	public NumberPropertyVo updateFindNumberProperty(final String category) {
		NumberPropertyVo vo = this.sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<NumberPropertyVo>() {

					public NumberPropertyVo execute(Connection connection)
							throws SQLException {
						Statement statement = connection.createStatement();
						NumberPropertyVo vo = new NumberPropertyVo();
						ResultSet resultSet = statement
								.executeQuery("SELECT prefix,number FROM number_property WHERE category='"
										+ category + "'");
						while (resultSet.next()) {
							vo.setCategory(category);
							vo.setPrefix(resultSet.getString("prefix"));
							vo.setNumber(resultSet.getLong("number"));
						}
						if (vo.getPrefix() == null) {
							vo.setCategory(category);
							vo.setPrefix(category);
							vo.setNumber(1L);
							statement
									.executeUpdate("INSERT INTO number_property(category,prefix,number) VALUES('"
											+ category
											+ "','"
											+ category
											+ "',1)");
						}
						vo.setCompleteString(vo.getPrefix() + "-"
								+ vo.getNumber());
						statement.close();
						return vo;
					}
				});
		return vo;
	}

	public void incrementNumberProperty(final String category) {
		this.sessionFactory.getCurrentSession().doWork(new Work() {

			public void execute(Connection connection) throws SQLException {
				Statement statement = connection.createStatement();
				statement
						.execute("UPDATE number_property SET number=number+1 WHERE category='"
								+ category + "'");
				statement.close();
			}
		});
	}

	public NumberProperty findNumberPropertyObject(String category) {
		Criteria criteria = this.sessionFactory.getCurrentSession()
				.createCriteria(NumberProperty.class);
		criteria.add(Restrictions.eq("category", category));
		NumberProperty numberProperty = (NumberProperty) criteria
				.uniqueResult();
		return numberProperty;
	}

}
