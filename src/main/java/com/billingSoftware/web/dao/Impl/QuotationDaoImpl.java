package com.billingSoftware.web.dao.Impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.billingSoftware.web.constants.CommonConstants;
import com.billingSoftware.web.dao.AbstractDao;
import com.billingSoftware.web.dao.CustomerDao;
import com.billingSoftware.web.dao.NumberPropertyDao;
import com.billingSoftware.web.dao.PaymentModeDao;
import com.billingSoftware.web.dao.ProductDao;
import com.billingSoftware.web.dao.QuotationDao;
import com.billingSoftware.web.dao.ServiceProviderDao;
import com.billingSoftware.web.dao.TaxDao;
import com.billingSoftware.web.entities.BillTax;
import com.billingSoftware.web.entities.Customer;
import com.billingSoftware.web.entities.PaymentMode;
import com.billingSoftware.web.entities.Product;
import com.billingSoftware.web.entities.Bill;
import com.billingSoftware.web.entities.BillItem;
import com.billingSoftware.web.entities.ServiceProvider;
import com.billingSoftware.web.entities.Tax;
import com.billingSoftware.web.execptions.CustomException;
import com.billingSoftware.web.util.DateFormatter;
import com.billingSoftware.web.util.StringUtil;
import com.billingSoftware.web.vo.BillTaxVo;
import com.billingSoftware.web.vo.CustomerVo;
import com.billingSoftware.web.vo.NumberPropertyVo;
import com.billingSoftware.web.vo.QuotationItemVo;
import com.billingSoftware.web.vo.QuotationVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 9, 2015
 */
@Repository
public class QuotationDaoImpl extends AbstractDao implements QuotationDao {

	@Autowired
	private ProductDao productDao;

	@Autowired
	private NumberPropertyDao numberPropertyDao;

	@Autowired
	private CustomerDao customerDao;

	@Autowired
	private PaymentModeDao paymentModeDao;

	@Autowired
	private TaxDao taxDao;

	@Autowired
	private ServiceProviderDao serviceProviderDao;

	public Long saveOrUpdate(QuotationVo quotationVo) {
		Long id = quotationVo.getId();
		Bill bill = findQuotationObject(quotationVo.getId());
		Customer customer = customerDao.findCustomerObject(quotationVo.getCustomerId());
		if (customer == null) {
			customer = new Customer();
			customer.setName(quotationVo.getCustomerName());
			customer.setMobile(quotationVo.getMobile());
			customer.setReferenceNumber(numberPropertyDao.updateFindNumberProperty("CUST").getCompleteString());
			this.sessionFactory.getCurrentSession().save(customer);
			numberPropertyDao.incrementNumberProperty("CUST");
		} else {
			customer.setName(quotationVo.getCustomerName());
			customer.setMobile(quotationVo.getMobile());
		}
		if (bill == null) {
			bill = new Bill();
			bill.setIsDeleted(false);
			bill.setCustomer(customer);
			bill.setDate(DateFormatter.convertStringToDate(quotationVo.getDate()));
			bill = this.setAttributesForQuotation(quotationVo, bill);
			id = (Long) this.sessionFactory.getCurrentSession().save(bill);
		} else {

			if (quotationVo.getReferenceNumber().compareTo(bill.getReferenceNumber()) != 0) {
				bill.setUpdatedReferenceNumber(quotationVo.getReferenceNumber());
				bill.setReferenceNumber(quotationVo.getReferenceNumber());
			}

			bill.setCustomer(customer);
			bill.setDate(DateFormatter.convertStringToDate(quotationVo.getDate()));
			bill = this.setAttributesForQuotation(quotationVo, bill);
			this.sessionFactory.getCurrentSession().save(bill);
		}
		return id;
	}

	private Bill setAttributesForQuotation(QuotationVo quotationVo, Bill bill) {
		PaymentMode paymentMode = paymentModeDao.findPaymentMode(quotationVo.getPaymentModeId());
		bill.setPaymentMode(paymentMode);
		Map<Product, BillItem> map = new HashMap<Product, BillItem>();
		for (BillItem item : bill.getBillItems())
			map.put(item.getProduct(), item);
		bill.setTotal(quotationVo.getGrandTotal());
		bill.setDiscountAmount(quotationVo.getDiscountAmount());
		bill.setDiscountPercentage(quotationVo.getDiscountPercentage());
		quotationVo.setReferenceNumber(quotationVo.getReferenceNumber());
		for (QuotationItemVo itemVo : quotationVo.getItemVos()) {
			Product product = productDao.findProduct(itemVo.getProductId());
			if (product == null)
				throw new CustomException("Product Not Found");
			BillItem item = map.get(product);
			if (item == null)
				item = new BillItem();
			else
				map.remove(product);
			item.setProduct(product);
			item.setQuantity(itemVo.getQuantity());
			item.setRate(itemVo.getRate());
			item.setTotal(itemVo.getTotal());
			item.setBill(bill);
			ServiceProvider serviceProvider = serviceProviderDao.findServiceProvider(itemVo.getServiceProviderId());
			item.setServiceProvider(serviceProvider);
			bill.getQuotationItems().add(item);
		}
		for (BillItem billItem : map.values())
			bill.getBillItems().remove(billItem);

		Map<Tax, BillTax> taxMap = new HashMap<Tax, BillTax>();
		for (BillTax tax : bill.getBillTaxs())
			taxMap.put(tax.getTax(), tax);
		bill.setTaxTotal(quotationVo.getTaxTotal());
		for (BillTaxVo taxVo : quotationVo.getTaxVos()) {
			Tax tax = taxDao.findTaxObject(taxVo.getTaxId());
			if (tax == null)
				throw new CustomException("Tax not found");
			BillTax billTax = taxMap.get(tax);
			if (billTax == null)
				billTax = new BillTax();
			else
				taxMap.remove(tax);
			billTax.setTax(tax);
			billTax.setBill(bill);
			billTax.setAmount(taxVo.getAmount());
			bill.getBillTaxs().add(billTax);
		}
		for (BillTax billTax : taxMap.values())
			bill.getBillTaxs().remove(billTax);
		bill.setNetTotal(bill.getTotal().add(bill.getTaxTotal()).subtract(bill.getDiscountAmount()));
		return bill;
	}

	public void deleteQuotation(final Long id) {
		this.sessionFactory.getCurrentSession().doWork(new Work() {
			public void execute(Connection connection) throws SQLException {
				Statement statement = connection.createStatement();
				/*
				 * statement.execute("DELETE FROM bill_item WHERE bill_id=" + id);
				 * statement.execute("DELETE FROM bill_tax WHERE bill_id=" + id);
				 * statement.execute("DELETE FROM bill WHERE id=" + id);
				 */
				statement.execute("UPDATE bill SET is_deleted=TRUE WHERE id=" + id);
				statement.execute(
						"UPDATE bill JOIN (SELECT @number := 0) r SET reference_number = @number := @number + 1 WHERE is_deleted=FALSE");
				statement.close();
			}
		});
	}

	public void updateReferenceNUmber() {
		this.sessionFactory.getCurrentSession().doWork(new Work() {
			public void execute(Connection connection) throws SQLException {
				Statement statement = connection.createStatement();
				statement.execute(
						"UPDATE bill JOIN (SELECT @number := 0) r SET reference_number = CASE WHEN (@number + 1) IN (SELECT refNo FROM (SELECT updated_reference_number AS refNo FROM bill t WHERE t.updated_reference_number IS NOT NULL) AS ref) THEN @number := @number + 2 ELSE @number := @number + 1 END WHERE updated_reference_number IS NULL AND is_deleted=FALSE ");
				statement.close();
			}
		});
	}

	public QuotationVo updateFindQuotation(Long id) {
		Bill bill = findQuotationObject(id);
		if (bill == null)
			throw new CustomException("Bill Not Found");
		return this.createVo(bill);
	}

	private QuotationVo createVo(Bill bill) {
		QuotationVo quotationVo = new QuotationVo();
		quotationVo.setIsPrinted(bill.getIsPrinted());
		quotationVo.setIsDeleted(bill.getIsDeleted());
		quotationVo.setReferenceNumber(bill.getReferenceNumber());
		if (bill.getUpdatedReferenceNumber() != null)
			quotationVo.setReferenceNumber(bill.getUpdatedReferenceNumber());
		quotationVo.setId(bill.getId());
		quotationVo.setDiscountAmount(bill.getDiscountAmount());
		if (quotationVo.getDiscountAmount() == null)
			quotationVo.setDiscountAmount(new BigDecimal("0.00"));
		quotationVo.setGrandTotal(bill.getTotal());
		quotationVo.setDate(DateFormatter.convertDateToString(bill.getDate()));
		Customer customer = bill.getCustomer();
		quotationVo.setCustomerId(customer.getId());
		quotationVo.setCustomerName(customer.getName());
		quotationVo.setMobile(customer.getMobile());
		for (BillItem item : bill.getQuotationItems()) {
			QuotationItemVo vo = new QuotationItemVo();
			Product product = item.getProduct();
			if (product != null) {
				vo.setProductId(product.getId());
			}
			ServiceProvider serviceProvider = item.getServiceProvider();
			if (serviceProvider != null) {
				vo.setServiceProvider(serviceProvider.getServiceProvider());
				vo.setServiceProviderId(serviceProvider.getId());
			}
			vo.setUnit(product.getUnit());
			vo.setId(item.getId());
			vo.setProductName(product.getProduct_name());
			vo.setProductId(product.getId());
			vo.setRate(item.getRate());
			vo.setTotal(item.getTotal());
			vo.setQuantity(item.getQuantity());
			quotationVo.getItemVos().add(vo);
		}
		for (BillTax billTax : bill.getBillTaxs()) {
			Tax tax = billTax.getTax();
			BillTaxVo taxVo = new BillTaxVo();
			taxVo.setId(billTax.getId());
			quotationVo.getTaxids().add(tax.getId());
			taxVo.setTaxId(tax.getId());
			taxVo.setTax(tax.getTax());
			taxVo.setPercentage(tax.getPercentage());
			taxVo.setAmount(billTax.getAmount());
			quotationVo.getTaxVos().add(taxVo);
		}
		quotationVo.setTaxTotal(bill.getTaxTotal());
		quotationVo.setNetTotal(bill.getNetTotal());
		quotationVo.setDiscountPercentage(bill.getDiscountPercentage());
		if (quotationVo.getDiscountPercentage() == null)
			quotationVo.setDiscountPercentage(new BigDecimal("0.00"));
		PaymentMode paymentMode = bill.getPaymentMode();
		if (paymentMode != null) {
			quotationVo.setPaymentMode(paymentMode.getPaymentMode());
			quotationVo.setPaymentModeId(paymentMode.getId());
		}
		return quotationVo;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<QuotationVo> listAllQuotations() {
		List<QuotationVo> vos = this.sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<List<QuotationVo>>() {

					public List<QuotationVo> execute(Connection connection) throws SQLException {
						String sql = "SELECT bill.id, bill.bill_date, bill.reference_number,bill.updated_reference_number, bill.total,bill.tax_total,bill.net_total,customer.id, customer.customer_name,customer.mobile,bill.reference_number AS billNo,bill.discount_amount,bill.discount_percentage,payment_mode.id,payment_mode.payment_mode,service_provider.id,service_provider.service_provider,SUM(item.total) AS serviceProvideContribution  "
								+ ",bill.is_printed FROM bill JOIN customer ON customer.id = bill.customer_id JOIN number_property np ON np.category='BILL' "
								+ "LEFT OUTER JOIN payment_mode ON payment_mode.id=bill.paymentMode_id LEFT OUTER JOIN service_provider "
								+ "ON service_provider.id=bill.serviceProvider_id LEFT OUTER JOIN bill_item item ON item.bill_id = bill.id  "
								+ "WHERE (bill.is_deleted IS NULL OR bill.is_deleted = FALSE ) GROUP BY bill.id ORDER BY bill.reference_number ASC";
						Statement statement = connection.createStatement();
						List<QuotationVo> vos = new ArrayList<QuotationVo>();
						ResultSet resultSet = statement.executeQuery(sql);
						while (resultSet.next()) {
							QuotationVo vo = new QuotationVo();
							vo.setIsPrinted(resultSet.getBoolean("is_printed"));
							vo.setId(resultSet.getLong("bill.id"));
							vo.setReferenceNumber(resultSet.getString("billNo"));
							if (resultSet.getString("updated_reference_number") != null)
								vo.setReferenceNumber(resultSet.getString("updated_reference_number"));
							vo.setMobile(resultSet.getString("customer.mobile"));
							vo.setTaxTotal(resultSet.getBigDecimal("tax_total"));
							vo.setNetTotal(resultSet.getBigDecimal("net_total"));
							vo.setServiceProviderContribution(resultSet.getBigDecimal("serviceProvideContribution"));
							vo.setDate(DateFormatter.convertDateToString(resultSet.getDate("bill.bill_date")));
							vo.setGrandTotal(resultSet.getBigDecimal("bill.total"));
							vo.setCustomerId(resultSet.getLong("customer.id"));
							vo.setCustomerName(resultSet.getString("customer.customer_name"));
							vo.setDiscountAmount(resultSet.getBigDecimal("bill.discount_amount"));
							vo.setDiscountPercentage(resultSet.getBigDecimal("bill.discount_percentage"));
							if (vo.getDiscountPercentage() == null)
								vo.setDiscountPercentage(new BigDecimal("0.00"));
							if (vo.getDiscountAmount() == null)
								vo.setDiscountAmount(new BigDecimal("0.00"));
							vo.setTaxableValue(vo.getGrandTotal().subtract(vo.getDiscountAmount()));
							vo.setPaymentMode(resultSet.getString("payment_mode.payment_mode"));
							vo.setPaymentModeId(resultSet.getLong("payment_mode.id"));
							vo.setServiceProviderId(resultSet.getLong("service_provider.id"));
							vo.setServiceProvider(resultSet.getString("service_provider.service_provider"));
							if (vo.getDiscountPercentage().compareTo(BigDecimal.ZERO) != 0
									&& vo.getServiceProviderContribution() != null
									&& vo.getServiceProviderContribution().compareTo(BigDecimal.ZERO) != 0)
								vo.setServiceProviderContribution(vo.getServiceProviderContribution()
										.subtract(vo.getServiceProviderContribution()
												.multiply(vo.getDiscountPercentage()).divide(new BigDecimal(100), 2))
										.setScale(2, RoundingMode.HALF_EVEN));
							vos.add(vo);
						}
						statement.close();
						return vos;
					}
				});
		return vos;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	public List<QuotationVo> findQuotationsWithinDateRange(final String startDate, final String endDate,
			final Long customerId, final Long paymentModeId, final Long serviceProviderId) {
		List<QuotationVo> vos = this.sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<List<QuotationVo>>() {

					public List<QuotationVo> execute(Connection connection) throws SQLException {
						String sql = "SELECT bill.id, bill.bill_date, bill.reference_number,bill.updated_reference_number, "
								+ "bill.total,bill.tax_total,bill.net_total,customer.id, customer.customer_name,"
								+ "customer.mobile,bill.reference_number AS billNo,bill.discount_amount,bill.discount_percentage,"
								+ "payment_mode.id,payment_mode.payment_mode,service_provider.id,service_provider.service_provider,"
								+ "service_provider.service_provider,SUM(item.total) AS serviceProvideContribution "
								+ ",bill.is_printed FROM bill JOIN customer ON customer.id = bill.customer_id JOIN "
								+ "number_property np ON np.category='BILL'  LEFT OUTER JOIN payment_mode ON "
								+ "payment_mode.id=bill.paymentMode_id  LEFT OUTER JOIN bill_item item ON "
								+ "item.bill_id = bill.id LEFT OUTER JOIN service_provider ON service_provider.id=item.serviceProvider_id ";
						if (customerId != null)
							if (sql.contains("WHERE"))
								sql += " AND customer.id=" + customerId;
							else
								sql += " WHERE customer.id=" + customerId;
						if (paymentModeId != null)
							if (sql.contains("WHERE"))
								sql += " AND payment_mode.id=" + paymentModeId;
							else
								sql += " WHERE payment_mode.id=" + paymentModeId;
						if (serviceProviderId != null)
							if (sql.contains("WHERE"))
								sql += " AND service_provider.id=" + serviceProviderId;
							else
								sql += " WHERE service_provider.id=" + serviceProviderId;
						if (startDate != null && !startDate.isEmpty())
							if (sql.contains("WHERE"))
								sql += " AND bill.bill_date>='"
										+ new Date(DateFormatter.convertStringToDate(startDate).getTime()) + "'";
							else
								sql += " WHERE bill.bill_date>='"
										+ new Date(DateFormatter.convertStringToDate(startDate).getTime()) + "'";
						if (endDate != null && !endDate.isEmpty())
							if (sql.contains("WHERE"))
								sql += " AND bill.bill_date<='"
										+ new Date(DateFormatter.convertStringToDate(endDate).getTime()) + "'";
							else
								sql += " WHERE bill.bill_date<='"
										+ new Date(DateFormatter.convertStringToDate(endDate).getTime()) + "'";
						sql += " AND (bill.is_deleted IS NULL OR bill.is_deleted = FALSE ) GROUP BY bill.id  ORDER BY bill.reference_number ASC";
						Statement statement = connection.createStatement();
						List<QuotationVo> vos = new ArrayList<QuotationVo>();
						ResultSet resultSet = statement.executeQuery(sql);
						while (resultSet.next()) {
							QuotationVo vo = new QuotationVo();
							vo.setIsPrinted(resultSet.getBoolean("is_printed"));
							vo.setId(resultSet.getLong("bill.id"));
							vo.setReferenceNumber(resultSet.getString("billNo"));
							if (resultSet.getString("updated_reference_number") != null)
								vo.setReferenceNumber(resultSet.getString("updated_reference_number"));
							vo.setMobile(resultSet.getString("customer.mobile"));
							vo.setTaxTotal(resultSet.getBigDecimal("tax_total"));
							vo.setNetTotal(resultSet.getBigDecimal("net_total"));
							vo.setServiceProviderContribution(resultSet.getBigDecimal("serviceProvideContribution"));
							vo.setDate(DateFormatter.convertDateToString(resultSet.getDate("bill.bill_date")));
							vo.setGrandTotal(resultSet.getBigDecimal("bill.total"));
							vo.setCustomerId(resultSet.getLong("customer.id"));
							vo.setCustomerName(resultSet.getString("customer.customer_name"));
							vo.setDiscountAmount(resultSet.getBigDecimal("bill.discount_amount"));
							vo.setDiscountPercentage(resultSet.getBigDecimal("bill.discount_percentage"));
							if (vo.getDiscountPercentage() == null)
								vo.setDiscountPercentage(new BigDecimal("0.00"));
							if (vo.getDiscountAmount() == null)
								vo.setDiscountAmount(new BigDecimal("0.00"));
							vo.setTaxableValue(vo.getGrandTotal().subtract(vo.getDiscountAmount()));
							vo.setPaymentMode(resultSet.getString("payment_mode.payment_mode"));
							vo.setPaymentModeId(resultSet.getLong("payment_mode.id"));
							vo.setServiceProviderId(resultSet.getLong("service_provider.id"));
							vo.setServiceProvider(resultSet.getString("service_provider.service_provider"));
							if (vo.getDiscountPercentage().compareTo(BigDecimal.ZERO) != 0
									&& vo.getServiceProviderContribution() != null
									&& vo.getServiceProviderContribution().compareTo(BigDecimal.ZERO) != 0)
								vo.setServiceProviderContribution(vo.getServiceProviderContribution()
										.subtract(vo.getServiceProviderContribution()
												.multiply(vo.getDiscountPercentage()).divide(new BigDecimal(100), 2))
										.setScale(2, RoundingMode.HALF_EVEN));
							vos.add(vo);
						}
						statement.close();
						return vos;
					}
				});
		return vos;
	}

	public Bill findQuotationObject(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Bill.class);
		criteria.add(Restrictions.eq("id", id));
		return (Bill) criteria.uniqueResult();
	}

	public void updatePrintStatus(final Long id) {
		this.sessionFactory.getCurrentSession().doWork(new Work() {
			public void execute(Connection connection) throws SQLException {
				Statement statement = connection.createStatement();
				statement.execute("UPDATE bill SET is_printed=TRUE WHERE id=" + id);
				statement.close();
			}
		});
	}

	public List<QuotationVo> findDeletedQuotations(final String startDate, final String endDate) {
		List<QuotationVo> vos = this.sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<List<QuotationVo>>() {

					public List<QuotationVo> execute(Connection connection) throws SQLException {
						String sql = "SELECT bill.id, bill.bill_date, bill.reference_number,bill.updated_reference_number, bill.total,bill.tax_total,bill.net_total,customer.id, customer.customer_name,customer.mobile,bill.reference_number AS billNo,bill.discount_amount,bill.discount_percentage,payment_mode.id,payment_mode.payment_mode,service_provider.id,service_provider.service_provider,service_provider.service_provider,SUM(item.total) AS serviceProvideContribution "
								+ ",bill.is_printed FROM bill JOIN customer ON customer.id = bill.customer_id JOIN number_property np ON np.category='BILL'  LEFT OUTER JOIN payment_mode ON payment_mode.id=bill.paymentMode_id  LEFT OUTER JOIN bill_item item ON item.bill_id = bill.id LEFT OUTER JOIN service_provider ON service_provider.id=item.serviceProvider_id ";
						/*
						 * if (startDate != null && !startDate.isEmpty()) if (sql.contains("WHERE")) sql
						 * += " AND bill.bill_date>='" + new
						 * Date(DateFormatter.convertStringToDate(startDate).getTime()) + "'"; else sql
						 * += " WHERE bill.bill_date>='" + new
						 * Date(DateFormatter.convertStringToDate(startDate).getTime()) + "'"; if
						 * (endDate != null && !endDate.isEmpty()) if (sql.contains("WHERE")) sql +=
						 * " AND bill.bill_date<='" + new
						 * Date(DateFormatter.convertStringToDate(endDate).getTime()) + "'"; else sql +=
						 * " WHERE bill.bill_date<='" + new
						 * Date(DateFormatter.convertStringToDate(endDate).getTime()) + "'";
						 */
						sql += " WHERE bill.is_deleted=TRUE GROUP BY bill.id  ORDER BY bill.reference_number ASC";
						Statement statement = connection.createStatement();
						List<QuotationVo> vos = new ArrayList<QuotationVo>();
						ResultSet resultSet = statement.executeQuery(sql);
						while (resultSet.next()) {
							QuotationVo vo = new QuotationVo();
							vo.setIsPrinted(resultSet.getBoolean("is_printed"));
							vo.setId(resultSet.getLong("bill.id"));
							vo.setReferenceNumber(resultSet.getString("billNo"));
							if (resultSet.getString("updated_reference_number") != null)
								vo.setReferenceNumber(resultSet.getString("updated_reference_number"));
							vo.setMobile(resultSet.getString("customer.mobile"));
							vo.setTaxTotal(resultSet.getBigDecimal("tax_total"));
							vo.setNetTotal(resultSet.getBigDecimal("net_total"));
							vo.setServiceProviderContribution(resultSet.getBigDecimal("serviceProvideContribution"));
							vo.setDate(DateFormatter.convertDateToString(resultSet.getDate("bill.bill_date")));
							vo.setGrandTotal(resultSet.getBigDecimal("bill.total"));
							vo.setCustomerId(resultSet.getLong("customer.id"));
							vo.setCustomerName(resultSet.getString("customer.customer_name"));
							vo.setDiscountAmount(resultSet.getBigDecimal("bill.discount_amount"));
							vo.setDiscountPercentage(resultSet.getBigDecimal("bill.discount_percentage"));
							if (vo.getDiscountPercentage() == null)
								vo.setDiscountPercentage(new BigDecimal("0.00"));
							if (vo.getDiscountAmount() == null)
								vo.setDiscountAmount(new BigDecimal("0.00"));
							vo.setTaxableValue(vo.getGrandTotal().subtract(vo.getDiscountAmount()));
							vo.setPaymentMode(resultSet.getString("payment_mode.payment_mode"));
							vo.setPaymentModeId(resultSet.getLong("payment_mode.id"));
							vo.setServiceProviderId(resultSet.getLong("service_provider.id"));
							vo.setServiceProvider(resultSet.getString("service_provider.service_provider"));
							if (vo.getDiscountPercentage().compareTo(BigDecimal.ZERO) != 0
									&& vo.getServiceProviderContribution() != null
									&& vo.getServiceProviderContribution().compareTo(BigDecimal.ZERO) != 0)
								vo.setServiceProviderContribution(vo.getServiceProviderContribution()
										.subtract(vo.getServiceProviderContribution()
												.multiply(vo.getDiscountPercentage()).divide(new BigDecimal(100), 2))
										.setScale(2, RoundingMode.HALF_EVEN));
							vos.add(vo);
						}
						statement.close();
						return vos;
					}
				});
		return vos;
	}

}
