package com.billingSoftware.web.dao.Impl;

import java.util.Calendar;
import java.util.TimeZone;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.util.DigestUtils;

import com.billingSoftware.web.dao.AbstractDao;
import com.billingSoftware.web.dao.UserDao;
import com.billingSoftware.web.entities.Settings;
import com.billingSoftware.web.entities.User;

/**
 * @author Shamsheer
 * @since Jun 17, 2017
 */
@Repository
public class UserDaoImpl extends AbstractDao implements UserDao {

	public User findUser(String username) {
		return (User) this.sessionFactory.getCurrentSession()
				.createCriteria(User.class)
				.add(Restrictions.eq("username", username)).uniqueResult();
	}

	public void updatePassword(String username, String password) {
		User user = this.findUser(username);
		if (user != null) {
			user.setPassword(DigestUtils.md5DigestAsHex(password.getBytes()));
			this.sessionFactory.getCurrentSession().save(user);
		}
	}

	public Settings updateFindSettings() {
		Settings settings = (Settings) this.sessionFactory.getCurrentSession()
				.createCriteria(Settings.class).uniqueResult();
		if (settings == null) {
			Calendar calendar = Calendar.getInstance(TimeZone
					.getTimeZone("Asia/Kolkata"));
			calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + 1);
			settings = new Settings();
			settings.setExpiryDate(calendar.getTime());
			this.sessionFactory.getCurrentSession().save(settings);
		}
		return settings;
	}


}
