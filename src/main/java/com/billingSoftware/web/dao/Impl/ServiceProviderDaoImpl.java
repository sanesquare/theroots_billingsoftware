package com.billingSoftware.web.dao.Impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.hibernate.jdbc.ReturningWork;
import org.springframework.stereotype.Repository;

import com.billingSoftware.web.dao.AbstractDao;
import com.billingSoftware.web.dao.ServiceProviderDao;
import com.billingSoftware.web.entities.ServiceProvider;
import com.billingSoftware.web.execptions.CustomException;
import com.billingSoftware.web.vo.ServiceProviderVo;

/**
 * @author Shamsheer
 * @since Jul 13, 2017
 */
@Repository
public class ServiceProviderDaoImpl extends AbstractDao implements
		ServiceProviderDao {

	public Long saveServiceProvider(ServiceProviderVo vo) {
		Long id = vo.getId();
		ServiceProvider serviceProvider = null;
		if (id != null)
			serviceProvider = this.findServiceProvider(id);
		if (serviceProvider == null)
			serviceProvider = new ServiceProvider();
		serviceProvider.setServiceProvider(vo.getServiceProvider());
		serviceProvider.setMobile(vo.getMobile());
		id = (Long) this.sessionFactory.getCurrentSession().save(
				serviceProvider);
		return id;
	}

	public void deleteServiceProvider(Long id) {
		ServiceProvider serviceProvider = this.findServiceProvider(id);
		if (serviceProvider == null)
			throw new CustomException("Service provider not found");
		this.sessionFactory.getCurrentSession().delete(serviceProvider);
	}

	public List<ServiceProviderVo> findAllServiceProviders() {
		List<ServiceProviderVo> vos = this.sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<List<ServiceProviderVo>>() {

					public List<ServiceProviderVo> execute(Connection connection)
							throws SQLException {
						Statement statement = connection.createStatement();
						List<ServiceProviderVo> vos = new ArrayList<ServiceProviderVo>();
						ResultSet resultSet = statement
								.executeQuery("SELECT id,service_provider,mobile FROM service_provider");
						while (resultSet.next()) {
							ServiceProviderVo vo = new ServiceProviderVo();
							vo.setId(resultSet.getLong("id"));
							vo.setServiceProvider(resultSet
									.getString("service_provider"));
							vo.setMobile(resultSet.getString("mobile"));
							vos.add(vo);
						}
						statement.close();
						return vos;
					}
				});
		return vos;
	}

	public ServiceProvider findServiceProvider(Long id) {
		return (ServiceProvider) this.sessionFactory.getCurrentSession()
				.createCriteria(ServiceProvider.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}

}
