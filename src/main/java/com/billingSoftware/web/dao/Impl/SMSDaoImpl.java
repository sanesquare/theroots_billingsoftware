package com.billingSoftware.web.dao.Impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hibernate.jdbc.ReturningWork;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.billingSoftware.web.dao.AbstractDao;
import com.billingSoftware.web.dao.CustomerDao;
import com.billingSoftware.web.dao.SMSDao;
import com.billingSoftware.web.entities.SMSHistory;
import com.billingSoftware.web.util.DateFormatter;
import com.billingSoftware.web.vo.CustomerVo;
import com.billingSoftware.web.vo.SMSHistoryVo;
import com.billingSoftware.web.vo.SMSVo;

/**
 * 
 * @author Suhail
 * @since Jun 17, 2017
 */

@Repository
public class SMSDaoImpl extends AbstractDao implements SMSDao {
	
	@Autowired
	CustomerDao customerDao;

	public void saveSentSMS(SMSVo vo) {
		SMSHistory smsHistory = new SMSHistory();
		smsHistory.setMessage(vo.getMessage());
		smsHistory.setResponse(vo.getResponse());
		smsHistory.setCustomerIDs(vo.getCustomerIds());
		smsHistory.setDate(new Timestamp(new java.util.Date().getTime()));
		smsHistory.setOtherNumbers(vo.getOtherNumbers());
		this.sessionFactory.getCurrentSession().save(smsHistory);
	}
	
	public List<SMSHistoryVo> findAllSMShistory(){
		List<SMSHistoryVo> historyVos = this.sessionFactory.getCurrentSession().doReturningWork(new ReturningWork<List<SMSHistoryVo>>() {

			public List<SMSHistoryVo> execute(Connection conn)
					throws SQLException {
				List<SMSHistoryVo> historyVos = new ArrayList<SMSHistoryVo>();
				String sql = "SELECT * FROM sms_history";
				Statement statement = conn.createStatement();
				ResultSet resultSet = statement.executeQuery(sql);
				while(resultSet.next()){
					SMSHistoryVo vo = new SMSHistoryVo();
					vo.setId(resultSet.getLong("id"));
					vo.setMessage(resultSet.getString("message"));
					vo.setReponse(resultSet.getString("response"));
					vo.setCustomerNames("");
					vo.setDateTime(DateFormatter.convertDateToString(resultSet.getDate("date_time")));
					if(resultSet.getString("customer_ids")!=null){
						List<String> items = Arrays.asList(resultSet.getString("customer_ids").split("\\s*,\\s*"));
						for(String item : items){
							CustomerVo customerVo = customerDao.findCustomer(Long.parseLong(item));
							vo.setCustomerNames(vo.getCustomerNames() + customerVo.getName() + ", ");
						}
						
					}
					if(resultSet.getString("other_numbers")!=null){
						vo.setCustomerNames(vo.getCustomerNames() + resultSet.getString("other_numbers"));
					}
					if(vo.getCustomerNames()!=null)
						vo.getCustomerNames().replaceAll(", $", "");
					historyVos.add(vo);
				}
				return historyVos;
			}
		});
		return historyVos;
	}

}
