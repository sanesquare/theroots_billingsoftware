package com.billingSoftware.web.dao;

import java.util.List;

import com.billingSoftware.web.entities.PaymentMode;
import com.billingSoftware.web.vo.PaymentModeVo;

/**
 * @author Shamsheer
 * @since Jul 13, 2017
 */
public interface PaymentModeDao {

	/**
	 * method to save payment mode
	 * 
	 * @param vo
	 * @return
	 */
	public Long savePaymentMode(PaymentModeVo vo);

	/**
	 * method to find payment mode
	 * 
	 * @param id
	 * @return
	 */
	public PaymentMode findPaymentMode(Long id);

	/**
	 * method to find payment mode
	 * 
	 * @param paymentMode
	 * @return
	 */
	public PaymentMode findPaymentMode(String paymentMode);

	/**
	 * method to find payment modes
	 * 
	 * @return
	 */
	public List<PaymentModeVo> findAllPaymentModes();
}
