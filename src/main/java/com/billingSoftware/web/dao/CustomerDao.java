/**
 * 
 */
package com.billingSoftware.web.dao;

import java.util.List;

import com.billingSoftware.web.entities.Customer;
import com.billingSoftware.web.vo.CustomerVo;

/**
 * @author Shamsheer
 * @since Jun 15, 2017
 */
public interface CustomerDao {

	/**
	 * method to save customer
	 * 
	 * @param vo
	 */
	public Long saveCustomer(CustomerVo vo);

	/**
	 * method to delete customer
	 * 
	 * @param id
	 */
	public void deleteCustomer(Long id);

	/**
	 * method to find customer object
	 * 
	 * @param id
	 * @return
	 */
	public Customer findCustomerObject(Long id);

	/**
	 * method to find customer by id
	 * 
	 * @param id
	 * @return
	 */
	public CustomerVo findCustomer(Long id);

	/**
	 * method to list all customers
	 * 
	 * @return
	 */
	public List<CustomerVo> findAllCustomers();
}
