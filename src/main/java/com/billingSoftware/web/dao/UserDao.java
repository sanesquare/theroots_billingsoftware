package com.billingSoftware.web.dao;

import com.billingSoftware.web.entities.Settings;
import com.billingSoftware.web.entities.User;

/**
 * @author Shamsheer
 * @since Jun 17, 2017
 */
public interface UserDao {

	/**
	 * method to find user by username
	 * 
	 * @param username
	 * @return
	 */
	public User findUser(String username);

	/**
	 * method to update password
	 * 
	 * @param username
	 * @param password
	 */
	public void updatePassword(String username, String password);
	
	/**
	 * @return
	 */
	public Settings updateFindSettings();
}
