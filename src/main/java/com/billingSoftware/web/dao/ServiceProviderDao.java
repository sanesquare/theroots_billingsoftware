package com.billingSoftware.web.dao;

import java.util.List;

import com.billingSoftware.web.entities.ServiceProvider;
import com.billingSoftware.web.vo.ServiceProviderVo;

/**
 * @author Shamsheer
 * @since Jul 13, 2017
 */
public interface ServiceProviderDao {

	/**
	 * method to save service provider
	 * 
	 * @param vo
	 * @return
	 */
	public Long saveServiceProvider(ServiceProviderVo vo);

	/**
	 * method to delete service provider
	 * 
	 * @param id
	 */
	public void deleteServiceProvider(Long id);

	/**
	 * method to list all service providers
	 * 
	 * @return
	 */
	public List<ServiceProviderVo> findAllServiceProviders();

	/**
	 * method to find service provider object
	 * 
	 * @param id
	 * @return
	 */
	public ServiceProvider findServiceProvider(Long id);
}
