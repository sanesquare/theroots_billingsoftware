package com.billingSoftware.web.dao;

import java.util.List;

import com.billingSoftware.web.entities.Bill;
import com.billingSoftware.web.vo.CustomerVo;
import com.billingSoftware.web.vo.QuotationVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 9, 2015
 */
public interface QuotationDao {

	/**
	 * Method to save or update quotation
	 * 
	 * @param quotationVo
	 */
	public Long saveOrUpdate(QuotationVo quotationVo);

	/**
	 * Method to delete quotation
	 * 
	 * @param id
	 */
	public void deleteQuotation(Long id);

	/**
	 * Method to find quotation
	 * 
	 * @param id
	 * @return
	 */
	public QuotationVo updateFindQuotation(Long id);

	/**
	 * Method to list all quotations
	 * 
	 * @return
	 */
	public List<QuotationVo> listAllQuotations();

	/**
	 * Method to list quotations within date range
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<QuotationVo> findQuotationsWithinDateRange(String startDate,
			String endDate, Long customerId, Long paymentModeId,
			Long serviceProviderId);

	/**
	 * Method to find quotation object by id
	 * 
	 * @param id
	 * @return
	 */
	public Bill findQuotationObject(Long id);

	/**
	 * method to update reference number
	 */
	public void updateReferenceNUmber();
	
	public void updatePrintStatus(Long id);

	public List<QuotationVo> findDeletedQuotations(final String startDate, final String endDate);
}
