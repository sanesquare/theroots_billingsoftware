package com.billingSoftware.web.dao;

import java.util.List;

import com.billingSoftware.web.entities.Product;
import com.billingSoftware.web.vo.ProductVo;

public interface ProductDao {

	/**
	 * method to save product
	 * @param productVo
	 */
	public void saveProduct(ProductVo productVo);
	
	/**
	 * method to delete product
	 * @param id
	 */
	public void deleteProduct(Long id);
	
	/**
	 * method to find product
	 * @param id
	 * @return
	 */
	public Product findProduct(Long id);
	
	/**
	 * method to find product
	 * @param id
	 * @return
	 */
	public ProductVo findProductById(Long id);
	
	/**
	 * method to find all products
	 * @return
	 */
	public List<ProductVo> findAllProducts();
	
	/**
	 * method to find products with given ids
	 * @param ids
	 * @return
	 */
	public List<ProductVo> findProducts(List<Long> ids);
}
