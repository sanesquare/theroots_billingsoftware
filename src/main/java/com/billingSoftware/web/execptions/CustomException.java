package com.billingSoftware.web.execptions;

/**
 * 
 * @author Vinutha
 * @since 20-July-2015
 *
 */
public class CustomException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1350499370235922058L;

	private String message = null;

	/**
	 * 
	 * @param message
	 */
	public CustomException(String message) {
		super(message);
		this.message = message;
	}

	/**
	 * 
	 * @param cause
	 */
	public CustomException(Throwable cause) {
		super(cause);
	}

	@Override
	public String toString() {
		return message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
