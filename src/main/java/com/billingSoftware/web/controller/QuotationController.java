package com.billingSoftware.web.controller;

import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.billingSoftware.web.constants.CommonConstants;
import com.billingSoftware.web.constants.PageNameConstants;
import com.billingSoftware.web.constants.RequestConstants;
import com.billingSoftware.web.entities.BillItem;
import com.billingSoftware.web.execptions.CustomException;
import com.billingSoftware.web.logger.Log;
import com.billingSoftware.web.mail.ApplicationMailSender;
import com.billingSoftware.web.service.CustomerService;
import com.billingSoftware.web.service.PaymentModeService;
import com.billingSoftware.web.service.QuotationService;
import com.billingSoftware.web.service.ServiceProviderService;
import com.billingSoftware.web.service.TaxService;
import com.billingSoftware.web.util.DateFormatter;
import com.billingSoftware.web.util.RandomNumberUtil;
import com.billingSoftware.web.util.SaveUsernameToRemoteDb;
import com.billingSoftware.web.vo.BillJsonVo;
import com.billingSoftware.web.vo.BillTaxVo;
import com.billingSoftware.web.vo.EmailVo;
import com.billingSoftware.web.vo.MailConfigurationVo;
import com.billingSoftware.web.vo.QuotationItemVo;
import com.billingSoftware.web.vo.QuotationVo;
import com.billingSoftware.web.vo.TaxVo;

@Controller
public class QuotationController {

	/**
	 * Log
	 */
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	@Qualifier(value = "taskExecutor")
	private TaskExecutor taskExecutor;

	@Autowired
	private QuotationService quotationService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private ServletContext context;

	@Autowired
	private TaxService taxService;

	@Autowired
	private ServletContext servletContext;

	@Autowired
	private PaymentModeService paymentModeService;

	@Autowired
	private ServiceProviderService serviceProviderService;

	@RequestMapping(value = RequestConstants.BILL, method = RequestMethod.GET)
	public String homePge(final Model model, HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("customers", customerService.findAllCustomers());
			model.addAttribute("paymentModes", paymentModeService.findAllPaymentModes());
			model.addAttribute("serviceProviders", serviceProviderService.findAllServiceProviders());
			QuotationVo quotationVo = new QuotationVo();
			quotationVo.setDate(DateFormatter.convertDateToString(new Date()));
			String stringId = request.getParameter("id");
			if (stringId != null) {
				Long id = Long.parseLong(stringId);
				quotationVo = quotationService.findQuotation(id);
			}
			model.addAttribute("taxVos", taxService.findAllTaxes());
			model.addAttribute("quotation", quotationVo);
		} catch (Exception e) {
			logger.error("QUOTATION : ", e);
		}
		return PageNameConstants.BILL;
	}

	@RequestMapping(value = RequestConstants.BILL_HISTORY, method = RequestMethod.GET)
	public String billHistory(final Model model, HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			String endDate = DateFormatter.convertDateToString(calendar.getTime());
			calendar.set(Calendar.DATE, -7);
			String startDate = DateFormatter.convertDateToString(calendar.getTime());
			List<QuotationVo> vos = quotationService.findQuotationsWithinDateRange(startDate, endDate, null, null,
					null);
			BigDecimal total = BigDecimal.ZERO;
			BigDecimal discoundTotal = BigDecimal.ZERO;
			BigDecimal taxableTotal = BigDecimal.ZERO;
			BigDecimal taxTotal = BigDecimal.ZERO;
			BigDecimal netTotal = BigDecimal.ZERO;
			BigDecimal serviceProviderContributionTotal = BigDecimal.ZERO;
			for (QuotationVo vo : vos) {
				total = total.add(vo.getGrandTotal());
				discoundTotal = discoundTotal.add(vo.getDiscountAmount());
				taxableTotal = taxableTotal.add(vo.getTaxableValue());
				taxTotal = taxTotal.add(vo.getTaxTotal());
				netTotal = netTotal.add(vo.getNetTotal());
				if (vo.getServiceProviderContribution() != null)
					serviceProviderContributionTotal = serviceProviderContributionTotal
							.add(vo.getServiceProviderContribution());
			}
			model.addAttribute("startDate", startDate);
			model.addAttribute("endDate", endDate);
			model.addAttribute("serviceProviderContributionTotal", serviceProviderContributionTotal);
			model.addAttribute("netTotal", netTotal);
			model.addAttribute("taxTotal", taxTotal);
			model.addAttribute("discoundTotal", discoundTotal);
			model.addAttribute("taxableTotal", taxableTotal);
			model.addAttribute("total", total);
			model.addAttribute("bills", vos);
			model.addAttribute("customers", customerService.findAllCustomers());
			model.addAttribute("paymentModes", paymentModeService.findAllPaymentModes());
			model.addAttribute("serviceProviders", serviceProviderService.findAllServiceProviders());
		} catch (Exception e) {
			logger.error("QUOTATION : ", e);
		}
		return PageNameConstants.BILL_HISTORY;
	}

	@RequestMapping(value = RequestConstants.BILL_LIST, method = RequestMethod.GET)
	public String billList(final Model model, HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			List<QuotationVo> vos = quotationService.listAllQuotations();
			BigDecimal total = BigDecimal.ZERO;
			BigDecimal discoundTotal = BigDecimal.ZERO;
			BigDecimal taxableTotal = BigDecimal.ZERO;
			BigDecimal taxTotal = BigDecimal.ZERO;
			BigDecimal serviceProviderContributionTotal = BigDecimal.ZERO;
			BigDecimal netTotal = BigDecimal.ZERO;
			for (QuotationVo vo : vos) {
				total = total.add(vo.getGrandTotal());
				discoundTotal = discoundTotal.add(vo.getDiscountAmount());
				taxableTotal = taxableTotal.add(vo.getTaxableValue());
				taxTotal = taxTotal.add(vo.getTaxTotal());
				netTotal = netTotal.add(vo.getNetTotal());
				serviceProviderContributionTotal = serviceProviderContributionTotal
						.add(vo.getServiceProviderContribution());
			}
			model.addAttribute("serviceProviderContributionTotal", serviceProviderContributionTotal);
			model.addAttribute("netTotal", netTotal);
			model.addAttribute("taxTotal", taxTotal);
			model.addAttribute("discoundTotal", discoundTotal);
			model.addAttribute("taxableTotal", taxableTotal);
			model.addAttribute("total", total);
			model.addAttribute("bills", vos);
		} catch (Exception e) {
			logger.error("QUOTATION : ", e);
		}
		return PageNameConstants.BILL_LIST;
	}

	@RequestMapping(value = RequestConstants.BILL_FILTER, method = RequestMethod.GET)
	public String billFilter(final Model model, HttpServletRequest request,
			@RequestParam(value = "fromDate", required = false) String fromDate,
			@RequestParam(value = "toDate", required = false) String toDate,
			@RequestParam(value = "customerId", required = false) Long customerId,
			@RequestParam(value = "paymentModeId", required = false) Long paymentModeId,
			@RequestParam(value = "serviceProviderId", required = false) Long serviceProviderId) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			List<QuotationVo> vos = quotationService.findQuotationsWithinDateRange(fromDate, toDate, customerId,
					paymentModeId, serviceProviderId);
			BigDecimal total = BigDecimal.ZERO;
			BigDecimal discoundTotal = BigDecimal.ZERO;
			BigDecimal taxableTotal = BigDecimal.ZERO;
			BigDecimal taxTotal = BigDecimal.ZERO;
			BigDecimal netTotal = BigDecimal.ZERO;
			BigDecimal serviceProviderContributionTotal = BigDecimal.ZERO;
			for (QuotationVo vo : vos) {
				total = total.add(vo.getGrandTotal());
				discoundTotal = discoundTotal.add(vo.getDiscountAmount());
				taxableTotal = taxableTotal.add(vo.getTaxableValue());
				taxTotal = taxTotal.add(vo.getTaxTotal());
				netTotal = netTotal.add(vo.getNetTotal());
				if (vo.getServiceProviderContribution() != null)
					serviceProviderContributionTotal = serviceProviderContributionTotal
							.add(vo.getServiceProviderContribution());
			}
			model.addAttribute("serviceProviderContributionTotal", serviceProviderContributionTotal);
			model.addAttribute("netTotal", netTotal);
			model.addAttribute("taxTotal", taxTotal);
			model.addAttribute("discoundTotal", discoundTotal);
			model.addAttribute("taxableTotal", taxableTotal);
			model.addAttribute("total", total);
			model.addAttribute("bills", vos);
		} catch (Exception e) {
			logger.error("QUOTATION : ", e);
		}
		return PageNameConstants.BILL_LIST;
	}

	@RequestMapping(value = RequestConstants.BILL_SAVE, method = RequestMethod.POST)
	public @ResponseBody BillJsonVo saveQuotation(final Model model, @RequestBody QuotationVo quotationVo,
			@RequestParam(value = "print") Boolean print) {
		Long id = 0L;
		BillJsonVo billJsonVo = new BillJsonVo();
		billJsonVo.setPrint(print);
		try {
			id = quotationService.saveOrUpdate(quotationVo);
			quotationService.updateReferenceNUmber();
			if (print) {
				printThisBill(id);
				billJsonVo.setUrl("resources/bill.pdf");
			}
		} catch (Exception e) {
			logger.error("QUOTATION_SAVE : ", e);
		}
		billJsonVo.setId(id);
		return billJsonVo;
	}

	@RequestMapping(value = RequestConstants.BILL_PRINT, method = RequestMethod.GET)
	public @ResponseBody String printBill(@RequestParam(value = "id", required = true) Long id) {
		String path = "";
		try {
			printThisBill(id);
			path = "resources/bill.pdf";
		} catch (Exception e) {
			logger.error("", e);
		}
		return path;
	}

	private File printThisBill(Long id) throws JRException, IOException {
		QuotationVo vo = quotationService.findQuotation(id);
		quotationService.updatePrintStatus(id);
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<QuotationItemVo> itemVos = vo.getItemVos();
		List<BillTaxVo> taxVos = vo.getTaxVos();
		QuotationItemVo discountVo = new QuotationItemVo();
		discountVo.setTax("Discount:");
		discountVo.setAmount(vo.getDiscountAmount());
		itemVos.add(discountVo);
		QuotationItemVo totalVo = new QuotationItemVo();
		totalVo.setTax("Taxable Value:");
		totalVo.setAmount(vo.getGrandTotal().subtract(vo.getDiscountAmount()));
		itemVos.add(totalVo);
		for (BillTaxVo taxVo : taxVos) {
			QuotationItemVo itemVo = new QuotationItemVo();
			itemVo.setTax(taxVo.getTax());
			itemVo.setAmount(taxVo.getAmount());
			itemVos.add(itemVo);
		}

		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(itemVos);
		parameters.put("datasource", dataSource);
		parameters.put("grandTotalLabel", "Total:");
		parameters.put("customerName", vo.getCustomerName());
		parameters.put("referenceNumber", vo.getReferenceNumber());
		parameters.put("date", vo.getDate());
		parameters.put("grossTotal", vo.getGrandTotal());
		parameters.put("mobile", vo.getMobile());
		parameters.put("taxTotal", vo.getTaxTotal());
		parameters.put("netTotal", vo.getNetTotal());
		parameters.put("logo", context.getRealPath("/WEB-INF/jrxml/theRoots.PNG"));
		String url = "/WEB-INF/jrxml/bill.jrxml";
		InputStream streamIo = context.getResourceAsStream(url);
		JasperReport jasperReport = JasperCompileManager.compileReport(streamIo);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

		File file = new File(servletContext.getRealPath("/resources/") + "/bill.pdf");

		/*
		 * if (file.exists()) file.delete();
		 */

		JasperExportManager.exportReportToPdfFile(jasperPrint, file.getAbsolutePath());
		return file;
		// JasperPrintManager.printReport(jasperPrint, true);
	}

	@RequestMapping(value = RequestConstants.BILL_DELETE, method = RequestMethod.GET)
	public ModelAndView deleteBill(HttpServletRequest request, @RequestParam(value = "id", required = true) Long id,
			final Model model) {
		try {
			quotationService.deleteQuotation(id);
			model.addAttribute("msg", "Deleted Successfully");
		} catch (CustomException e) {
			model.addAttribute("msg", e.getMessage());
		} catch (Exception e) {
			logger.error("", e);
			model.addAttribute("msg", "Delete failed");
		}
		return new ModelAndView(new RedirectView(RequestConstants.BILL_LIST));
	}

	@RequestMapping(value = RequestConstants.BILL_CALCULATE_TAX, method = RequestMethod.GET)
	public String calculateTax(final Model model, @RequestParam(value = "total", required = true) BigDecimal total,
			@RequestParam(value = "taxIds", required = false) String idString) {
		try {
			List<Long> taxIds = new ArrayList<Long>();
			List<BillTaxVo> billTaxVos = new ArrayList<BillTaxVo>();
			String[] idArray = idString.replace("'", "").split(",");
			BigDecimal taxTotal = BigDecimal.ZERO;
			if (idArray != null)
				for (int i = 0; i < idArray.length; i++) {
					try {
						taxIds.add(new Long(idArray[i]));
					} catch (Exception e) {

					}
				}
			if (taxIds != null && !taxIds.isEmpty()) {
				List<TaxVo> taxVos = taxService.findTaxes(taxIds);
				for (TaxVo taxVo : taxVos) {
					BillTaxVo vo = new BillTaxVo();
					vo.setTaxId(taxVo.getId());
					vo.setTax(taxVo.getTax());
					vo.setPercentage(taxVo.getPercentage());
					if (taxVo.getPercentage().compareTo(BigDecimal.ZERO) != 0 && total.compareTo(BigDecimal.ZERO) != 0)
						vo.setAmount(total.multiply(taxVo.getPercentage()).divide(new BigDecimal(100), 2,
								RoundingMode.HALF_EVEN));
					taxTotal = taxTotal.add(vo.getAmount());
					billTaxVos.add(vo);
				}
			}
			model.addAttribute("taxTotal", taxTotal);
			model.addAttribute("taxVos", billTaxVos);
			model.addAttribute("netTotal", total.add(taxTotal));
		} catch (Exception e) {
			logger.error("", e);
		}
		return PageNameConstants.BILL_CALCULATE_TAX;
	}

	@RequestMapping(value = RequestConstants.BILL_REPORT, method = RequestMethod.GET)
	public String bonusReport(final ModelMap map, @RequestParam(value = "format", required = true) String format,
			@RequestParam(value = "fromDate", required = false) String fromDate,
			@RequestParam(value = "toDate", required = false) String toDate,
			@RequestParam(value = "customerId", required = false) Long customerId,
			@RequestParam(value = "paymentModeId", required = false) Long paymentModeId,
			@RequestParam(value = "serviceProviderId", required = false) Long serviceProviderId) {
		try {
			List<QuotationVo> vos = quotationService.findQuotationsWithinDateRange(fromDate, toDate, customerId,
					paymentModeId, serviceProviderId);
			Integer slNo = 1;
			QuotationVo totalVo = new QuotationVo();
			totalVo.setReferenceNumber("Total");
			for (QuotationVo vo : vos) {
				vo.setSlNo(slNo);
				slNo++;
				totalVo.setNetTotal(totalVo.getNetTotal().add(vo.getNetTotal()));
				totalVo.setServiceProviderContribution(
						totalVo.getServiceProviderContribution().add(vo.getServiceProviderContribution()));
				totalVo.setGrandTotal(totalVo.getGrandTotal().add(vo.getGrandTotal()));
				totalVo.setTaxTotal(totalVo.getTaxTotal().add(vo.getTaxTotal()));
			}
			vos.add(totalVo);
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			parameterMap.put("datasource", new JREmptyDataSource());
			parameterMap.put("ITEMS", new JRBeanCollectionDataSource(vos));
			String title = "TheRoots - Bill History";
			parameterMap.put("REPORT_TITLE", title);
			map.addAllAttributes(parameterMap);
		} catch (Exception e) {
			logger.error("", e);
		}
		if (format.equals("pdf"))
			return "billHistoryPdf";
		else
			return "billHistoryXls";
	}

	@RequestMapping(value = RequestConstants.DELETED_BILLS, method = RequestMethod.GET)
	public String deleteBills(final Model model, HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			String endDate = DateFormatter.convertDateToString(calendar.getTime());
			calendar.set(Calendar.DATE, -7);
			String startDate = DateFormatter.convertDateToString(calendar.getTime());
			List<QuotationVo> vos = quotationService.findDeletedQuotations(startDate, endDate);
			BigDecimal total = BigDecimal.ZERO;
			BigDecimal discoundTotal = BigDecimal.ZERO;
			BigDecimal taxableTotal = BigDecimal.ZERO;
			BigDecimal taxTotal = BigDecimal.ZERO;
			BigDecimal netTotal = BigDecimal.ZERO;
			BigDecimal serviceProviderContributionTotal = BigDecimal.ZERO;
			for (QuotationVo vo : vos) {
				total = total.add(vo.getGrandTotal());
				discoundTotal = discoundTotal.add(vo.getDiscountAmount());
				taxableTotal = taxableTotal.add(vo.getTaxableValue());
				taxTotal = taxTotal.add(vo.getTaxTotal());
				netTotal = netTotal.add(vo.getNetTotal());
				if (vo.getServiceProviderContribution() != null)
					serviceProviderContributionTotal = serviceProviderContributionTotal
							.add(vo.getServiceProviderContribution());
			}
			model.addAttribute("startDate", startDate);
			model.addAttribute("endDate", endDate);
			model.addAttribute("serviceProviderContributionTotal", serviceProviderContributionTotal);
			model.addAttribute("netTotal", netTotal);
			model.addAttribute("taxTotal", taxTotal);
			model.addAttribute("discoundTotal", discoundTotal);
			model.addAttribute("taxableTotal", taxableTotal);
			model.addAttribute("total", total);
			model.addAttribute("bills", vos);
			model.addAttribute("customers", customerService.findAllCustomers());
			model.addAttribute("paymentModes", paymentModeService.findAllPaymentModes());
			model.addAttribute("serviceProviders", serviceProviderService.findAllServiceProviders());
		} catch (Exception e) {
			logger.error("QUOTATION : ", e);
		}
		return PageNameConstants.DELETED_BILLS;
	}
}
