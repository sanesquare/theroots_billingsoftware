package com.billingSoftware.web.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.billingSoftware.web.constants.PageNameConstants;
import com.billingSoftware.web.constants.RequestConstants;
import com.billingSoftware.web.execptions.CustomException;
import com.billingSoftware.web.service.TaxService;
import com.billingSoftware.web.vo.TaxVo;

/**
 * @author Shamsheer
 * @since Jun 16, 2017
 */
@Controller
public class TaxController {

	/**
	 * Log
	 */
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private TaxService taxService;

	@RequestMapping(value = RequestConstants.TAX, method = RequestMethod.GET)
	public String homePge(final Model model, HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("taxes", taxService.findAllTaxes());
		} catch (Exception e) {
			logger.error("TAX : ", e);
		}
		return PageNameConstants.TAX;
	}

	@RequestMapping(value = RequestConstants.TAX_LIST, method = RequestMethod.GET)
	public String taxList(final Model model, HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("taxes", taxService.findAllTaxes());
		} catch (CustomException e) {
			model.addAttribute("msg", e.getMessage());
		} catch (Exception e) {
			logger.error("TAX : ", e);
		}
		return PageNameConstants.TAX_LIST;
	}

	@RequestMapping(value = RequestConstants.TAX_SAVE, method = RequestMethod.POST)
	public ModelAndView saveTax(final Model model, HttpServletRequest request,
			@RequestBody TaxVo vo) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			taxService.saveTax(vo);
			model.addAttribute("msg", "Saved successfully");
		} catch (CustomException e) {
			model.addAttribute("msg", e.getMessage());
		} catch (Exception e) {
			model.addAttribute("msg", "Something went wrong..!");
			logger.error("TAX : ", e);
		}
		return new ModelAndView(new RedirectView(RequestConstants.TAX_LIST));
	}

	@RequestMapping(value = RequestConstants.TAX_DELETE, method = RequestMethod.GET)
	public ModelAndView taxDelete(final Model model,
			HttpServletRequest request,
			@RequestParam(value = "id", required = true) Long id) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			taxService.deleteTax(id);
			model.addAttribute("msg", "Deleted successfully");
		} catch (CustomException e) {
			model.addAttribute("msg", e.getMessage());
		} catch (Exception e) {
			model.addAttribute("msg", "Something went wrong..!");
			logger.error("TAX : ", e);
		}
		return new ModelAndView(new RedirectView(RequestConstants.TAX_LIST));
	}
}
