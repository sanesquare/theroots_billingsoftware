package com.billingSoftware.web.controller;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.billingSoftware.web.constants.PageNameConstants;
import com.billingSoftware.web.constants.RequestConstants;
import com.billingSoftware.web.logger.Log;
import com.billingSoftware.web.service.CustomerService;
import com.billingSoftware.web.service.SMSService;
import com.billingSoftware.web.vo.SMSVo;

/**
 * 
 * @author Suhail
 * @since Jun 17, 2017
 */

@Controller
public class SMSController {

	@Log
	private static Logger logger;

	@Autowired
	CustomerService customerSerice;

	@Autowired
	SMSService smsService;

	@RequestMapping(value = RequestConstants.SMS_HOME)
	public String smsHomePage(final Model model) {
		try {
			model.addAttribute("customers", customerSerice.findAllCustomers());
		} catch (Exception e) {
			logger.error("", e);
		}
		return PageNameConstants.SMS_HOME;
	}
	
	@RequestMapping(value=RequestConstants.SEND_SMS, method=RequestMethod.POST)
	public @ResponseBody Boolean sendSMS(
			@RequestParam(value = "ids[]", required=false) List<String> ids,
			@RequestParam(value = "message") String message, @RequestParam(value="others", required=false)String others) {
		try {
			if(ids==null && others==null)
				throw(new Exception("At least on recepient should be selected"));
			smsService.sendSMS(ids, message, others);

			return true;
		} catch (Exception e) {
			logger.error("", e);
			return false;
		}
	}
	
	@RequestMapping(value=RequestConstants.SMS_HISTORY)
	public String smsHistory(final Model model){
		try{
			model.addAttribute("histories", smsService.findAllSMShistory());
		}catch(Exception e){
			logger.error("", e);
		}
		return PageNameConstants.SMS_HISTORY;
	}
}
