package com.billingSoftware.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.billingSoftware.web.constants.PageNameConstants;
import com.billingSoftware.web.constants.RequestConstants;
import com.billingSoftware.web.logger.Log;
import com.billingSoftware.web.service.ProductService;
import com.billingSoftware.web.service.ServiceProviderService;
import com.billingSoftware.web.vo.ProductVo;

@Controller
public class ProductsController {

	@Log
	private Logger logger;

	@Autowired
	private ProductService productService;

	@Autowired
	private ServiceProviderService serviceProviderService;

	@RequestMapping(value = RequestConstants.PRODUCTS, method = RequestMethod.GET)
	public String homePge(final Model model, HttpServletRequest request) {
		String msg = request.getParameter("msg");
		model.addAttribute("msg", msg);
		try {
			model.addAttribute("products", productService.findAllProducts());
		} catch (Exception e) {
			logger.error("PRODUCTS : ", e);
		}
		return PageNameConstants.PRODUCTS;
	}

	@RequestMapping(value = RequestConstants.PRODUCTS_POP_UP, method = RequestMethod.POST)
	public String productPopUp(final Model model,
			@RequestBody ProductVo productVo) {
		try {
			Map<Long, BigDecimal> idsAndQuantities = new LinkedHashMap<Long, BigDecimal>();
			for (String idsAndQuantity : productVo.getIdsAndQuantities()) {
				String[] split = idsAndQuantity.split("!!!");
				idsAndQuantities.put(new Long(split[0]), new BigDecimal(
						split[1]));
			}
			List<ProductVo> productVos = productService.findAllProducts();
			for (ProductVo vo : productVos) {
				if (idsAndQuantities.keySet().contains(vo.getId())) {
					vo.setSelected(true);
					vo.setAvailableQty(idsAndQuantities.get(vo.getId()));
				}
			}
			model.addAttribute("products", productVos);
		} catch (Exception e) {
			logger.error("PRODUCTS_POP_UP : ", e);
		}
		return PageNameConstants.PRODUCT_POP_UP;
	}

	@RequestMapping(value = RequestConstants.PRODUCTS_FIND, method = RequestMethod.POST)
	public String findProducts(final Model model,
			@RequestBody ProductVo productVo) {
		try {
			if (productVo.getIdsAndQuantities() != null
					&& !productVo.getIdsAndQuantities().isEmpty()) {
				Map<Long, BigDecimal> idsAndQuantities = new LinkedHashMap<Long, BigDecimal>();
				for (String idsAndQuantity : productVo.getIdsAndQuantities()) {
					String[] split = idsAndQuantity.split("!!!");
					idsAndQuantities.put(new Long(split[0]), new BigDecimal(
							split[1]));
				}
				List<ProductVo> productVos = productService
						.findProducts(new ArrayList<Long>(idsAndQuantities
								.keySet()));
				for (ProductVo vo : productVos) {
					vo.setAvailableQty(idsAndQuantities.get(vo.getId()));
					vo.setTotal(vo.getAvailableQty().multiply(vo.getRate()));
				}
				model.addAttribute("products", productVos);
			}
			model.addAttribute("count", productVo.getRowCount());
			model.addAttribute("serviceProviders",
					serviceProviderService.findAllServiceProviders());
		} catch (Exception ex) {
			logger.error("PRODUCTS_FIND : ", ex);
		}

		return PageNameConstants.PRODUCT_ROWS;
	}

	@RequestMapping(value = RequestConstants.PRODUCTS_ADD, method = RequestMethod.GET)
	public String addProduct(final Model model,
			@ModelAttribute ProductVo productVo) {
		try {

		} catch (Exception e) {
			logger.error("PRODUCTS_ADD : ", e);
		}
		return PageNameConstants.PRODUCT_ADD;
	}

	@RequestMapping(value = RequestConstants.PRODUCT_SAVE, method = RequestMethod.POST)
	public String saveProduct(final Model model,
			@ModelAttribute ProductVo productVo) {
		String msg = "";
		try {
			productService.saveProduct(productVo);
			msg = "Product Saved Successfully";
		} catch (Exception e) {
			msg = "Product Save Failed";
			logger.error("PRODUCT_SAVE : ", e);
		}
		return "redirect:" + RequestConstants.PRODUCTS + "?msg=" + msg;
	}

	@RequestMapping(value = RequestConstants.PRODUCTS_EDIT, method = RequestMethod.GET)
	public String editProduct(final Model model,
			@RequestParam(value = "id", required = true) Long id) {
		try {
			model.addAttribute("productVo", productService.findProductById(id));
		} catch (Exception e) {
			logger.error("PRODUCTS_ADD : ", e);
		}
		return PageNameConstants.PRODUCT_ADD;
	}

	@RequestMapping(value = RequestConstants.PRODUCTS_DELETE, method = RequestMethod.GET)
	public @ResponseBody String deleteProduct(final Model model,
			@RequestParam(value = "id", required = true) Long id) {
		try {
			productService.deleteProduct(id);
			model.addAttribute("msg", "Product Deleted Successfully");
		} catch (Exception e) {
			model.addAttribute("msg", "Product Delete Failed");
			logger.error("PRODUCTS_ADD : ", e);
			return "Product Delete Failed";
		}
		return "Product Deleted Successfully";
	}

	@RequestMapping(value = RequestConstants.PRODUCT_LIST, method = RequestMethod.GET)
	public String productList(final Model model, HttpServletRequest request) {
		String msg = request.getParameter("msg");
		model.addAttribute("msg", msg);
		try {
			model.addAttribute("products", productService.findAllProducts());
		} catch (Exception e) {
			logger.error("PRODUCTS : ", e);
		}
		return PageNameConstants.PRODUCT_LIST;
	}

}
