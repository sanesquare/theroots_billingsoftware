package com.billingSoftware.web.controller;

import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.billingSoftware.web.constants.PageNameConstants;
import com.billingSoftware.web.constants.RequestConstants;
import com.billingSoftware.web.logger.Log;

@Controller
public class QuotationReportController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;
	
	@RequestMapping(value = RequestConstants.QUOTATION_REPORTS, method = RequestMethod.GET)
	public String homePge(final Model model) {
		try {
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("homePge : ",e);
		}
		return PageNameConstants.QUOTATION_REPORTS;
	}
}
