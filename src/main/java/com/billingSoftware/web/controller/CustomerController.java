package com.billingSoftware.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.billingSoftware.web.constants.PageNameConstants;
import com.billingSoftware.web.constants.RequestConstants;
import com.billingSoftware.web.execptions.CustomException;
import com.billingSoftware.web.service.CustomerService;
import com.billingSoftware.web.vo.CustomerVo;

/**
 * @author Shamsheer
 * @since Jun 16, 2017
 */
@Controller
public class CustomerController {

	/**
	 * Log
	 */
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = RequestConstants.CUSTOMER, method = RequestMethod.GET)
	public String homePge(final Model model, HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("customers", customerService.findAllCustomers());
		} catch (Exception e) {
			logger.error("CUSTOMER : ", e);
		}
		return PageNameConstants.CUSTOMER;
	}

	@RequestMapping(value = RequestConstants.CUSTOMER_LIST, method = RequestMethod.GET)
	public String customerList(final Model model, HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("customers", customerService.findAllCustomers());
		} catch (CustomException e) {
			model.addAttribute("msg", e.getMessage());
		} catch (Exception e) {
			logger.error("CUSTOMER : ", e);
		}
		return PageNameConstants.CUSTOMER_LIST;
	}

	@RequestMapping(value = RequestConstants.CUSTOMER_SAVE, method = RequestMethod.POST)
	public ModelAndView saveTax(final Model model, HttpServletRequest request,
			@RequestBody CustomerVo vo) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			customerService.saveCustomer(vo);
			model.addAttribute("msg", "Saved successfully");
		} catch (CustomException e) {
			model.addAttribute("msg", e.getMessage());
		} catch (Exception e) {
			model.addAttribute("msg", "Something went wrong..!");
			logger.error("CUSTOMER : ", e);
		}
		return new ModelAndView(
				new RedirectView(RequestConstants.CUSTOMER_LIST));
	}

	@RequestMapping(value = RequestConstants.CUSTOMER_DELETE, method = RequestMethod.GET)
	public ModelAndView customerDelete(final Model model,
			HttpServletRequest request,
			@RequestParam(value = "id", required = true) Long id) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			customerService.deleteCustomer(id);
			model.addAttribute("msg", "Deleted successfully");
		} catch (CustomException e) {
			model.addAttribute("msg", e.getMessage());
		} catch (Exception e) {
			model.addAttribute("msg", "Something went wrong..!");
			logger.error("CUSTOMER : ", e);
		}
		return new ModelAndView(
				new RedirectView(RequestConstants.CUSTOMER_LIST));
	}

	/*
	 * @RequestMapping(value = RequestConstants.CUSTOMER_ADD_POP, method =
	 * RequestMethod.GET) public String customerAddPop() { return
	 * PageNameConstants.CUSTOMER_ADD_POP; }
	 */

	@RequestMapping(value = RequestConstants.CUSTOMER_ADD_POP, method = RequestMethod.GET)
	public @ResponseBody CustomerVo customerAddPop(
			@RequestParam(value = "custId", required = true) Long id) {
		CustomerVo vo = new CustomerVo();
		try {
			vo = customerService.findCustomer(id);
		} catch (Exception e) {
			logger.error("", e);
		}
		return vo;
	}

	@RequestMapping(value = RequestConstants.CUSTOMER_ADD_POP, method = RequestMethod.POST)
	public String customerSaveFromPop(final Model model,
			HttpServletRequest request, @RequestBody CustomerVo vo) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("id", customerService.saveCustomer(vo));
			model.addAttribute("msg", "Saved successfully");
			model.addAttribute("customers", customerService.findAllCustomers());
		} catch (CustomException e) {
			model.addAttribute("msg", e.getMessage());
		} catch (Exception e) {
			model.addAttribute("msg", "Something went wrong..!");
			logger.error("CUSTOMER : ", e);
		}
		return PageNameConstants.CUSTOMER_SELECT_BOX;
	}
}
