package com.billingSoftware.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.billingSoftware.web.constants.PageNameConstants;
import com.billingSoftware.web.constants.RequestConstants;
import com.billingSoftware.web.execptions.CustomException;
import com.billingSoftware.web.service.ServiceProviderService;
import com.billingSoftware.web.vo.CustomerVo;
import com.billingSoftware.web.vo.ServiceProviderVo;

/**
 * @author Shamsheer
 * @since Jul 13, 2017
 */
@Controller
public class ServiceProviderController {

	/**
	 * Log
	 */
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ServiceProviderService serviceProviderService;

	@RequestMapping(value = RequestConstants.SERVICE_PROVIDER, method = RequestMethod.GET)
	public String homePge(final Model model, HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("serviceProviders",
					serviceProviderService.findAllServiceProviders());
		} catch (Exception e) {
			logger.error("SERVICE_PROVIDER : ", e);
		}
		return PageNameConstants.SERVICE_PROVIDER;
	}

	@RequestMapping(value = RequestConstants.SERVICE_PROVIDER_LIST, method = RequestMethod.GET)
	public String serviceProviderList(final Model model,
			HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			model.addAttribute("serviceProviders",
					serviceProviderService.findAllServiceProviders());
		} catch (CustomException e) {
			model.addAttribute("msg", e.getMessage());
		} catch (Exception e) {
			logger.error("SERVICE_PROVIDER : ", e);
		}
		return PageNameConstants.SERVICE_PROVIDER_LIST;
	}

	@RequestMapping(value = RequestConstants.SERVICE_PROVIDER_SAVE, method = RequestMethod.POST)
	public ModelAndView saveServiceProvider(final Model model,
			HttpServletRequest request, @RequestBody ServiceProviderVo vo) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			serviceProviderService.saveServiceProvider(vo);
			model.addAttribute("msg", "Saved successfully");
		} catch (CustomException e) {
			model.addAttribute("msg", e.getMessage());
		} catch (Exception e) {
			model.addAttribute("msg", "Something went wrong..!");
			logger.error("SERVICE_PROVIDER : ", e);
		}
		return new ModelAndView(new RedirectView(
				RequestConstants.SERVICE_PROVIDER_LIST));
	}

	@RequestMapping(value = RequestConstants.SERVICE_PROVIDER_DELETE, method = RequestMethod.GET)
	public ModelAndView serviceProviderDelete(final Model model,
			HttpServletRequest request,
			@RequestParam(value = "id", required = true) Long id) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			serviceProviderService.deleteServiceProvider(id);
			model.addAttribute("msg", "Deleted successfully");
		} catch (CustomException e) {
			model.addAttribute("msg", e.getMessage());
		} catch (Exception e) {
			model.addAttribute("msg", "Something went wrong..!");
			logger.error("SERVICE_PROVIDER : ", e);
		}
		return new ModelAndView(new RedirectView(
				RequestConstants.SERVICE_PROVIDER_LIST));
	}

}
