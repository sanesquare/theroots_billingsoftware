package com.billingSoftware.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.billingSoftware.web.constants.PageNameConstants;
import com.billingSoftware.web.constants.RequestConstants;
import com.billingSoftware.web.logger.Log;
import com.billingSoftware.web.service.UserService;
import com.billingSoftware.web.vo.UserVo;

@Controller
@SessionAttributes({ "username" })
public class SettingsController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private UserService userService;

	@RequestMapping(value = RequestConstants.SETTINGS, method = RequestMethod.GET)
	public String homePge(final Model model, HttpServletRequest request, @ModelAttribute("username") String username) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			UserVo userVo = new UserVo();
			userVo.setUsername(username);
			model.addAttribute("userVo", userVo);
		} catch (Exception e) {
			model.addAttribute("msg", "Oops.. Something Went Wrong.");
			e.printStackTrace();
			logger.error("SETTINGS : ", e);
		}
		return PageNameConstants.SETTINGS;
	}

	@RequestMapping(value = RequestConstants.UPDATE_PASSWORD, method = RequestMethod.POST)
	public String updatePassword(final Model model, @ModelAttribute UserVo userVo) {
		String msg = "";
		try {
			userService.updatePassword(userVo.getUsername(), userVo.getPassword());
			msg = "Password changed successfully";
		} catch (Exception e) {
			logger.error("", e);
		}
		model.addAttribute("msg", msg);
		return "redirect:" + RequestConstants.SETTINGS;
	}

}
