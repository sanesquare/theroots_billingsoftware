package com.billingSoftware.web.controller;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.docx4j.model.datastorage.XPathEnhancerParser.expr_return;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.billingSoftware.web.constants.PageNameConstants;
import com.billingSoftware.web.constants.RequestConstants;
import com.billingSoftware.web.entities.Settings;
import com.billingSoftware.web.logger.Log;
import com.billingSoftware.web.service.UserService;

/**
 * 
 * @author Vinutha
 * @since 20-July-2015
 */
@Controller
@SessionAttributes({ "username" })
public class BaseController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@Autowired
	private UserService userService;

	@RequestMapping(value = RequestConstants.LOGIN_URL, method = RequestMethod.GET)
	public String loginPage(final Model model,
			@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout,
			@RequestParam(value = "invalid", required = false) String invalid) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {

			return "redirect:" + RequestConstants.HOME_URL;
		}
		if (error != null) {
			model.addAttribute("status", "Invalid Username or Password");
		}
		if (logout != null) {
			model.addAttribute("status",
					"You have been logged out successfully.");
		}
		if (invalid != null) {
			model.addAttribute("status",
					"Your Session Expired.Please Do Login.");
		}
		/*
		 * User user =(User)
		 * SecurityContextHolder.getContext().getAuthentication().getPrincipal()
		 * ; System.out.println(user.getUsername() +"Password >>>>> "
		 * +user.getPassword());
		 */
		return PageNameConstants.SIGN_IN;
	}

	@RequestMapping(value = RequestConstants.HOME_URL, method = RequestMethod.GET)
	public String homePge(final Model model, HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			Settings settings = userService.updateFindSettings();
			Date date = new Date();
			if (date.compareTo(settings.getExpiryDate()) > 0) {
				model.addAttribute("expiry_licence",
						"Your license has been expired.");
			} else {
				Long diff = settings.getExpiryDate().getTime() - date.getTime();
				Long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
				if (days < 20) {
					days = days + 1;
					model.addAttribute("warning",
							"Your license will be expired in " + days
									+ " day(s)");
				}
			}
			User user = (User) SecurityContextHolder.getContext()
					.getAuthentication().getPrincipal();
			model.addAttribute("username", user.getUsername());
		} catch (Exception e) {
			logger.error("HOME_URL : ", e);
		}
		return PageNameConstants.HOME_PAGE;
	}

	/*
	 * public static void main(String[] args) { try {
	 * Class.forName("com.mysql.jdbc.Driver"); Connection connection =
	 * DriverManager.getConnection(
	 * "jdbc:mysql://localhost:3306/the_roots_billing_software", "root",
	 * "logonsoft_123"); Statement statement = connection.createStatement();
	 * 
	 * InputStream input = new BufferedInputStream(new FileInputStream(
	 * "C:/Users/Shamsheer's PC/Downloads/Contacts.xlsx")); OPCPackage fs =
	 * OPCPackage.open(input); XSSFWorkbook wb = new XSSFWorkbook(fs); XSSFSheet
	 * sheet = wb.getSheetAt(0); Iterator<Row> rows = sheet.rowIterator();
	 * Integer rowCount = 0; while (rows.hasNext()) { XSSFRow row = (XSSFRow)
	 * rows.next(); Iterator cells = row.cellIterator(); String name = null;
	 * String mobile = null; while (cells.hasNext()) { XSSFCell cell =
	 * (XSSFCell) cells.next(); if (name == null) try { name =
	 * cell.getStringCellValue(); } catch (Exception e) { name = new
	 * BigDecimal(cell.getNumericCellValue()) .toString(); } else try { mobile =
	 * cell.getStringCellValue(); } catch (Exception e) { mobile = new
	 * BigDecimal(cell.getNumericCellValue()) .toString(); } if
	 * (HSSFCell.CELL_TYPE_NUMERIC == cell.getCellType()) {
	 * 
	 * } else if (HSSFCell.CELL_TYPE_STRING == cell.getCellType()) {
	 * 
	 * } } if (rowCount >= 1) { statement .execute(
	 * "INSERT INTO customer (id,customer_name,mobile,reference_number) VALUES("
	 * + rowCount + ",'" + name + "','" + mobile + "','CUST-" + rowCount +
	 * "')"); } rowCount++; }
	 * statement.execute("UPDATE number_property SET number=" + rowCount +
	 * " WHERE id=1"); ResultSet resultSet = statement
	 * .executeQuery("SELECT id,customer_name,mobile,reference_number FROM customer"
	 * ); while (resultSet.next()) { System.out.println(resultSet.getLong("id")
	 * + " > " + resultSet.getString("customer_name") + " > " +
	 * resultSet.getString("mobile") + " > " +
	 * resultSet.getString("reference_number")); } } catch (Exception e) {
	 * e.printStackTrace(); } }
	 */

}
