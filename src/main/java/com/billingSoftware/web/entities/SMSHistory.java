package com.billingSoftware.web.entities;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.billingSoftware.web.interfaces.AuditEntity;

/**
 * 
 * @author Suhail
 * @since Jun 17, 2017
 */

@Entity
@Table(name = "sms_history")
public class SMSHistory implements AuditEntity, Serializable {
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@Column(name="message", columnDefinition="TEXT")
	private String message;
	
	@Column(name="response", columnDefinition="TEXT")
	private String response;
	
	@Column(name="customer_ids")
	private String customerIDs;
	
	@Column(name="date_time")
	private Timestamp date;
	
	@Column(name="other_numbers", columnDefinition="TEXT")
	private String otherNumbers;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getCustomerIDs() {
		return customerIDs;
	}

	public void setCustomerIDs(String customerIDs) {
		this.customerIDs = customerIDs;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getOtherNumbers() {
		return otherNumbers;
	}

	public void setOtherNumbers(String otherNumbers) {
		this.otherNumbers = otherNumbers;
	}
}
