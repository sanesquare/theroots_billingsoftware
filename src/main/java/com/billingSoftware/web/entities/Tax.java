package com.billingSoftware.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.billingSoftware.web.interfaces.AuditEntity;

/**
 * @author Shamsheer
 * @since Jun 15, 2017
 */
@Entity
@Table(name = "tax")
public class Tax implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4955642347129858675L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "updated_by")
	private String updatedBy;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updated_on;

	@Column(name = "tax")
	private String tax;

	@Column(name = "percentage")
	private BigDecimal percentage;

	@OneToMany(mappedBy = "tax", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<BillTax> billTaxs = new HashSet<BillTax>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdated_on() {
		return updated_on;
	}

	public void setUpdated_on(Date updated_on) {
		this.updated_on = updated_on;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public BigDecimal getPercentage() {
		return percentage;
	}

	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<BillTax> getBillTaxs() {
		return billTaxs;
	}

	public void setBillTaxs(Set<BillTax> billTaxs) {
		this.billTaxs = billTaxs;
	}

}
