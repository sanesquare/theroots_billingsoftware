package com.billingSoftware.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

import com.billingSoftware.web.interfaces.AuditEntity;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 9, 2015
 */
@Entity
@Table(name = "bill")
public class Bill implements AuditEntity, Serializable {

	/**
	 * 
	 */
	private static long serialVersionUID = 3272470423272291480L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "updated_by")
	private String updatedBy;

	@Column(name = "created_on")
	private Date createdOn;

	@Column(name = "updated_on")
	private Date updated_on;

	@Column(name = "bill_date")
	private Date date;

	@Column(name = "reference_number")
	private String referenceNumber;

	@Column(name = "updated_reference_number")
	private String updatedReferenceNumber;

	@Column(name = "total")
	private BigDecimal total;

	@Column(name = "tax_total")
	private BigDecimal taxTotal;

	@Column(name = "discount_amount")
	private BigDecimal discountAmount;

	@Column(name = "discount_percentage")
	private BigDecimal discountPercentage;

	@Column(name = "net_total")
	private BigDecimal netTotal;

	@OneToMany(mappedBy = "bill", cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("id")
	private Set<BillItem> billItems = new HashSet<BillItem>();

	@ManyToOne
	private Customer customer;

	@OneToMany(mappedBy = "bill", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<BillTax> billTaxs = new HashSet<BillTax>();

	@ManyToOne
	private PaymentMode paymentMode;

	@ManyToOne
	private ServiceProvider serviceProvider;

	@Column(name = "is_deleted")
	private Boolean isDeleted;
	
	@Column(name = "is_printed")
	private Boolean isPrinted;

	public Set<BillItem> getQuotationItems() {
		return billItems;
	}

	public void setQuotationItems(Set<BillItem> billItems) {
		this.billItems = billItems;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdated_on() {
		return updated_on;
	}

	public void setUpdated_on(Date updated_on) {
		this.updated_on = updated_on;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Set<BillItem> getBillItems() {
		return billItems;
	}

	public void setBillItems(Set<BillItem> billItems) {
		this.billItems = billItems;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Set<BillTax> getBillTaxs() {
		return billTaxs;
	}

	public void setBillTaxs(Set<BillTax> billTaxs) {
		this.billTaxs = billTaxs;
	}

	public BigDecimal getTaxTotal() {
		return taxTotal;
	}

	public void setTaxTotal(BigDecimal taxTotal) {
		this.taxTotal = taxTotal;
	}

	public BigDecimal getNetTotal() {
		return netTotal;
	}

	public void setNetTotal(BigDecimal netTotal) {
		this.netTotal = netTotal;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(BigDecimal discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public String getUpdatedReferenceNumber() {
		return updatedReferenceNumber;
	}

	public void setUpdatedReferenceNumber(String updatedReferenceNumber) {
		this.updatedReferenceNumber = updatedReferenceNumber;
	}

	public PaymentMode getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(PaymentMode paymentMode) {
		this.paymentMode = paymentMode;
	}

	public ServiceProvider getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(ServiceProvider serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	public static void setSerialversionuid(long serialversionuid) {
		serialVersionUID = serialversionuid;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public static void setSerialVersionUID(long serialVersionUID) {
		Bill.serialVersionUID = serialVersionUID;
	}

	public Boolean getIsPrinted() {
		return isPrinted;
	}

	public void setIsPrinted(Boolean isPrinted) {
		this.isPrinted = isPrinted;
	}

}
