package com.billingSoftware.web.service;

import java.util.List;

import com.billingSoftware.web.vo.ServiceProviderVo;

/**
 * @author Shamsheer
 * @since Jul 13, 2017
 */
public interface ServiceProviderService {

	/**
	 * method to save service provider
	 * 
	 * @param vo
	 * @return
	 */
	public Long saveServiceProvider(ServiceProviderVo vo);

	/**
	 * method to delete service provider
	 * 
	 * @param id
	 */
	public void deleteServiceProvider(Long id);

	/**
	 * method to list all service providers
	 * 
	 * @return
	 */
	public List<ServiceProviderVo> findAllServiceProviders();
}
