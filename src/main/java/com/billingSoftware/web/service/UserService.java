package com.billingSoftware.web.service;

import com.billingSoftware.web.entities.Settings;

/**
 * @author Shamsheer
 * @since Jun 17, 2017
 */
public interface UserService {

	/**
	 * method to update password
	 * 
	 * @param username
	 * @param password
	 */
	public void updatePassword(String username, String password);
	
	/**
	 * @return
	 */
	public Settings updateFindSettings();
}
