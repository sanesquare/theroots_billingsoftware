package com.billingSoftware.web.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.billingSoftware.web.dao.UserDao;
import com.billingSoftware.web.entities.Settings;
import com.billingSoftware.web.service.UserService;

/**
 * @author Shamsheer
 * @since Jun 17, 2017
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao dao;

	public void updatePassword(String username, String password) {
		dao.updatePassword(username, password);
	}

	public Settings updateFindSettings() {
		return dao.updateFindSettings();
	}
}
