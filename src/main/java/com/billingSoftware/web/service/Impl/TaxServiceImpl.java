package com.billingSoftware.web.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.billingSoftware.web.dao.TaxDao;
import com.billingSoftware.web.service.TaxService;
import com.billingSoftware.web.vo.TaxVo;

/**
 * @author Shamsheer
 * @since Jun 16, 2017
 */
@Service
public class TaxServiceImpl implements TaxService {

	@Autowired
	private TaxDao dao;

	public void saveTax(TaxVo vo) {
		dao.saveTax(vo);
	}

	public void deleteTax(Long id) {
		dao.deleteTax(id);
	}

	public TaxVo findTax(Long id) {
		return dao.findTax(id);
	}

	public List<TaxVo> findAllTaxes() {
		return dao.findAllTaxes();
	}

	public List<TaxVo> findTaxes(List<Long> taxIds) {
		return dao.findTaxes(taxIds);
	}
}
