package com.billingSoftware.web.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.billingSoftware.web.dao.ServiceProviderDao;
import com.billingSoftware.web.service.ServiceProviderService;
import com.billingSoftware.web.vo.ServiceProviderVo;

/**
 * @author Shamsheer
 * @since Jul 13, 2017
 */
@Service
public class ServiceProviderServiceImpl implements ServiceProviderService {

	@Autowired
	private ServiceProviderDao dao;

	public Long saveServiceProvider(ServiceProviderVo vo) {
		return dao.saveServiceProvider(vo);
	}

	public void deleteServiceProvider(Long id) {
		dao.deleteServiceProvider(id);
	}

	public List<ServiceProviderVo> findAllServiceProviders() {
		return dao.findAllServiceProviders();
	}

}
