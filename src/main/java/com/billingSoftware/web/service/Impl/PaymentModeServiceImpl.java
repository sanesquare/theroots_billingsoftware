package com.billingSoftware.web.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.billingSoftware.web.dao.PaymentModeDao;
import com.billingSoftware.web.service.PaymentModeService;
import com.billingSoftware.web.vo.PaymentModeVo;

/**
 * @author Shamsheer
 * @since Jul 13, 2017
 */
@Service
public class PaymentModeServiceImpl implements PaymentModeService {

	@Autowired
	private PaymentModeDao dao;

	public List<PaymentModeVo> findAllPaymentModes() {
		return dao.findAllPaymentModes();
	}

}
