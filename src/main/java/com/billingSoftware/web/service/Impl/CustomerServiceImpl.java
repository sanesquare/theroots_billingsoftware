package com.billingSoftware.web.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.billingSoftware.web.dao.CustomerDao;
import com.billingSoftware.web.service.CustomerService;
import com.billingSoftware.web.vo.CustomerVo;

/**
 * @author Shamsheer
 * @since Jun 16, 2017
 */
@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDao dao;

	public Long saveCustomer(CustomerVo vo) {
		return dao.saveCustomer(vo);
	}

	public void deleteCustomer(Long id) {
		dao.deleteCustomer(id);
	}

	public CustomerVo findCustomer(Long id) {
		return dao.findCustomer(id);
	}

	public List<CustomerVo> findAllCustomers() {
		return dao.findAllCustomers();
	}

}
