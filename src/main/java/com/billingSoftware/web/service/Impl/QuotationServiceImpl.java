package com.billingSoftware.web.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import com.billingSoftware.web.dao.QuotationDao;
import com.billingSoftware.web.service.QuotationService;
import com.billingSoftware.web.vo.CustomerVo;
import com.billingSoftware.web.vo.QuotationVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 9, 2015
 */
@Service
public class QuotationServiceImpl implements QuotationService {

	@Autowired
	private QuotationDao dao;

	public Long saveOrUpdate(QuotationVo quotationVo) {
		return dao.saveOrUpdate(quotationVo);
	}

	public void deleteQuotation(Long id) {
		dao.deleteQuotation(id);
	}

	public QuotationVo findQuotation(Long id) {
		return dao.updateFindQuotation(id);
	}

	public List<QuotationVo> listAllQuotations() {
		return dao.listAllQuotations();
	}

	public List<QuotationVo> findQuotationsWithinDateRange(String startDate, String endDate, Long customerId,
			Long paymentModeId, Long serviceProviderId) {
		return dao.findQuotationsWithinDateRange(startDate, endDate, customerId, paymentModeId, serviceProviderId);
	}

	public void updateReferenceNUmber() {
		dao.updateReferenceNUmber();
	}

	public void updatePrintStatus(Long id) {
		dao.updatePrintStatus(id);
	}

	public List<QuotationVo> findDeletedQuotations(String startDate, String endDate) {
		return dao.findDeletedQuotations(startDate, endDate);
	}

	public static void main(String[] args) {
		System.out.println(DigestUtils.md5DigestAsHex("Anand_123".getBytes()));
	}
}
