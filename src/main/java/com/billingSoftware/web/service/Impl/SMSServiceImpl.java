package com.billingSoftware.web.service.Impl;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.billingSoftware.web.dao.CustomerDao;
import com.billingSoftware.web.dao.SMSDao;
import com.billingSoftware.web.service.SMSService;
import com.billingSoftware.web.util.SMSUtil;
import com.billingSoftware.web.vo.CustomerVo;
import com.billingSoftware.web.vo.SMSHistoryVo;
import com.billingSoftware.web.vo.SMSVo;

@Service
public class SMSServiceImpl implements SMSService {
	
	@Autowired
	SMSDao dao;
	
	@Autowired
	CustomerDao customerDao;

	public Boolean sendSMS(List<String> ids, String message, String others) {
		SMSVo vo = new SMSVo();
		if(ids!=null){
			for(String id : ids){
				CustomerVo customerVo = customerDao.findCustomer(Long.parseLong(id));
				vo.getNumbers().add(customerVo.getMobile());
			}
		}
		if(others!=null){
			List<String> otherNumbers = Arrays.asList(others.split(","));
			for(String other: otherNumbers){
				if(!other.equals("") && !other.equals(','))
					vo.getNumbers().add(other);
			}
		}
		
		vo.setMessage(message);
		String response = SMSUtil.sendSMS(vo);
		if(response.equals(""))
			return false;
		vo.setResponse(response);
		vo.setCustomerIds(StringUtils.join(ids, ','));
		vo.setOtherNumbers(others);
		dao.saveSentSMS(vo);
		return true;
	}

	public void saveSendSMS(SMSVo vo) {
		dao.saveSentSMS(vo);
	}
	
	public List<SMSHistoryVo> findAllSMShistory(){
		return dao.findAllSMShistory();
	}

}
