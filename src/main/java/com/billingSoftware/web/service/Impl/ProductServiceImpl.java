package com.billingSoftware.web.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.billingSoftware.web.dao.ProductDao;
import com.billingSoftware.web.service.ProductService;
import com.billingSoftware.web.vo.ProductVo;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductDao dao;
	
	public void saveProduct(ProductVo productVo) {
		dao.saveProduct(productVo);
	}

	public void deleteProduct(Long id) {
		dao.deleteProduct(id);
	}

	public ProductVo findProductById(Long id) {
		return dao.findProductById(id);
	}

	public List<ProductVo> findAllProducts() {
		return dao.findAllProducts();
	}

	public List<ProductVo> findProducts(List<Long> ids) {
		return dao.findProducts(ids);
	}

}
