package com.billingSoftware.web.service;

import java.util.List;

import com.billingSoftware.web.vo.SMSHistoryVo;
import com.billingSoftware.web.vo.SMSVo;

public interface SMSService {

	public Boolean sendSMS(List<String> numbers, String message, String others);
	
	public void saveSendSMS(SMSVo vo);
	
	public List<SMSHistoryVo> findAllSMShistory();
}
