package com.billingSoftware.web.service;

import java.util.List;

import com.billingSoftware.web.vo.CustomerVo;
import com.billingSoftware.web.vo.QuotationVo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 9, 2015
 */
public interface QuotationService {

	/**
	 * Method to save or update quotation
	 * 
	 * @param quotationVo
	 */
	public Long saveOrUpdate(QuotationVo quotationVo);

	/**
	 * Method to delete quotation
	 * 
	 * @param id
	 */
	public void deleteQuotation(Long id);

	/**
	 * Method to find quotation
	 * 
	 * @param id
	 * @return
	 */
	public QuotationVo findQuotation(Long id);

	/**
	 * Method to list all quotations
	 * 
	 * @return
	 */
	public List<QuotationVo> listAllQuotations();

	/**
	 * Method to list quotations within date range
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<QuotationVo> findQuotationsWithinDateRange(String startDate,
			String endDate, Long customerId,Long paymentModeId,Long serviceProviderId);

	/**
	 * method to update reference number
	 */
	public void updateReferenceNUmber();
	
	public void updatePrintStatus(Long id);
	
	public List<QuotationVo> findDeletedQuotations(final String startDate, final String endDate);
}
