package com.billingSoftware.web.service;

import java.util.List;

import com.billingSoftware.web.vo.PaymentModeVo;

/**
 * @author Shamsheer
 * @since Jul 13, 2017
 */
public interface PaymentModeService {

	/**
	 * method to find payment modes
	 * 
	 * @return
	 */
	public List<PaymentModeVo> findAllPaymentModes();
}
