package com.billingSoftware.web.service;

import java.util.List;

import com.billingSoftware.web.vo.TaxVo;

/**
 * @author Shamsheer
 * @since Jun 16, 2017
 */
public interface TaxService {

	/**
	 * method to save tax
	 * 
	 * @param vo
	 */
	public void saveTax(TaxVo vo);

	/**
	 * method to delete tax
	 * 
	 * @param id
	 */
	public void deleteTax(Long id);

	/**
	 * method to find tax by id
	 * 
	 * @param id
	 * @return
	 */
	public TaxVo findTax(Long id);

	/**
	 * method to find all taxes
	 * 
	 * @return
	 */
	public List<TaxVo> findAllTaxes();
	
	/**
	 * method to find tax
	 * 
	 * @param taxIds
	 * @return
	 */
	public List<TaxVo> findTaxes(List<Long> taxIds);
}
