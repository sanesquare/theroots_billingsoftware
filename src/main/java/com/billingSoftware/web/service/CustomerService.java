package com.billingSoftware.web.service;

import java.util.List;

import com.billingSoftware.web.vo.CustomerVo;

/**
 * @author Shamsheer
 * @since Jun 16, 2017
 */
public interface CustomerService {

	/**
	 * method to save customer
	 * 
	 * @param vo
	 */
	public Long saveCustomer(CustomerVo vo);

	/**
	 * method to delete customer
	 * 
	 * @param id
	 */
	public void deleteCustomer(Long id);

	/**
	 * method to find customer by id
	 * 
	 * @param id
	 * @return
	 */
	public CustomerVo findCustomer(Long id);

	/**
	 * method to list all customers
	 * 
	 * @return
	 */
	public List<CustomerVo> findAllCustomers();

}
