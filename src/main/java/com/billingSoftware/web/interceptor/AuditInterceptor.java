package com.billingSoftware.web.interceptor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import com.billingSoftware.web.interfaces.AuditEntity;

/**
 * 
 * @author Vinutha
 * @since 20-July-2015
 */
public class AuditInterceptor extends EmptyInterceptor {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1110720040158233063L;

	/**
	 * This method sets the value on save
	 * 
	 * @param entity
	 *            Object
	 * @param id
	 *            Serializable
	 * @param state
	 *            Object[]
	 * @param propertyNames
	 *            String[]
	 * @param types
	 *            Type[]
	 * @return changed
	 */
	@Override
	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
		boolean changed = false;
		User user = null;
		try {
			user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} catch (Exception e) {
		}
		if (entity instanceof AuditEntity) {
			setValue(state, propertyNames, "createdBy", user.getUsername());
			setValue(state, propertyNames, "createdOn", new Date());
			changed = true;
		}
		return changed;
	}

	/**
	 * This method sets the value on update
	 * 
	 * @param entity
	 *            Object
	 * @param id
	 *            Serializable
	 * @param currentState
	 *            Object[]
	 * @param propertyNames
	 *            String[]
	 * @param previousState
	 *            Object[]
	 * @param types
	 *            Type[]
	 * @return changed
	 * @return
	 */
	@Override
	public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState,
			String[] propertyNames, Type[] types) {
		boolean changed = false;
		User user = null;
		try {
			user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} catch (Exception e) {
		}
		if (entity instanceof AuditEntity) {
			setValue(currentState, propertyNames, "updatedBy", user.getUsername());
			setValue(currentState, propertyNames, "updatedOn", new Date());
			changed = true;
		}
		return changed;
	}

	/**
	 * This method sets the value.
	 * 
	 * @param currentState
	 * @param propertyNames
	 * @param propertyToSet
	 * @param value
	 */
	private void setValue(Object[] currentState, String[] propertyNames, String propertyToSet, Object value) {
		int index = Arrays.asList(propertyNames).indexOf(propertyToSet);
		if (index >= 0) {
			currentState[index] = value;
		}
	}
}
