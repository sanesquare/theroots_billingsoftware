package com.billingSoftware.web.security;

import java.io.IOException;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.billingSoftware.web.constants.RequestConstants;


/**
 * 
 * @author Shamsheer
 * @since 30-June-2015
 */
public class KarunaSuccessAuthenticationHandler implements AuthenticationSuccessHandler{

	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		response.sendRedirect(RequestConstants.HOME_URL+"?jsession=FA");
	}

}
