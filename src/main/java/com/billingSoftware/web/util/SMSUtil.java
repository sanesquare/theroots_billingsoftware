package com.billingSoftware.web.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.springframework.util.DigestUtils;

import com.billingSoftware.web.logger.Log;
import com.billingSoftware.web.vo.SMSVo;

/**
 * 
 * @author Suhail
 * @since Jun 17, 2017
 */

public class SMSUtil {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	private static String BASE_URL = "http://alerts.ebensms.com/api/v3/";

	private static String API_KEY = "A35e447822b5a27a172224ae820e51572";

	private static String METHOD = "sms";

	private static String SENDER = "SNOGOL";

	public static String sendSMS(SMSVo vo) {
		try {
			String USER_AGENT = "Mozilla/5.0";
			String urlString = buildURL(vo);

			URL url = new URL(urlString);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String output;
			StringBuffer response = new StringBuffer();

			while ((output = in.readLine()) != null) {
				response.append(output);
			}
			in.close();
			return response.toString();
		} catch (Exception e) {
			logger.info("", e);
		}
		return "";
	}

	private static String buildURL(SMSVo vo) {
		String url = BASE_URL + "?method=" + METHOD + "&sender=" + SENDER
				+ "&api_key=" + API_KEY;
		url = url + "&to=" + StringUtils.join(vo.getNumbers(), ',');
		url = url + "&message=" + vo.getMessage().replace(" ", "%20");
		return url;
	}

	public static void main(String[] args) {
		System.out.println(DigestUtils.md5DigestAsHex(new String("TheRoots").getBytes()));
	}
	
}
