package com.billingSoftware.web.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class SingleTon {

	private Connection connection;
	private static int count = 0;
	private static SingleTon singleTon;

	public static SingleTon getSingleTon() {
		if (count == 0)
			singleTon = new SingleTon();
		return singleTon;
	}

	private SingleTon() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://208.91.198.74:3306/snogop2y_vijayaprakash",
					"snogop2y_vijaya", "vijaya456");
			count++;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Connection getConnection() {
		return connection;
	}

}
