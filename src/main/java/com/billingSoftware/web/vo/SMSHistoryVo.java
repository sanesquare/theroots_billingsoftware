package com.billingSoftware.web.vo;

import java.util.ArrayList;
import java.util.List;

/***
 * 
 * @author Suhail
 * @since Jun 17, 2017
 */

public class SMSHistoryVo {
	
	private Long id;
	
	private String message;
	
	private String reponse;
	
	private String customerNames;
	
	private String dateTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getReponse() {
		return reponse;
	}

	public void setReponse(String reponse) {
		this.reponse = reponse;
	}

	public String getCustomerNames() {
		return customerNames;
	}

	public void setCustomerNames(String customerNames) {
		this.customerNames = customerNames;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
}
