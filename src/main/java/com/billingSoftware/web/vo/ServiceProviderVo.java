package com.billingSoftware.web.vo;

/**
 * @author Shamsheer
 * @since Jul 13, 2017
 */
public class ServiceProviderVo {

	private Long id;

	private String serviceProvider;

	private String mobile;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
}
