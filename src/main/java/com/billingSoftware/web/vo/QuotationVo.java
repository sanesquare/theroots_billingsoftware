package com.billingSoftware.web.vo;

import java.math.BigDecimal;
import java.util.*;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 9, 2015
 */
public class QuotationVo {

	private Integer slNo;
	
	private Boolean isPrinted=false;
	
	private Boolean isDeleted=false;

	private BigDecimal grandTotal = new BigDecimal("0.00");

	private BigDecimal taxTotal = new BigDecimal("0.00");

	private BigDecimal taxableValue = new BigDecimal("0.00");

	private BigDecimal netTotal = new BigDecimal("0.00");

	private BigDecimal discountAmount = new BigDecimal("0.00");

	private BigDecimal discountPercentage = new BigDecimal("0.00");

	private BigDecimal serviceProviderContribution = new BigDecimal("0.00");

	private Long id;

	private String date;

	private String referenceNumber;

	private List<QuotationItemVo> itemVos = new ArrayList<QuotationItemVo>();

	private List<BillTaxVo> taxVos = new ArrayList<BillTaxVo>();

	private Long customerId;

	private String customerName;

	private String mobile;

	private List<Long> taxids = new ArrayList<Long>();

	private Long paymentModeId;

	private String paymentMode;

	private Long serviceProviderId;

	private String serviceProvider;

	public BigDecimal getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public List<QuotationItemVo> getItemVos() {
		return itemVos;
	}

	public void setItemVos(List<QuotationItemVo> itemVos) {
		this.itemVos = itemVos;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public List<BillTaxVo> getTaxVos() {
		return taxVos;
	}

	public void setTaxVos(List<BillTaxVo> taxVos) {
		this.taxVos = taxVos;
	}

	public BigDecimal getTaxTotal() {
		return taxTotal;
	}

	public void setTaxTotal(BigDecimal taxTotal) {
		this.taxTotal = taxTotal;
	}

	public BigDecimal getNetTotal() {
		return netTotal;
	}

	public void setNetTotal(BigDecimal netTotal) {
		this.netTotal = netTotal;
	}

	public List<Long> getTaxids() {
		return taxids;
	}

	public void setTaxids(List<Long> taxids) {
		this.taxids = taxids;
	}

	public Integer getSlNo() {
		return slNo;
	}

	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(BigDecimal discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public Long getPaymentModeId() {
		return paymentModeId;
	}

	public void setPaymentModeId(Long paymentModeId) {
		this.paymentModeId = paymentModeId;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public Long getServiceProviderId() {
		return serviceProviderId;
	}

	public void setServiceProviderId(Long serviceProviderId) {
		this.serviceProviderId = serviceProviderId;
	}

	public String getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	public BigDecimal getTaxableValue() {
		return taxableValue;
	}

	public void setTaxableValue(BigDecimal taxableValue) {
		this.taxableValue = taxableValue;
	}

	public BigDecimal getServiceProviderContribution() {
		return serviceProviderContribution;
	}

	public void setServiceProviderContribution(
			BigDecimal serviceProviderContribution) {
		this.serviceProviderContribution = serviceProviderContribution;
	}

	public Boolean getIsPrinted() {
		return isPrinted;
	}

	public void setIsPrinted(Boolean isPrinted) {
		this.isPrinted = isPrinted;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

}
