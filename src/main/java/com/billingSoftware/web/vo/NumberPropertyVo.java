package com.billingSoftware.web.vo;

/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 9, 2015
 */
public class NumberPropertyVo {

	private String category;

	private String prefix;

	private Long number;

	private String completeString;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}

	public String getCompleteString() {
		return completeString;
	}

	public void setCompleteString(String completeString) {
		this.completeString = completeString;
	}

}