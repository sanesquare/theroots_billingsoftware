package com.billingSoftware.web.vo;

import java.math.BigDecimal;

/**
 * 
 * @author Shamsheer
 * @since Jun 16, 2017
 */
public class TaxVo {

	private Long id;

	private String tax;

	private BigDecimal percentage;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public BigDecimal getPercentage() {
		return percentage;
	}

	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}
}
