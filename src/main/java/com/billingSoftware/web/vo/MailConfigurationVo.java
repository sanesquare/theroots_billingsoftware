package com.billingSoftware.web.vo;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 9, 2015
 */
public class MailConfigurationVo {

	private String userName;
	
	private String password;
	
	private String host;
	
	private String senderId;
	
	private Long portNo;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public Long getPortNo() {
		return portNo;
	}

	public void setPortNo(Long portNo) {
		this.portNo = portNo;
	}

	
}
