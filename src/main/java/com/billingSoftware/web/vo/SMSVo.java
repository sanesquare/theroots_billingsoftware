package com.billingSoftware.web.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Suhail
 * @since Jun 17, 2017
 */

public class SMSVo {

	private List<String> numbers = new ArrayList<String>();
	
	private String message;
	
	private String response;
	
	private String customerIds;
	
	private String otherNumbers;

	public List<String> getNumbers() {
		return numbers;
	}

	public void setNumbers(List<String> numbers) {
		this.numbers = numbers;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getCustomerIds() {
		return customerIds;
	}

	public void setCustomerIds(String customerIds) {
		this.customerIds = customerIds;
	}

	public String getOtherNumbers() {
		return otherNumbers;
	}

	public void setOtherNumbers(String otherNumbers) {
		this.otherNumbers = otherNumbers;
	}
}
