package com.billingSoftware.web.vo;

/**
 * @author Shamsheer
 * @since Jul 13, 2017
 */
public class PaymentModeVo {

	private Long id;

	private String paymentMode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
}
