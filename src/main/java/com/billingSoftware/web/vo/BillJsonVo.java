package com.billingSoftware.web.vo;

import java.io.File;

/**
 * @author Shamsheer
 * @since Jun 20, 2017
 */
public class BillJsonVo {

	private Long id;

	private File file;

	private Boolean print;

	private String url;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public Boolean getPrint() {
		return print;
	}

	public void setPrint(Boolean print) {
		this.print = print;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
