package com.billingSoftware.web.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ProductVo {

	private Integer rowCount;

	private Boolean selected = false;

	List<Long> ids = new ArrayList<Long>();

	List<String> idsAndQuantities = new ArrayList<String>();

	private BigDecimal availableQty = BigDecimal.ZERO;

	private Long id;

	private String productName;

	private String description;

	private BigDecimal rate;

	private String unit;

	private BigDecimal total = new BigDecimal("0.00");

	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public Integer getRowCount() {
		return rowCount;
	}

	public void setRowCount(Integer rowCount) {
		this.rowCount = rowCount;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public List<String> getIdsAndQuantities() {
		return idsAndQuantities;
	}

	public void setIdsAndQuantities(List<String> idsAndQuantities) {
		this.idsAndQuantities = idsAndQuantities;
	}

	public BigDecimal getAvailableQty() {
		return availableQty;
	}

	public void setAvailableQty(BigDecimal availableQty) {
		this.availableQty = availableQty;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

}
