package com.billingSoftware.web.constants;

/**
 * 
 * @author Vinutha
 * @since 20-July-2015
 */
public interface PageNameConstants {

	String SIGN_IN = "login";
	String HOME_PAGE = "home";
	String ORDERS = "orders";
	String ORDER_LIST = "orderList";
	String QUOTATION_LIST = "quotationList";
	String PENDING_QUOTATION = "pendingQuotation";

	String PRODUCTS = "products";
	String PRODUCT_POP_UP = "product_pop";
	String PRODUCT_ROWS = "prodcutRows";
	String PRODUCT_ADD = "addProduct";
	String PRODUCT_LIST = "productList";

	String SEND_VIDEO = "sendVideo";
	String SETTINGS = "settings";
	String QUOTATION_REPORTS = "quotationReports";

	String CUSTOMER_POP = "customerPOp";

	String TAX = "tax";
	String TAX_LIST = "taxList";

	String CUSTOMER = "customer";
	String CUSTOMER_LIST = "customerList";

	String SERVICE_PROVIDER = "serviceProvider";
	String SERVICE_PROVIDER_LIST = "serviceProviderList";

	String SMS_HOME = "smsHome";
	String SMS_HISTORY = "smsHistory";

	String BILL_HISTORY = "billHistory";
	String BILL_LIST = "billList";
	String BILL = "bill";
	String BILL_CALCULATE_TAX = "billCalculateTax";

	String CUSTOMER_ADD_POP = "customerAddPop";
	String CUSTOMER_SELECT_BOX = "custSelectBox";

	String DELETED_BILLS = "deletedBills";
}
