package com.billingSoftware.web.constants;

/**
 * 
 * @author Vinutha
 * @since 20-July-2015
 */
public interface RequestConstants {

	String LOGIN_URL = "signin";
	String LOGOUT_URL = "signout";
	String HOME_URL = "homePage.do";

	String ORDER = "orders.do";
	String ORDER_LIST = "orderList.do";
	String CONVERT_ORDER_TO_QUOTATION = "convertOrderToQuotation.do";

	String PRODUCTS = "products.do";
	String PRODUCTS_POP_UP = "productPopUp.do";
	String PRODUCTS_FIND = "findProducts.do";
	String PRODUCTS_ADD = "addProduct.do";
	String PRODUCTS_EDIT = "editProduct.do";
	String PRODUCTS_DELETE = "deleteProduct.do";
	String PRODUCT_SAVE = "saveProduct.do";
	String PRODUCT_LIST = "productList.do";

	String BILL = "bill.do";
	String BILL_SAVE = "saveBill.do";
	String BILL_PRINT = "printBill.do";
	String BILL_HISTORY = "billHistory.do";
	String BILL_DELETE = "billDelete.do";
	String BILL_LIST = "billList.do";
	String BILL_FILTER = "billFilter.do";
	String BILL_CALCULATE_TAX = "billCalculateTax.do";
	String BILL_REPORT = "billHistoryReport.do";

	String PENDING_QUOTATION = "pendingQuotation.do";

	String SEND_VIDEO = "sendVideo.do";
	String SEND_VIDEO_MAIL = "sendVideoMail.do";

	String SETTINGS = "settings.do";
	String UPDATE_PASSWORD = "changePassword.do";

	String QUOTATION_REPORTS = "quotationReports.do";

	String QUOTATION_PDF = "quotationPdf.do";
	String QUOTATION_PRINT = "quotationPrint.do";

	String QUOTATION_HISTORY = "quotationHistory.do";
	String ORDER_HISTORY = "orderHistory.do";

	String GET_CUSTOMERS = "getCustomers.do";
	String FIND_CUSTOMERS = "findCustomers.do";

	String TAX = "tax.do";
	String TAX_LIST = "taxList.do";
	String TAX_SAVE = "taxSave.do";
	String TAX_DELETE = "taxDelete.do";

	String SMS_HOME = "smsHome.do";
	String SEND_SMS = "smsSend.do";
	String SMS_HISTORY = "smsHistory.do";

	String CUSTOMER = "customer.do";
	String CUSTOMER_LIST = "customerList.do";
	String CUSTOMER_SAVE = "customerSave.do";
	String CUSTOMER_DELETE = "customerDelete.do";

	String CUSTOMER_ADD_POP = "customerAddPop.do";

	String SERVICE_PROVIDER = "serviceProvider.do";
	String SERVICE_PROVIDER_LIST = "serviceProviderList.do";
	String SERVICE_PROVIDER_SAVE = "serviceProviderSave.do";
	String SERVICE_PROVIDER_DELETE = "serviceProviderDelete.do";

	String DELETED_BILLS = "deletedBills.do";
}
