package com.billingSoftware.web.constants;
/**
 * 
 * @author Shamsheer & Vinutha
 * @since Oct 10, 2015
 */
public interface CommonConstants {

	String QUOTATION="QUOTATION";
	String ORDER="ORDER";
	
	String SEND_VIDEO="sendVideo";
	
	String VIDEO_URL="http://www.vijayaprakash.com/login";
}
