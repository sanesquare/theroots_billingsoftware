package com.billingSoftware.web.mail;

import java.util.Properties;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import com.billingSoftware.web.vo.MailConfigurationVo;

public class MailConfiguration {

	public Session mailSender(MailConfigurationVo mailConfigurationVo) {
		final String user_name = mailConfigurationVo.getUserName();
		final String password = mailConfigurationVo.getPassword();
		String host =mailConfigurationVo.getHost();
		Properties javaMailProperties = new Properties();
		javaMailProperties.put("mail.smtp.auth", "true");
		//javaMailProperties.put("mail.smtp.socketFactory.port", "465");
		javaMailProperties.put("mail.smtp.socketFactory.port", mailConfigurationVo.getPortNo());
		javaMailProperties.put("mail.smtp.startssl.enable", "true");
		javaMailProperties.put("mail.smtp.host", host);
		javaMailProperties.put("mail.smtp.starttls.enable", "true");
		return Session.getInstance(javaMailProperties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user_name, password);
			}
		});
	}
}
