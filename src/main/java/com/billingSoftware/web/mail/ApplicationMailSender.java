package com.billingSoftware.web.mail;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map.Entry;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Configurable;

import com.billingSoftware.web.constants.CommonConstants;
import com.billingSoftware.web.util.DateFormatter;
import com.billingSoftware.web.vo.EmailVo;
import com.billingSoftware.web.vo.MailConfigurationVo;
import com.billingSoftware.web.vo.QuotationVo;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * 
 * @author Jithin Mohan
 * @since 7-May-2015
 *
 */
@Configurable
public class ApplicationMailSender implements Runnable {

	private MailConfiguration mailConfiguration = new MailConfiguration();

	private MailConfigurationVo mailConfigurationVo;

	private EmailVo emailVo;

	private QuotationVo quotationVo;

	private String type;

	private String contextUrl;

	public void run() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
