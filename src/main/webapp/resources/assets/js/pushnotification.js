/**
 * Used to connect to web socket
 */

var stompClient = null;

function connect(handleNotification) {
	// var socket = new SockJS('/HRMS/notifications');
	var socket = Singleton.getInstance();
	stompClient = Stomp.over(socket);
	stompClient.connect({}, function(frame) {
		console.log('Connected: ' + frame);
		stompClient.subscribe('/HRMS/topic/message', function(message) {
			handleNotification(JSON.parse(message.body).content);
		});
	});
}

function disconnect() {
	if (stompClient != null) {
		stompClient.disconnect();
	}
}

/**
 * @author: Jithin Mohan
 * @since : 3-May-2015
 */
var Singleton = (function() {
	var instance;

	function createInstance() {
		// var object = new Object("I am the instance");
		var socket = new SockJS('/HRMS/notifications');
		return socket;
	}

	return {
		getInstance : function() {
			if (!instance) {
				instance = createInstance();
			}
			return instance;
		}
	};
})();