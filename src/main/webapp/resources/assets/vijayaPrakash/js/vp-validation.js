/**
 * @author Shamsheer & Vinutha
 * @since 4-August-2015
 */
$(document)
		.ready(
				function() {
					$(".ledgDate").on(
							"change",
							function() {
								$("#send_video_frm").formValidation(
										'revalidateField', "expiryDate");
							});

					$("#prdct_frm").formValidation({
						icon : {
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							productName : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Name is required'
									}
								}
							},
							rate : {
								row : '.col-sm-9',
								validators : {
									notEmpty : {
										message : 'Rate is required'
									},
									regexp : {
										regexp : /^((\d+)((\.\d{1,2})?))$/i,
										message : 'Enter a valid decimal place'
									}
								}
							}
						}
					});

					$("#qtn_frm")
							.formValidation(
									{
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											email : {
												row : '.col-sm-9',
												validators : {
													regexp : {
														regexp : '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
														message : 'The value is not a valid email address'
													}
												}
											}
										}
									});

					$("#send_video_frm")
							.formValidation(
									{
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											to : {
												row : '.col-sm-9',
												validators : {
													regexp : {
														regexp : '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
														message : 'The value is not a valid email address'
													},
													notEmpty : {
														message : 'E-mail is required'
													}
												}
											},
											companyId : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'required'
													}
												}
											},
											noOfTimes : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'required'
													}
												}
											},
											customer : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'required'
													}
												}
											},
											subject : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'required'
													}
												}
											},
											expiryDate : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'required'
													},
													date : {
														message : 'The  date is not valid. ',
														format : 'DD/MM/YYYY',
														min : 'today',
													}
												}
											}
										}
									});

					$("#pswrd_change_from")
							.formValidation(
									{
										icon : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											password : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'required'
													},
													identical : {
														field : 'confirmPassword',
														message : 'The password and its confirm are not the same'
													}
												}
											},
											confirmPassword : {
												row : '.col-sm-9',
												validators : {
													notEmpty : {
														message : 'required'
													},
													identical : {
														field : 'password',
														message : 'The password and its confirm are not the same'
													}
												}
											},
										}
									});
				});