/**
 * js file for purchase
 * 
 * @since 30-July-2015
 */

function changeDocumentTitle(menu, title) {
	$(".list-unstyled li").removeClass("active");
	$("#" + menu).addClass("active");
	document.title = title;
}

$('.numbersonly').keypress(function(e) {
	var a = [];
	var k = e.which;

	for (i = 48; i < 58; i++)
		a.push(i);

	if (!(a.indexOf(k) >= 0))
		e.preventDefault();

});

$('.numbersonly').bind("paste", function(e) {
	e.preventDefault();
});
$('.double').keypress(function(e) {
	var a = [];
	var k = e.which;

	for (i = 48; i < 58; i++) {
		a.push(i);
	}
	if (i = 46) {
		if ($(this).val().indexOf('.') == -1)
			a.push(i);
	}
	if (!(a.indexOf(k) >= 0))
		e.preventDefault();
	// alert($(this).val().indexOf('.'));
});
$('.double').bind("paste", function(e) {
	e.preventDefault();
});

$(".double").on("click", function() {
	this.select();
});

$(".double").on("change", function() {
	if (this.value == '')
		this.value = '0';
});

setTimeout(function() {
	$(".successMsg").fadeOut(600);
}, 3500);
$(".close-msg").click(function() {
	$(".successMsg").fadeOut(600);
});

/**
 * function to delete item
 * 
 * @param id
 */
function deleteThisUnit(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('unit');
	$("#id-hid").val(id);
}

/**
 * function to close confirm-delete pop
 */
function cancel() {
	$("#confirm-delete").css("display", "none");
	$("#id-hid").val('');
}

/**
 * function to confirm delete
 */
function deleteItem() {
	var item = $("#delete-item").val();
	if (item == 'product')
		deleteProduct($("#id-hid").val());
	else if (item == "company")
		deleteThisCompany($("#id-hid").val());
	else if (item == "tax")
		deleteThisTax($("#id-hid").val());
	else if (item == "customer")
		deleteThisCustomer($("#id-hid").val());
	else if (item == "bill")
		deleteBill($("#id-hid").val());
	else if (item == "service")
		deleteServicePro($("#id-hid").val());
	$("#confirm-delete").css("display", "none");
}

function deleteThisProduct(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('product');
	$("#id-hid").val(id);
}

/**
 * function to delete product
 * 
 * @param id
 */
function deleteProduct(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "deleteProduct.do",
		cache : false,
		async : true,
		data : {
			"id" : id
		},
		success : function(response) {
			if (response != 'ok') {
				$(".notfctn-cntnnt").text(response);
				$("#erro-msg").css("display", "block");
				$("#load-erp").css("display", "none");
			} else {
				$("#notfctn-cntnnt").text(response);
				$("#successMsg").css("display", "block");
			}
			loadProductTable();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});

}

/**
 * function to load product list
 */
function loadProductTable() {
	$.ajax({
		type : "GET",
		url : 'productList.do',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			$("#product_tbl_div").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);
}

/**
 * Add bill Page
 */

getProductDetails = function() {
	var idsAndQuantities = [];
	$(".prodctId").each(
			function() {
				idsAndQuantities.push(this.value + '!!!'
						+ $("#product_qty" + this.value).val());
			});
	var vo = new Object();
	vo.idsAndQuantities = idsAndQuantities;
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "POST",
		contentType : "application/json; charset=utf-8",
		dataType : "html",
		cache : false,
		data : JSON.stringify(vo),
		async : true,
		url : "productPopUp.do",
		success : function(response) {
			$("#pdtPop").html(response);
			$("#pdtPop").css("display", "block");
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});

}

addQtyToId = function(id, value) {
	var oldVal = $("." + id).val();
	var split = oldVal.split("!!!");
	$("." + id).val(split[0] + "!!!" + value);
	if (value != '' && value != 0)
		$("." + id).prop('checked', true);
	else
		$("." + id).prop('checked', false);
}

closePdtpo = function() {
	$("#load-erp").css("display", "block");
	var idsAndQuantities = [];

	$("input[name=product]:checked",
			$('#basic-datatables').dataTable().fnGetNodes()).each(function() {
		idsAndQuantities.push(this.value);
	});

	/*
	 * $("input[name=product]").each(function() { if (this.checked)
	 * ids.push(this.value); });
	 */

	$("input[name=selectedProduct]:checked",
			$('#basic-datatables').dataTable().fnGetNodes()).each(function() {
		idsAndQuantities.push(this.value);
	});

	/*
	 * $("input[name=selectedProduct]").each(function() { if (this.checked)
	 * ids.push(this.value); });
	 */
	var vo = new Object();
	vo.idsAndQuantities = idsAndQuantities;
	var count = $(".prdct_tbl tr").size();
	count = parseFloat(count) - parseFloat(1);
	vo.rowCount = count;
	$.ajax({
		type : 'POST',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		data : JSON.stringify(vo),
		url : "findProducts.do",
		success : function(response) {
			$(".prdct_tbl tr").remove();
			$(".prdct_tbl").append(response);
			findQuotationTotal();
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
	$("#pdtPop").fadeOut("600");
}

cancelPdtpo = function() {
	$("#pdtPop").fadeOut("600");
}

$('.proTable tr').click(function(event) {
	if (event.target.type !== 'checkbox') {
		$(':checkbox', this).trigger('click');
	}
});

/**
 * function to calculate product row total
 * 
 * @param id
 */
function calculateProductSum(id) {
	var qty = $("#product_qty" + id).val();
	var rate = $("#product_rt" + id).val();
	var total = parseFloat(qty) * parseFloat(rate).toFixed(2);
	$("#prod_tot" + id).text(total);
	findQuotationTotal();
}

/**
 * function to calculate grand total
 */
function findQuotationTotal() {
	var total = "0.00";
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			total = parseFloat(total) + parseFloat($("#prod_tot" + id).text());
		}
	});
	$("#grandTotal").text(total);
	calculateDiscount();
	// calculateTax();
}

function findTaxSum() {
	var total = $("#grandTotal").text();
	var tax = 0;
	$(".tax_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			if ($("#tax_amnt" + id).val() == '')
				$("#tax_amnt" + id).val(0);
			tax = parseFloat(tax) + parseFloat($("#tax_amnt" + id).val());
		}
	});
	$("#taxTotal").text(tax);
	$("#netTotal").text(parseFloat(total) + parseFloat(tax));
}

function closeError() {
	$("#errror-msg").css("display", "none");
}

/**
 * function to send quotation mail
 */
function sendQuotationMail() {
	$("#load-erp").css("display", "block");
	var flag = true;
	var msg;
	var vo = createQuotationVo();
	if (vo.companyId == "") {
		flag = false;
		msg = "Company is required.";
	} else if (vo.customerDetails == "") {
		flag = false;
		msg = "Customer details is required.";
	} else if (vo.date == "") {
		flag = false;
		msg = "Date is required.";
	} else if (vo.itemVos.length == 0) {
		flag = false;
		msg = "Products required.";
	} else if (vo.email == "") {
		flag = false;
		msg = "Email required.";
	} else if (vo.customerName == "") {
		flag = false;
		msg = "Name is required.";
	}

	if (flag) {
		$.ajax({
			type : "POST",
			cache : false,
			async : true,
			url : "sendQuotationMail.do",
			dataType : "json",
			contentType : "application/json;charset=utf-8",
			data : JSON.stringify(vo),
			success : function(response) {
				setTimeout(function() {
					$("#load-erp").css("display", "none");
				}, 1000);
				var currentUrl = location.href;
				var splits = currentUrl.split("VijayPrakash");
				var newUrl = splits[0] + "VijayPrakash/quotation.do?id="
						+ response
						+ "&msg=Your quotation has been saved and mailed to "
						+ vo.email;
				window.history.pushState({
					path : newUrl
				}, '', newUrl);
				location.reload();
			},
			error : function(requestObject, error, errorThrown) {
				$("#load-erp").css("display", "none");
				alert(errorThrown);
			}
		});
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(msg);
		$("#load-erp").css("display", "none");
	}

}

function calculateDiscount() {
	var percentage = $("#discountPercentage").val();
	var total = $("#grandTotal").text();
	$("#discountAmount")
			.val((parseFloat(total) * parseFloat(percentage)) / 100);
	calculateTax();
}

/**
 * function to save quotation
 */
function saveQuotation(print) {
	var msg;
	var flag = true;
	var vo = new Object();
	var itemVos = [];
	vo.grandTotal = $("#grandTotal").text();
	vo.taxTotal = $("#taxTotal").text();
	vo.discountAmount = $("#discountAmount").val();
	vo.discountPercentage = $("#discountPercentage").val();
	vo.netTotal = $("#netTotal").text();
	vo.id = $("#quotationId").val();
	vo.date = $("#quot-date").val();
	vo.customerId = $("#billCust").val();
	vo.referenceNumber = $("#referenceNumber").val();
	vo.customerName = $("#custName").val();
	vo.mobile = $("#custMobile").val();
	vo.paymentModeId = $("#pymntMode").val();
	vo.serviceProviderId = $("#serviceProvider").val();
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var item = new Object();
			item.productId = id;
			item.productName = $("#prod_name" + id).text();
			item.description = $("#prod_desc" + id).val();
			item.total = $("#prod_tot" + id).text();
			item.quantity = $("#product_qty" + id).val();
			item.rate = $("#product_rt" + id).val();
			item.serviceProviderId = $("#prdctSrvcProvider" + id).val();
			itemVos.push(item);
		}
	});
	vo.itemVos = itemVos;

	var taxVos = [];
	$(".tax_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var tax = new Object();
			tax.taxId = id;
			tax.amount = $("#tax_amnt" + id).val();
			taxVos.push(tax);
		}
	});
	vo.taxVos = taxVos;

	if (vo.customerName == '') {
		flag = false;
		msg = 'Select Customer';
	} else if (itemVos.length == 0) {
		flag = false;
		msg = 'Atleast one product is required';
	}
	if (flag) {
		$("#load-erp").css("display", "block");
		$
				.ajax({
					type : "POST",
					cache : false,
					async : true,
					url : "saveBill.do?print=" + print,
					contentType : "application/json;charset=utf-8",
					data : JSON.stringify(vo),
					success : function(response) {
						console.log(JSON.stringify(response));

						if (response.id == 0) {
							$("#errror-msg").css("display", "block");
							$(".modal-body")
									.text("Oops.. something went wrong");
						} else {
							$('#quotationId').val(response.id);
							/*
							 * var currentUrl = location.href; var splits =
							 * currentUrl.split("bill.do"); var newUrl =
							 * splits[0] + "bill.do?id=" + response.id +
							 * "&msg=Your bill has been saved Successfully ";
							 * window.history.pushState({ path : newUrl }, '',
							 * newUrl);
							 */
							/*
							 * $('.notfctn-cntnnt').html("Your bill has been
							 * saved Successfully "); $('.successMsg').show();
							 * setTimeout(function(){ $('.successMsg').hide();
							 * location.reload(); }, 4000)
							 */
						}
						if (response.print) {
							var w = window.open(response.url);
							w.focus();
							w.print();
							// $("#pdf").attr("src", response.url);
						} else {
							window.history
									.pushState(
											{
												path : "bill.do?msg=Your bill has been saved Successfully"
											}, '',
											"bill.do?msg=Your bill has been saved Successfully");
							location.reload();
						}
					},
					error : function(requestObject, error, errorThrown) {
						alert(errorThrown);
					},
					complete : function() {
						$("#load-erp").css("display", "none");
					}
				});
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(msg);
	}
}

function printThisBill(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		cache : false,
		async : true,
		url : "printBill.do",
		data : {
			"id" : id
		},
		success : function(response) {
			var w = window.open(response);
			w.focus();
			w.print();
			filterBill();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

jQuery(document).bind("keyup keydown", function(e) {
	if (e.ctrlKey && e.keyCode == 80) {
		e.preventDefault();
		saveQuotation(true);
	} else if (e.ctrlKey && e.keyCode == 83) {
		e.preventDefault();
		saveQuotation(false);
	}
});

function isLoaded() {
	$("#load-erp").css("display", "block");
	var pdfFrame = window.frames["pdf"];
	pdfFrame.focus();
	pdfFrame.print();
}

function deleteCompany(id) {
	$("#confirm-delete").css("display", "block");
	$("#id-hid").val(id);
	$("#delete-item").val('company');
}

function deleteThisCompany(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		cache : false,
		async : true,
		url : "deleteThisCompany.do",
		data : {
			"id" : id
		},
		dataType : "json",
		contentType : "application/json;charset=utf-8",
		success : function(response) {
			var msg = "";
			if (response == true) {
				msg = "Company Deleted Successfully.";
			} else {
				msg = "Company Delete Failed.";
			}
			var currentUrl = location.href;
			var splits = currentUrl.split("VijayPrakash");
			var newUrl = splits[0] + "VijayPrakash/settings.do?msg=" + msg;
			window.history.pushState({
				path : newUrl
			}, '', newUrl);
			location.reload();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
	$("#load-erp").css("display", "none");
}

filterPendingQuotes = function() {
	$("#totl_amnt").text("0.00");
	var vo = new Object();
	vo.startDate = $("#peQotStrt").val();
	vo.endDate = $("#peQotEnd").val();
	vo.companyIds = $("#companyIdSelect").val();

	if (vo.startDate.length > 0 && vo.endDate.length > 0) {
		if (process(vo.startDate) > process(vo.endDate)) {
			$(".modal-body").text("Please Select valid date range..");
			$("#errror-msg").css("display", "block");
		} else {
			$("#load-erp").css("display", "block");
			$.ajax({
				type : "POST",
				url : "quotationList.do",
				cache : false,
				async : true,
				data : JSON.stringify(vo),
				contentType : "application/json;charset=utf-8",
				dataType : "html",
				success : function(response) {
					$("#quot_tbl_div").html(response);
					$("#load-erp").css("display", "none");
					calculateGrandTotal();
				},
				error : function(requestObject, error, errorThrown) {
					$(".notfctn-cntnnt").text(errorThrown);
					$("#success-msg").css("display", "block");
				}
			});
		}
	} else {
		$(".modal-body").text("Date fields cannot be empty..");
		$("#errror-msg").css("display", "block");
	}
}

function process(date) {
	var parts = date.split("/");
	return new Date(parts[2], parts[1] - 1, parts[0]);
}

calculateGrandTotal = function() {
	var total = 0;
	$(".qot-tabl tr").each(function() {
		var content = this.cells[4].innerHTML;
		if (content != "Total") {
			total = parseFloat(total) + parseFloat(content);
		}
	});
	$("#totl_amnt").text(total.toFixed(2));
}

calculateGrandTotalOrders = function() {
	var total = 0;
	$(".ord-tbl tr").each(function() {
		var content = this.cells[4].innerHTML;
		if (content != "Total") {
			total = parseFloat(total) + parseFloat(content);
		}
	});
	$("#totl_amnt_ordr").text(total.toFixed(2));
}

filterOrder = function() {
	$("#totl_amnt_ordr").text("0.00");
	var vo = new Object();
	vo.startDate = $("#ordrStart").val();
	vo.endDate = $("#ordrEnd").val();
	vo.companyIds = $("#companyIdSelect").val();
	if (vo.startDate.length > 0 && vo.endDate.length > 0) {
		if (process(vo.startDate) > process(vo.endDate)) {
			$(".modal-body").text("Please Select valid date range..");
			$("#errror-msg").css("display", "block");
		} else {
			$("#load-erp").css("display", "block");
			$.ajax({
				type : "POST",
				url : "orderList.do",
				cache : false,
				async : true,
				data : JSON.stringify(vo),
				contentType : "application/json;charset=utf-8",
				dataType : "html",
				success : function(response) {
					$("#ordr_tbl_div").html(response);
					$("#load-erp").css("display", "none");
					calculateGrandTotal();
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
					$(".notfctn-cntnnt").text(errorThrown);
					$("#success-msg").css("display", "block");
					$("#load-erp").css("display", "none");
				}
			});
		}

	} else {
		$(".modal-body").text("Date fields cannot be empty..");
		$("#errror-msg").css("display", "block");
	}
}
closeError = function() {
	$("#errror-msg").fadeOut("600");
}
printQuotation = function() {

	var id = $("#quotationId").val();
	if (id.length == 0) {
		$(".modal-body").text("Please Save Quotation..");
		$("#errror-msg").css("display", "block");
	} else {
		window.location = 'quotationPrint.do?id=' + id;
	}

}
quotationPdf = function() {
	var id = $("#quotationId").val();
	if (id.length == 0) {
		$(".modal-body").text("Please Save Quotation..");
		$("#errror-msg").css("display", "block");
	} else {
		window.location = 'quotationPdf.do?id=' + id;
	}
}

quotationHistory = function() {
	var start = $("#peQotStrt").val();
	var end = $("#peQotEnd").val();
	$("#load-erp").css("display", "block");
	window.location = 'quotationHistory.do?startDate=' + start + '&endDate='
			+ end;
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);

}

orderHistory = function() {
	var start = $("#ordrStart").val();
	var end = $("#ordrEnd").val();
	$("#load-erp").css("display", "block");
	window.location = 'orderHistory.do?startDate=' + start + '&endDate=' + end;
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);

}
var label = '"' + $("#company").val() + '"';
var prefix = '';
function changeComName() {

	$("#load-erp").css("display", "block");
	getCodeForCOmpany($("#companyIdSelect").val(), $("#isOrder").val());
	/*
	 * var oldLabel = label; // label = '"' + $("#companyIdSelect
	 * option:selected").text() + '"'; var id = $("#companyIdSelect").val(); if
	 * (id != '') { $.ajax({ type : "GET", cache : false, async : true, url :
	 * "getProductPrefixForCompany.do", data : { "id" : id }, success :
	 * function(r) { prefix = r; label = '"' + prefix + '"'; $(".prdct_tbl
	 * tr").each(function(i, item) { var id = this.id; if (id != '') { var des =
	 * $("#prod_desc" + id).text(); if (des.indexOf(oldLabel) >= 0) { var
	 * splittedDes = des.split(oldLabel); des = splittedDes[1]; } if (label !=
	 * '"-Select Company-"') var updDes = label + ' ' + des; else var updDes =
	 * des; $("#prod_desc" + id).text(updDes); } }); }, error :
	 * function(requestObj, err, errorThro) { alert(errorThro); } }); }
	 */
	$("#load-erp").css("display", "none");

}

function getCodeForCOmpany(id, isOrder) {
	if (id != '') {
		$.ajax({
			type : "GET",
			cache : false,
			async : true,
			url : "getCodeForCompany.do",
			data : {
				"id" : id,
				"isOrder" : isOrder
			},
			success : function(r) {
				$("#ref_quot").text(r);
			},
			error : function(requestObj, err, errorThro) {
				alert(errorThro);
			}
		});
	} else {
		$("#ref_quot").text("");
	}
}

function findCustomers() {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		contentType : "application/json; charset=utf-8",
		dataType : "html",
		cache : false,
		async : true,
		url : "findCustomers.do",
		success : function(response) {
			$("#pdtPop").html(response);
			$("#pdtPop").css("display", "block");
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

function selectThisCustomer() {
	$("input[name=cust]:checked",
			$('#basic-datatables').dataTable().fnGetNodes()).each(function() {
		var cust = this.value;
		var custArray = cust.split('!@#');
		$("#qut_cust_nm").val(custArray[0]);
		$("#qut_cust_det").val(custArray[1]);
		$("#ref_mail").val(custArray[2]);
		$("#pdtPop").fadeOut("600");
	});
}

/**
 * function to clear tax form
 */
function clearTaxForm() {
	$("#taxId").val('');
	$("#taxName").val('');
	$("#taxPercentage").val('');
}

function editTax(id, tax, percentage) {
	$("#taxId").val(id);
	$("#taxName").val(tax);
	$("#taxPercentage").val(percentage);
}

function deleteTax(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('tax');
	$("#id-hid").val(id);
}

function deleteThisTax(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "taxDelete.do",
		cache : false,
		async : true,
		data : {
			"id" : id
		},
		success : function(response) {
			$("#taxPage").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

function saveTax() {
	var vo = new Object();
	vo.id = $("#taxId").val();
	vo.tax = $("#taxName").val();
	vo.percentage = $("#taxPercentage").val();
	if ($("#taxName").val() != '' && $("#taxPercentage").val() != '') {
		$("#load-erp").css("display", "block");
		$.ajax({
			type : "POST",
			url : "taxSave.do",
			cache : false,
			async : true,
			data : JSON.stringify(vo),
			contentType : "application/json;charset=utf-8",
			dataType : "html",
			success : function(response) {
				$("#taxPage").html(response);
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			},
			complete : function() {
				$("#load-erp").css("display", "none");
			}
		});
	} else {
		if ($("#taxName").val() == '')
			$("#taxName").focus();
		else
			$("#taxPercentage").focus();
	}
}

/**
 * function to delete customer
 * 
 * @param id
 */
deleteThisCustomer = function(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "customerDelete.do",
		cache : false,
		async : true,
		data : {
			"id" : id
		},
		success : function(response) {
			$("#customerPage").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}
function clearCustomerForm() {
	$("#customerName").val('');
	$("#customerId").val('');
	$("#customerMobile").val('');
	$("#customerEmail").val('');
	$("#custAddress").val('');
}

function editCustomer(id, name, address, mobile, email) {
	$("#customerId").val(id);
	$("#customerName").val(name);
	$("#customerMobile").val(mobile);
	$("#customerEmail").val(email);
	$("#custAddress").val(address);
}

function deleteCustomer(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('customer');
	$("#id-hid").val(id);
}

function saveCustomer() {
	var vo = new Object();
	vo.id = $("#customerId").val();
	vo.name = $("#customerName").val();
	vo.mobile = $("#customerMobile").val();
	vo.email = $("#customerEmail").val();
	vo.address = $("#custAddress").val();
	if ($("#customerName").val() != '' && $("#customerMobile").val() != '') {
		$("#load-erp").css("display", "block");
		$.ajax({
			type : "POST",
			url : "customerSave.do",
			cache : false,
			async : true,
			data : JSON.stringify(vo),
			contentType : "application/json;charset=utf-8",
			dataType : "html",
			success : function(response) {
				$("#customerPage").html(response);
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			},
			complete : function() {
				$("#load-erp").css("display", "none");
			}
		});
	} else {
		if ($("#customerName").val() == '')
			$("#customerName").focus();
		else
			$("#customerMobile").focus();
	}
}

/**
 * BILL
 */
function deleteThisBill(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('bill');
	$("#id-hid").val(id);
}

deleteBill = function(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "billDelete.do",
		cache : false,
		async : true,
		data : {
			"id" : id
		},
		success : function(response) {
			$("#product_tbl_div").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

calculateTax = function(id) {
	$("#load-erp").css("display", "block");
	var ids = "'" + $("#taxSelect").val() + "'";
	$.ajax({
		type : "GET",
		url : "billCalculateTax.do",
		cache : false,
		async : true,
		data : {
			"total" : parseFloat($("#grandTotal").text())
					- parseFloat($("#discountAmount").val()),
			"taxIds" : ids
		},
		success : function(response) {
			$("#taxSection").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

filterBill = function() {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "billFilter.do",
		cache : false,
		async : true,
		data : {
			"fromDate" : $("#billFromDate").val(),
			"toDate" : $("#billToDate").val(),
			"customerId" : $("#billFilterCust").val(),
			"paymentModeId" : $("#billFilterPymntMode").val(),
			"serviceProviderId" : $("#billFilterSrvcPrvdr").val()
		},
		success : function(response) {
			$("#product_tbl_div").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}

function editThisBill(url, type) {
	$("#url_hid").val(url);
	$("#invldPwd").hide();
	$("#ediType").val(type);
	$("#confirm-edit").show();
	$("#pswd").focus();
}

function cancelEdit() {
	$("#url_hid").val('');
	$("#ediType").val('');
	$("#confirm-edit").hide();
	$("#pswd").val('');
}

function editThisItem() {
	var password = $("#pswd").val();
	var editType = $("#ediType").val();
	$("#pswd").val('');
	if (password == 'admin654') {
		$("#confirm-edit").hide();
		if (editType == 'edit') {
			window.history.pushState({
				path : $("#url_hid").val()
			}, '', $("#url_hid").val());
			location.reload();
		} else {
			deleteThisBill($("#url_hid").val());
		}
	} else {
		$("#pswd").val('');
		$("#invldPwd").show();
	}
}

$("#billCust").on('change', function() {
	if (this.value != 'new') {
		$("#load-erp").css("display", "block");
		$.ajax({
			type : "GET",
			dataType : "html",
			cache : false,
			async : true,
			data : {
				'custId' : this.value
			},
			url : "customerAddPop.do",
			success : function(response) {
				/*
				 * $("#pdtPop").html(response); $("#pdtPop").show();
				 */
				var obj = $.parseJSON(response);
				console.log(obj);
				$("#custName").val(obj.name);
				$("#custMobile").val(obj.mobile);
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			},
			complete : function() {
				$("#load-erp").css("display", "none");
			}
		});
	} else {
		$("#billCust").val('');
		$('#billCust').chosen().trigger("chosen:updated");
		$("#custName").val('');
		$("#custMobile").val('');
	}
});

function saveCustomerFromPop() {
	var vo = new Object();
	vo.id = $("#customerId").val();
	vo.name = $("#customerName").val();
	vo.mobile = $("#customerMobile").val();
	vo.email = $("#customerEmail").val();
	vo.address = $("#custAddress").val();
	if ($("#customerName").val() != '' && $("#customerMobile").val() != '') {
		$("#load-erp").css("display", "block");
		$("#pdtPop").hide();
		$.ajax({
			type : "POST",
			url : "customerAddPop.do",
			cache : false,
			async : true,
			data : JSON.stringify(vo),
			contentType : "application/json;charset=utf-8",
			dataType : "html",
			success : function(response) {
				$("#custSelectDiv").html(response);
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			},
			complete : function() {
				$("#load-erp").css("display", "none");
			}
		});
	} else {
		if ($("#customerName").val() == '')
			$("#customerName").focus();
		else
			$("#customerMobile").focus();
	}
}

/** SERVICE PROVIDER */
saveServiceProvider = function() {
	var vo = new Object();
	vo.id = $("#serviceProviderId").val();
	vo.serviceProvider = $("#serviceProviderName").val();
	vo.mobile = $("#serviceProviderMobile").val();
	if ($("#serviceProviderName").val() != '') {
		$("#load-erp").css("display", "block");
		$.ajax({
			type : "POST",
			url : "serviceProviderSave.do",
			cache : false,
			async : true,
			data : JSON.stringify(vo),
			contentType : "application/json;charset=utf-8",
			dataType : "html",
			success : function(response) {
				$("#serviceProviderPage").html(response);
			},
			error : function(requestObject, error, errorThrown) {
				alert(errorThrown);
			},
			complete : function() {
				$("#load-erp").css("display", "none");
			}
		});
	} else {
		if ($("#serviceProviderName").val() == '')
			$("#serviceProviderName").focus();
	}
}

clearServiceForm = function() {
	$("#serviceProviderId").val('');
	$("#serviceProviderMobile").val('');
	$("#serviceProviderName").val('');
}

function editServiceProvider(id, serviceProvider, mobile) {
	$("#serviceProviderId").val(id);
	$("#serviceProviderMobile").val(mobile);
	$("#serviceProviderName").val(serviceProvider);
}

function deleteServiceProvider(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('service');
	$("#id-hid").val(id);
}

function deleteServicePro(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "serviceProviderDelete.do",
		cache : false,
		async : true,
		data : {
			"id" : id
		},
		success : function(response) {
			$("#serviceProviderPage").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		},
		complete : function() {
			$("#load-erp").css("display", "none");
		}
	});
}