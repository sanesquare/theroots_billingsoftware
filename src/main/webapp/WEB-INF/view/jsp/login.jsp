<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>TheRoots BillingSoftware</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" />
<link href='<c:url value="/resources/assets/css/bootstrap.css"></c:url>'
	rel='stylesheet' type='text/css' />
<link href='<c:url value="/resources/assets/css/style.css"></c:url>'
	rel="stylesheet" type="text/css" media="all" />
<script
	src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.valRidate.min.js"
	type="text/javascript"></script>
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900'
	rel='stylesheet' type='text/css'>
<style type="text/css">
.error {
	color: #DF0101;
	font-weight: bold;
}
</style>
<body>
	<div class="header text-center">
		<div class="container">
			<%-- <div class="logo">
				<a href="#"><span><img src='<c:url value="/resources/assets/images/logo.png"></c:url>' /></span></a>
			</div> --%>
		</div>
	</div>
	<div class="container">
		<div class="login-01">
			<div class="one-login  hvr-float-shadow">
				<div class="one-login-head">
					<img
						src='<c:url value="/resources/assets/images/top-lock.png"></c:url>'
						alt="">
					<h1>LOGIN</h1>
					<span></span>
				</div>
				<form id="logForm" role="form"
					action="<c:url value='/j_spring_security_check' />" method="POST"
					name="loginForm">
					<li><input type="text" id="txtUser" class="text" tabindex="1"
						name="userName" /><a href="#" class=" icon user"></a></li>
					<li><input type="password" id="txtPassword" tabindex="2"
						name="password" /><a href="#" class=" icon lock"></a></li>
					<div class="p-container">
						<div class="clear"></div>
					</div>
					<!-- <a href="index.html"><div class="submit"> -->
					<input type="submit" class="btn" value="SIGN IN" />
					<p id="error" class="error"></p>
				</form>
			</div>

		</div>
	</div>
	</div>
	<script src='<c:url value="/resources/asset/js/jquery.min.js"></c:url>'></script>
	<script type="text/javascript">
		function validateForm() {
			var error = document.getElementById("error");

			if ($("#txtUser").val() == '')
				error.innerHTML = "Enter User Name";
			else if ($("#txtPassword").val() == '')
				error.innerHTML = "Enter password";
			else
				logForm.submit();
		}
		function clearUnameMsg() {
			document.getElementById("error").innerHTML = "";
		}
	</script>
</body>
</html>