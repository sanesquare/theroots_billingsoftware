<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/validation/formValidation.css'/>" />
</head>
<body>
	<div class="purchase_home">
		<div class="warper container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">
							Change Password<a href="homePage.do"><span class="back-btn"><i
									class="fa fa-arrow-left"></i></span></a>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<form:form action="changePassword.do" commandName="userVo"
										id="pswrd_change_from">
										<form:hidden path="username" />
										<div class="row">
											<div class="col-md-12">
												<div class="panel panel-default erp-info">
													<div class="panel-heading panel-inf">Password Change</div>
													<div class="panel-body">

														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">New Password<span
																class="stars">*</span>
															</label>
															<div class="col-sm-9">
																<form:password path="password" minlength="4" class="form-control paste" />
															</div>
														</div>
														<div class="form-group">
															<label for="inputPassword3"
																class="col-sm-3 control-label">Confirm Password<span
																class="stars">*</span>
															</label>
															<div class="col-sm-9">
																<input name="confirmPassword" type="password"
																	class="form-control " />
															</div>
														</div>
														<div class="form-group">
															<button type="submit" class="btn btn-primary erp-btn">Save</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form:form>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="confirm-delete" style="display: none;">
		<div class="modal " style="display: block;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Confirmation</h4>
					</div>
					<div class="modal-body">Do you really want to delete this
						item?</div>
					<input type="hidden" id="id-hid">
					<div class="modal-footer clearfix">
						<div class="btn-toolbar pull-right">
							<input type="hidden" id="delete-item">
							<button type="button" onclick="cancel()" class="btn btn-default">No</button>
							<button type="button" onclick="deleteItem()"
								class="btn btn-primary">Yes</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="load-erp">
		<div id="spinneeer-erp"></div>
	</div>
	<c:if test="${! empty msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${msg }</div>
			<span class="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

	<script
		src="<c:url value='/resources/assets/vijayaPrakash/js/vp.js' />"></script>
	<script
		src="<c:url value='/resources/assets/vijayaPrakash/js/vp-validation.js' />"></script>

	<script type="text/javascript"
		src="<c:url value='/resources/assets/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/assets/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/assets/js/validation/bootstrap.js' />"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			changeDocumentTitle('settings', 'Settings');
		});
		</script>
</body>
</html>