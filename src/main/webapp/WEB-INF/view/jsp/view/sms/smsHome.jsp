<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/multi-select/fSelect.css'/>" />
</head>
<body>
	<div class="purchase_home">
		<div class="warper container-fluid " id="customerPage">
			<c:if test="${! empty msg }">
				<div class="notification-panl successMsg">
					<div class="notfctn-cntnnt">${msg }</div>
					<span class="close-msg"><i class="fa fa-times"></i></span>
				</div>
			</c:if>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default erp-info">
						<div class="panel-heading panel-inf">SMS
							<a href="smsHistory.do"><span style="float: right;font-weight: bold">View History</span></a>
						</div>
						<div class="panel-body">

							<div class="form-group">
								<label class="col-sm-3 control-label">Message<span
									class="stars">*</span>
								</label>
								<div class="col-sm-9">
									<textarea placeholder="Your message goes here.." rows="5"
										style="resize: none" class="form-control" id="message"></textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">Select customers<span
									class="stars">*</span>
								</label>
								<div class="col-sm-9">
									<select class="demo" id="customers" name="customers"
										data-placeholder="All customers" multiple="multiple">
											<option value='-1' id="select_all" onclick="selectAll()">Select All</option>
										<c:forEach items="${customers }" var="customer">
											<option value="${customer.id }">${customer.name }</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Other numbers
								</label>
								<div class="col-sm-9">
									<span style="font-weight: bold">Separate multiple numbers with comma</span>
									<input type="text" id="otherNumbers" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<input style="float:right" onclick="sendSMS()" type="button" class="btn btn-primary erp-btn" value="Send">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="load-erp">
		<div id="spinneeer-erp"></div>
	</div>
	<div id="success-msg" style="display: none">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${success_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</div>

	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/vijayaPrakash/js/vp.js' />"></script>
	<script
		src="<c:url value='/resources/assets/multi-select/fSelect.js' />"></script>

	<script type="text/javascript">
		$(document).ready(function() {

			$('#customers').fSelect();
			
			$('#otherNumbers').on('keypress', function(event){
				return isNumberKey(event)
			});

		});
		
		$(document).on('click', '.fs-option[data-index="0"]', function(e){
			if($(this).hasClass('selected')){
				$('.fs-option.selected').click();
			}
			else{
				$('.fs-option').not(".fs-option[data-index='0'], .fs-option.selected").click();
			}
			$('.fs-label').html($('.fs-label').html().replace("Select All, ", ""));
		});
		
		function sendSMS(){
			var ids = ($('#customers').val());
			if(ids==null && $('otherNumbers').val()==""){
				alert("Please add atleat on recepient");
				return false;
			}
			if( $('#message').val()==""){
				 $('#message').focus();
				 return false
			}
			$.ajax({
				type : "POST",
				url : "smsSend.do",
				cache : false,
				async : true,
				data : {
					"ids" : ids, "message": $('#message').val(), "others":$('#otherNumbers').val() 
				},
				beforeSend: function(){
					$('#load-erp').show();
				},
				success : function(response) {
					if(response==true){
						alert("message sent")
					}
					else{
						alert("message not sent");
					}

				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
				},
				complete: function(){
					$('#load-erp').hide();
				}
			});
		}
		
		
		
		function isNumberKey(evt){
	          var charCode = (evt.which) ? evt.which : evt.keyCode;
	          if (charCode != 44 && charCode > 31 
	            && (charCode<48 || charCode>57))
	             return false;

	          return true;
	    }
		
	</script>
</body>
</html>