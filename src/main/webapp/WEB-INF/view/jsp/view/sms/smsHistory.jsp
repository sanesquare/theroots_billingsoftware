<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head></head>
<body>
	<div class="purchase_home">
		<div class="warper container-fluid " id="customerPage">
			<c:if test="${! empty msg }">
				<div class="notification-panl successMsg">
					<div class="notfctn-cntnnt">${msg }</div>
					<span class="close-msg"><i class="fa fa-times"></i></span>
				</div>
			</c:if>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">SMS History</div>
						<div class="panel-body">
							<div id="product_tbl_div">
								<table cellpadding="0" cellspacing="0" border="0"
									class="table table-striped table-bordered erp-tbl"
									id="basic-datatable">
									<thead>
										<tr>
											<th width="5%">#</th>
											<th width="50%">Message</th>
											<th width="40%">Customers</th>
											<th width="5%">Date</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${histories }" var="hist" varStatus="s">
											<tr>
												<td>${s.count }</td>
												<td>${hist.message }</td>
												<td>${hist.customerNames }</td>
												<td>${hist.dateTime }</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="load-erp">
		<div id="spinneeer-erp"></div>
	</div>
	<div id="success-msg" style="display: none">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${success_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</div>

	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script
		src="<c:url value='/resources/assets/vijayaPrakash/js/vp.js' />"></script>
</body>
</html>