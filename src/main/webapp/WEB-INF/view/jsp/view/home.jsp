<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
</head>
<body>
	<div class="purchase_home vp_home">
		<div class="warper container-fluid">
			<c:if test="${not empty warning}">
				<div class="row">
					<div class="col-md-12">
						<div class="breakingNews bn-large" id="bn10">
							<ul id="announ_ses"
								style="font-size: 21px; border-radius: 8px; background-color: rgba(210, 38, 38, 0.3); color: #fff;">
								<marquee class="mrq" onmouseover="stop();" onmouseout="start();">
									<i class="fa fa-asterisk"
										style="font-size: 10px; color: #15AFEA;"></i>&nbsp;&nbsp;${warning}&nbsp;&nbsp;
								</marquee>
							</ul>
						</div>
					</div>
				</div>
			</c:if>
			<img alt="" id="logoDashboard"
				src='<c:url value='/resources/assets/vijayaPrakash/images/theRoots.PNG'/>'>
		</div>
	</div>
	<c:if test="${! empty msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${msg }</div>
			<span class="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>

	<script
		src="<c:url value='/resources/assets/vijayaPrakash/js/vp.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			changeDocumentTitle('dashbordli', 'Home');
			var expiry_licence = "${expiry_licence}";
			if (expiry_licence != '') {
				alert(expiry_licence);
				location.href = "signout";
			}
		});
	</script>
</body>
</html>