<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:if test="${! empty msg }">
	<div class="notification-panl successMsg">
		<div class="notfctn-cntnnt">${msg }</div>
		<span class="close-msg"><i class="fa fa-times"></i></span>
	</div>
</c:if>
<c:if test="${! empty msg }">
	<div class="notification-panl successMsg">
		<div class="notfctn-cntnnt">${msg }</div>
		<span class="close-msg"><i class="fa fa-times"></i></span>
	</div>
</c:if>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default erp-panle">
			<div class="panel-heading  panel-inf">Customers</div>
			<div class="panel-body">
				<div id="product_tbl_div">
					<table cellpadding="0" cellspacing="0" border="0"
						class="table table-striped table-bordered erp-tbl"
						id="basic-datatable">
						<thead>
							<tr>
								<th width="5%">#</th>
								<th width="20%">Name</th>
								<th width="10%">Mobile</th>
								<th width="5%">Edit</th>
								<th width="5%">Delete</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${serviceProviders }" var="customer"
								varStatus="s">
								<tr>
									<td>${s.count }</td>
									<td>${customer.serviceProvider }</td>
									<td>${customer.mobile }</td>
									<td><i class="fa fa-pencil"
										onclick="editServiceProvider('${customer.id}','${customer.serviceProvider }','${customer.mobile }')"></i>
									</td>
									<td><i class="fa fa-times"
										onclick="deleteServiceProvider('${customer.id}')"></i></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>

			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default erp-info">
			<div class="panel-heading panel-inf">Service Provider
				Information</div>
			<div class="panel-body">

				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Name<span
						class="stars">*</span>
					</label>
					<div class="col-sm-9">
						<input type="hidden" id="serviceProviderId"> <input type="text"
							class="form-control" id="serviceProviderName">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Mobile<span
						class="stars">*</span>
					</label>
					<div class="col-sm-9">
						<input type="text" class="form-control numbersonly"
							id="serviceProviderMobile">
					</div>
				</div>
				<div class="form-group">
					<button type="button" onclick="saveServiceProvider()"
						class="btn btn-primary erp-btn">Save</button>
				</div>

				<button type="button" onclick="clearServiceForm()"
					class="btn btn-primary erp-btn right">Clear</button>
			</div>
		</div>
	</div>
</div>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script src="<c:url value='/resources/assets/vijayaPrakash/js/vp.js' />"></script>
