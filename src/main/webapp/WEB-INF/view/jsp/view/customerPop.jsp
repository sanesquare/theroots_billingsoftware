<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="modal prodModal" data-backdrop="static"
	data-keyboard="false" tabindex="-1" aria-hidden="false"
	style="display: block;" role="dialog">
	<div class="prodModal-dialogue">
		<div class="modal-content" style="min-width: 200%;">
			<div class="modal-header">
				<h4 class="modal-title">Customers</h4>
			</div>
			<div class="modal-body prod-mod">
				<form action="#">
					<table cellpadding="0" cellspacing="0" border="0"
						class="table  table-bordered erp-tbl proTable"
						id="basic-datatables">
						<thead>
							<tr>
								<th width="20%">Name</th>
								<th width="50%">Address</th>
								<th width="10%">Email</th>
								<th width="5%">Select</th>
							</tr>
						</thead>
						<c:forEach items="${customers }" var="c">
							<tr>
								<td>${c.name }</td>
								<td>${c.address }</td>
								<td>${c.email }</td>
								<td><input type="radio" name="cust"
									value="${c.name }!@#${c.address }!@#${c.email }"></td>
							</tr>
						</c:forEach>
						<tbody>
						</tbody>
					</table>
				</form>
			</div>

			<div class="modal-footer clearfix">
				<div class="btn-toolbar pull-right">
					<button type="button" onclick="selectThisCustomer()"
						class="btn btn-default">Ok</button>
					<button type="button" onclick="cancelPdtpo()"
						class="btn btn-default">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<c:url value='/resources/assets/vijayaPrakash/js/vp.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>