<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="modal prodModal" data-backdrop="static"
	data-keyboard="false" tabindex="-1" aria-hidden="false"
	style="display: block;" role="dialog">
	<div class="prodModal-dialogue">
		<div class="modal-content" style="min-width: 200%;">
			<div class="modal-header">
				<h4 class="modal-title">Products</h4>
			</div>
			<div class="modal-body prod-mod">
				<form action="#">
					<table cellpadding="0" cellspacing="0" border="0"
						class="table  table-bordered erp-tbl proTable"
						id="basic-datatables">
						<thead>
							<tr>
								<th width="20%">Item</th>
								<th width="10%">Rate</th>
								<th width="10%">Quantity</th>
								<!-- <th width="10%">Unit</th> -->
								<th width="10%" style="display: none;">Select</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${products }" var="p">
								<tr class="pdtRow">
									<td>${p.productName }</td>
									<td>${p.rate }</td>
									<td><input type="text" value="${p.availableQty }"
										class="double td_txt form-control"
										onblur="addQtyToId('${p.id }',this.value)" /></td>
									<%-- <td>${p.unit }</td> --%>
									<td style="display: none;"><c:choose>
											<c:when test="${p.selected }">
												<input type="checkbox" checked="checked" class="${p.id }"
													name="selectedProduct"
													value="${p.id }!!!${p.availableQty }" />
											</c:when>
											<c:otherwise>
												<input type="checkbox" class="${p.id }" name="product"
													value="${p.id }" />
											</c:otherwise>
										</c:choose></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</form>
			</div>

			<div class="modal-footer clearfix">
				<div class="btn-toolbar pull-right">
					<button type="button" onclick="closePdtpo()"
						class="btn btn-default">Ok</button>
					<button type="button" onclick="cancelPdtpo()"
						class="btn btn-default">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script src="<c:url value='/resources/assets/vijayaPrakash/js/vp.js' />"></script>