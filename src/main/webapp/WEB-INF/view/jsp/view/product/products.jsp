<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head></head>
<body>
	<div id="load-erp" style="display: block;">
		<div id="spinneeer-erp"></div>
	</div>
	<div class="purchase_home">
		<div class="warper container-fluid">

			<div class="row">


				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">Products</div>
						<div class="panel-body">
							<div id="product_tbl_div">
								<table cellpadding="0" cellspacing="0" border="0"
									class="table table-striped table-bordered erp-tbl"
									id="basic-datatable">
									<thead>
										<tr>
											<th width="5%">#</th>
											<th width="50%">Name</th>
											<th width="20%">Rate</th>
											<!-- <th width="20%">Unit</th> -->
											<th width="5%">Edit</th>
											<th width="5%">Delete</th>

										</tr>
									</thead>
									<tbody>
										<c:forEach items="${products }" var="p" varStatus="s">
											<tr>
												<td>${s.count}</td>
												<td>${p.productName }</td>
												<td>${p.rate }</td>
												<%-- <td>${p.unit }</td> --%>
												<td><a href="editProduct.do?id=${p.id }"><i
														class="fa fa-pencil"></i></a></td>
												<td><a onclick="deleteThisProduct('${p.id }')"><i
														class="fa fa-times"></i></a></td>
											</tr>
										</c:forEach>


									</tbody>
								</table>
							</div>
							<a href="addProduct.do">
								<button type="button" class="btn btn-primary erp-btn">New
									Product</button>
							</a>

						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="confirm-delete" style="display: none;">
			<div class="modal " style="display: block;">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Confirmation</h4>
						</div>
						<div class="modal-body">Do you really want to delete this
							item?</div>
						<input type="hidden" id="id-hid">
						<div class="modal-footer clearfix">
							<div class="btn-toolbar pull-right">
								<input type="hidden" id="delete-item">
								<button type="button" onclick="cancel()" class="btn btn-default">No</button>
								<button type="button" onclick="deleteItem()"
									class="btn btn-primary">Yes</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="success-msg" style="display: none">
			<div class="notification-panl successMsg">
				<div class="notfctn-cntnnt">${success_msg }</div>
				<span id="close-msg"><i class="fa fa-times"></i></span>
			</div>
		</div>
		<c:if test="${! empty msg }">
			<div class="notification-panl successMsg">
				<div class="notfctn-cntnnt">${msg }</div>
				<span class="close-msg"><i class="fa fa-times"></i></span>
			</div>
		</c:if>
		<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
		<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
		<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
		<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
		<script
			src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
		<script
			src="<c:url value='/resources/assets/vijayaPrakash/js/vp.js' />"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				changeDocumentTitle('productsli', 'Products');
				$("#load-erp").hide();
			});
		</script>
</body>
</html>