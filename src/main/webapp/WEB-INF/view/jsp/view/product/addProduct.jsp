<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/validation/formValidation.css'/>" />
</head>
<body>
	<div class="purchase_home">
		<div class="warper container-fluid">

			<div class="row">
				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">
							Product's Information<a href="products.do"><span
								class="back-btn"><i class="fa fa-arrow-left"></i></span></a>
						</div>
						<div class="panel-body">
							<form:form action="saveProduct.do" commandName="productVo"
								id="prdct_frm">
								<form:hidden path="id" />
								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default erp-info">
											<div class="panel-heading panel-inf">Basic Information</div>
											<div class="panel-body">

												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Name<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="productName" class="form-control paste" />
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Rate<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="rate" class="form-control double" />
													</div>
												</div>
												<%-- <div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Unit
													</label>
													<div class="col-sm-9">
														<form:input path="unit" class="form-control " />
													</div>
												</div> --%>
												<div class="form-group">
													<button type="submit" class="btn btn-primary erp-btn">Save</button>
												</div>

												<a href="addProduct.do">
													<button type="button" class="btn btn-primary erp-btn right">New
														Product</button>
												</a>
											</div>
										</div>
									</div>
								</div>
							</form:form>
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/nicescroll/jquery.nicescroll.min.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/assets/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/assets/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/assets/js/validation/bootstrap.js' />"></script>
		<script
		src="<c:url value='/resources/assets/vijayaPrakash/js/vp-validation.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>

	<script
		src="<c:url value='/resources/assets/vijayaPrakash/js/vp.js' />"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			changeDocumentTitle('productsli', 'Add Product');
		});
		</script>
</body>
</html>