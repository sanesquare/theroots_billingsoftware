<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<thead>
	<tr>
		<th width="5%">Sl. No.</th>
		<th width="50%">Product</th>
		<th width="10%">Quantity</th>
		<th width="10%">Service Provider</th>
		<th width="15%">Rate</th>
		<th width="15%">Total</th>
	</tr>
</thead>
<tbody>
	<c:forEach items="${products }" var="p" varStatus="s">
		<tr class="productTableRow" id="${p.id }">
			<td style="padding: .5%;">${s.count}</td>
			<input type="hidden" class="prodctId" value="${p.id }">
			<td style="padding: .5%;"><label id="prod_name${p.id }">${p.productName }</label></td>
			<td style="padding: .5%;"><input type="text"
				value="${p.availableQty }" onblur="calculateProductSum('${p.id}')"
				class="form-control numbersonly" id="product_qty${p.id }" /></td>
			<td style="padding: .5%;"><select
				class="form-control chosen-select" id="prdctSrvcProvider${p.id }"><option
						value="">-Service Provider-</option>
					<c:forEach items="${serviceProviders }" var="mode">
						<option value="${mode.id }">${mode.serviceProvider }</option>
					</c:forEach>
			</select></td>
			<td style="padding: .5%;"><input type="text" value="${p.rate }"
				onblur="calculateProductSum('${p.id}')" class="form-control double "
				id="product_rt${p.id }" /></td>
			<td style="padding: .5%;"><label id="prod_tot${p.id }">${p.total }</label></td>
		</tr>
	</c:forEach>
	<tr>
		<td colspan="7" style="padding: .5%;"><button type="button"
				onclick="getProductDetails()" class="btn btn-primary erp-btn right">Add
				Item</button></td>
	</tr>
</tbody>
<script src="<c:url value='/resources/assets/vijayaPrakash/js/vp.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script type="text/javascript">
	$(document).ready(
			function() {
				$(".prdct_tbl tr").each(
						function() {
							var id = this.id;
							if (id != '') {
								$('#prdctSrvcProvider' + id).chosen().trigger(
										"chosen:updated");
							}
						});
			});
</script>