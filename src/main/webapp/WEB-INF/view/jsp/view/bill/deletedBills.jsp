<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
</head>
<body>
	<div id="load-erp" style="display: block;">
		<div id="spinneeer-erp"></div>
	</div>
	<div class="warper container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default erp-panle">
					<div class="panel-heading  panel-inf">
						Bills<a href="bill.do"><span class="back-btn"><i
								class="fa fa-arrow-left"></i></span></a>
					</div>
					<div class="row">
						<div class="panel-body">
							<%-- <form:form action="billHistoryReport.do" method="GET"
								target="_blank">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<div class="col-sm-4">
												<div class="input-group date" id="datepicker">
													<input type="text" class="form-control" name="fromDate"
														style="background-color: #ffffff !important;"
														readonly="true" id="billFromDate" value="${startDate }"
														data-date-format="DD/MM/YYYY" /> <span
														class="input-group-addon"><span
														class="glyphicon-calendar glyphicon"></span> </span>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="input-group date" id="datepickers">
													<input type="text" class="form-control" name="toDate"
														style="background-color: #ffffff !important;"
														readonly="true" id="billToDate" value="${endDate }"
														data-date-format="DD/MM/YYYY" /> <span
														class="input-group-addon"><span
														class="glyphicon-calendar glyphicon"></span> </span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-9">
										<button type="button" id="attndnce_add_btn"
											onclick="filterBill()" class="btn btn-info">Filter</button>
									</div>
								</div>
								<div class="form-group">
									<button type="submit" id="" name="format" value="xls"
										class="btn btn-info">Excel Report</button>
									<button type="submit" id="" name="format" value="pdf"
										class="btn btn-info">PDF Report</button>
								</div>
							</form:form> --%>
						</div>
					</div>
					<div class="row">
						<div class="panel-body">
							<div class="col-md-12">
								<div id="product_tbl_div" style="overflow-x: scroll;">
									<c:if test="${! empty msg }">
										<div class="notification-panl successMsg">
											<div class="notfctn-cntnnt">${msg }</div>
											<span class="close-msg"><i class="fa fa-times"></i></span>
										</div>
									</c:if>
									<table cellpadding="0" cellspacing="0" border="0"
										class="table table-striped table-bordered erp-tbl"
										id="basic-datatable">
										<thead>
											<tr>
												<th width="10%">Ref No</th>
												<th width="20%">Customer</th>
												<th width="10%">Mobile</th>
												<th width="10%">Date</th>
												<th width="15%">Total</th>
												<th width="15%">Discount</th>
												<th width="15%">Taxable Value</th>
												<th width="15%">Tax</th>
												<th width="15%">Net</th>
												<th width="15%">Service Provider's Contribution</th>
												<th width="10%">Payment Mode</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${bills }" var="bill" varStatus="s">
												<tr>
													<c:choose>
														<c:when test="${bill.isPrinted }">
															<td><a href="#"
																onclick="editThisBill('bill.do?id=${bill.id}','edit')">${bill.referenceNumber }</a></td>
															<td><a href="#"
																onclick="editThisBill('bill.do?id=${bill.id}','edit')">${bill.customerName }</a></td>
															<td><a href="#"
																onclick="editThisBill('bill.do?id=${bill.id}','edit')">${bill.mobile }</a></td>
															<td><a href="#"
																onclick="editThisBill('bill.do?id=${bill.id}','edit')">${bill.date }</a></td>
															<td><a href="#"
																onclick="editThisBill('bill.do?id=${bill.id}','edit')"
																style="float: right;">${bill.grandTotal }</a></td>
															<td><a href="#"
																onclick="editThisBill('bill.do?id=${bill.id}','edit')"
																style="float: right;"> ${bill.discountAmount }</a></td>
															<td><a href="#"
																onclick="editThisBill('bill.do?id=${bill.id}','edit')"
																style="float: right;">${bill.taxableValue }</a></td>
															<td><a href="#"
																onclick="editThisBill('bill.do?id=${bill.id}','edit')"
																style="float: right;">${bill.taxTotal }</a></td>
															<td><a href="#"
																onclick="editThisBill('bill.do?id=${bill.id}','edit')"
																style="float: right;">${bill.netTotal }</a></td>
															<td><a href="#"
																onclick="editThisBill('bill.do?id=${bill.id}','edit')"
																style="float: right;">${bill.serviceProviderContribution }</a></td>
															<td><a href="#"
																onclick="editThisBill('bill.do?id=${bill.id}','edit')">${bill.paymentMode }</a></td>
														</c:when>
														<c:otherwise>
															<td><a href="bill.do?id=${bill.id}">${bill.referenceNumber }</a></td>
															<td><a href="bill.do?id=${bill.id}">${bill.customerName }</a></td>
															<td><a href="bill.do?id=${bill.id}">${bill.mobile }</a></td>
															<td><a href="bill.do?id=${bill.id}">${bill.date }</a></td>
															<td><a href="bill.do?id=${bill.id}"
																style="float: right;">${bill.grandTotal }</a></td>
															<td><a href="bill.do?id=${bill.id}"
																style="float: right;"> ${bill.discountAmount }</a></td>
															<td><a href="bill.do?id=${bill.id}"
																style="float: right;">${bill.taxableValue }</a></td>
															<td><a href="bill.do?id=${bill.id}"
																style="float: right;">${bill.taxTotal }</a></td>
															<td><a href="bill.do?id=${bill.id}"
																style="float: right;">${bill.netTotal }</a></td>
															<td><a href="bill.do?id=${bill.id}"
																style="float: right;">${bill.serviceProviderContribution }</a></td>
															<td><a href="bill.do?id=${bill.id}">${bill.paymentMode }</a></td>
														</c:otherwise>
													</c:choose>
												</tr>
											</c:forEach>
										</tbody>
										<tfoot>
											<tr>
												<td colspan="4"></td>
												<td><b style="float: right;">${total }</b></td>
												<td><b style="float: right;">${discoundTotal }</b></td>
												<td><b style="float: right;">${taxableTotal }</b></td>
												<td><b style="float: right;">${taxTotal }</b></td>
												<td><b style="float: right;">${netTotal }</b></td>
												<td><b style="float: right;">${serviceProviderContributionTotal }</b></td>
												<td colspan="1"></td>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="confirm-delete" style="display: none;">
		<div class="modal " style="display: block;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Confirmation</h4>
					</div>
					<div class="modal-body">Do you really want to delete this
						item?</div>
					<input type="hidden" id="id-hid">
					<div class="modal-footer clearfix">
						<div class="btn-toolbar pull-right">
							<input type="hidden" id="delete-item">
							<button type="button" onclick="cancel()" class="btn btn-default">No</button>
							<button type="button" onclick="deleteItem()"
								class="btn btn-primary">Yes</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="confirm-edit" style="display: none;">
		<div class="modal " style="display: block;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Password</h4>
					</div>
					<div class="modal-body">
						<input type="password" id="pswd" class="form-control"
							placeholder="Enter password" autocomplete="off">
					</div>
					<input type="hidden" id="url_hid">
					<div class="modal-footer clearfix">
						<div class="btn-toolbar pull-left">
							<label style="color: red; font-size: 16px; display: none;"
								id="invldPwd">Invalid Password</label>
						</div>
						<div class="btn-toolbar pull-right">
							<input type="hidden" id="ediType">
							<button type="button" onclick="cancelEdit()"
								class="btn btn-default">No</button>
							<button type="button" onclick="editThisItem()"
								class="btn btn-primary">Yes</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="success-msg" style="display: none">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${success_msg }</div>
			<span id="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</div>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/vijayaPrakash/js/vp.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/vijayaPrakash/js/vp.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			changeDocumentTitle('deleteli	','Deleted Bills');
			$("#load-erp").hide();
		});
		$(window).focus(function() {
			$("#load-erp").hide();
		});
	</script>
</html>