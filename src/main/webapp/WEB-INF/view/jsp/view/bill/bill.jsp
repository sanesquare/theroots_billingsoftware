<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/css/validation/formValidation.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/plugins/bootstrap-chosen/chosen.css'/>" />
</head>
<body>
	<div class="purchase_home">
		<div class="warper container-fluid">
			<div class="row">
				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">
							<c:choose>
								<c:when test="${empty quotation.referenceNumber }">Invoice</c:when>
								<c:otherwise>
									Invoice 
											<input type="text" value="${quotation.referenceNumber }"
										style="text-align: right;" class="numbersonly"
										id="referenceNumber">
								</c:otherwise>
							</c:choose>
							<a href="billHistory.do"><span class="back-btn"><label
									style="cursor: pointer;">History &nbsp;</label><i
									class="fa fa-arrow-right"></i></span></a>
						</div>
						<div class="panel-body">
							<form:form action="#" commandName="quotation">
								<form:hidden path="referenceNumber" id="" />
								<div class="row">
									<input type="hidden" value="${quotation.id }" id="quotationId">
									<div class="panel-body">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Customer<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9" id="custSelectDiv">
														<select class="form-control chosen-select" id="billCust">
															<option value="">-Customer-</option>
															<c:forEach items="${customers }" var="cust">
																<c:choose>
																	<c:when test="${cust.id eq quotation.customerId}">
																		<option value="${cust.id }" selected="selected">${cust.name }
																			- ${cust.mobile }</option>
																	</c:when>
																	<c:otherwise>
																		<option value="${cust.id }">${cust.name }-
																			${cust.mobile }</option>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
															<option value="new"
																style="background-color: rgba(198, 218, 177, 0.98);">New
																Customer</option>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Date<span
														class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<div class='input-group date billDate' id="datepicker">
															<input type="text" readonly="true" name="periodStarts"
																id="quot-date" value="${quotation.date }"
																style="background-color: #ffffff !important;"
																data-date-format="DD/MM/YYYY" class="form-control" /> <span
																class="input-group-addon"><span
																class="glyphicon glyphicon-calendar"></span> </span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Customer
														Name<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<input type="text" id="custName"
															value="${quotation.customerName }" class="form-control">
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Mobile
													</label>
													<div class="col-sm-9">
														<input type="number" id="custMobile"
															value="${quotation.mobile }" class="form-control">
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Payment
														Mode<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:select path="paymentModeId" cssClass="form-control"
															id="pymntMode">
															<form:option value="">-Select Payment Mode-</form:option>
															<c:forEach items="${paymentModes }" var="mode">
																<form:option value="${mode.id }">${mode.paymentMode }</form:option>
															</c:forEach>
														</form:select>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="panel-body">
										<div class="col-sm-12">
											<table cellpadding="0" cellspacing="0" border="0"
												class="table table-striped table-bordered erp-tbl prdct_tbl">
												<thead>
													<tr>
														<th width="5%">Sl. No.</th>
														<th width="40%">Product</th>
														<th width="10%">Quantity</th>
														<th width="20%">Service Provider</th>
														<th width="15%">Rate</th>
														<th width="15%">Total</th>
													</tr>
												</thead>
												<tbody>
													<c:forEach items="${quotation.itemVos }" var="p"
														varStatus="s">
														<tr class="productTableRow" id="${p.productId }">
															<td style="padding: .5%;">${s.count}</td>
															<input type="hidden" class="prodctId"
																value="${p.productId }">
															<td style="padding: .5%;"><label
																id="prod_name${p.productId }">${p.productName }</label></td>
															<td style="padding: .5%;"><input type="text"
																value="${p.quantity }"
																onblur="calculateProductSum('${p.productId}')"
																class="form-control numbersonly"
																id="product_qty${p.productId }" /></td>
															<td style="padding: .5%;"><select
																class="form-control chosen-select"
																id="prdctSrvcProvider${p.productId }">
																	<option value="">-Service Provider-</option>
																	<c:forEach items="${serviceProviders }" var="mode">
																		<c:choose>
																			<c:when test="${mode.id eq p.serviceProviderId}">
																				<option value="${mode.id }" selected="selected">${mode.serviceProvider }</option>
																			</c:when>
																			<c:otherwise>
																				<option value="${mode.id }">${mode.serviceProvider }</option>
																			</c:otherwise>
																		</c:choose>
																	</c:forEach>
															</select></td>
															<td style="padding: .5%;"><input type="text"
																value="${p.rate }"
																onblur="calculateProductSum('${p.productId}')"
																class="form-control double "
																id="product_rt${p.productId }" /></td>
															<td style="padding: .5%;"><label
																id="prod_tot${p.productId }">${p.total }</label></td>
														</tr>
													</c:forEach>
													<tr>
														<td colspan="7"><c:if
																test="${! quotation.isDeleted }">
																<button type="button" onclick="getProductDetails()"
																	class="btn btn-primary erp-btn right">Add
																	Product</button>
															</c:if></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6 "></div>
									<div class="col-md-6 ">
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">
												Total </label>
											<div class="col-sm-9">
												<label id="grandTotal">${quotation.grandTotal }</label>
											</div>
										</div>
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">
												Discount Percentage(%)</label>
											<div class="col-sm-9">
												<form:input path="discountPercentage"
													onblur="calculateDiscount()" id="discountPercentage"
													cssClass="form-control double" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">
												Discount Amount</label>
											<div class="col-sm-9">
												<form:input path="discountAmount" onblur="calculateTax()"
													id="discountAmount" cssClass="form-control double" />
											</div>
										</div>
										<div class="form-group">
											<label for="inputPassword3" class="col-sm-3 control-label">
												Tax </label>
											<div class="col-sm-9">
												<form:select class="form-control chosen-select"
													id="taxSelect" path="taxids" onchange="calculateTax()"
													multiple="multiple" data-placeholder="--Select Tax--">
													<c:forEach items="${taxVos }" var="tax">
														<form:option value="${tax.id}">${tax.tax }</form:option>
													</c:forEach>
												</form:select>
											</div>
										</div>
										<div id="taxSection">
											<c:if test="${!empty quotation.taxVos }">
												<div class="form-group">
													<table cellpadding="0" cellspacing="0" border="0"
														class="table table-striped table-bordered erp-tbl tax_tbl">
														<thead>
															<tr>
																<th width="20%">Tax</th>
																<th width="20%">Percentage</th>
																<th width="30%">Amount</th>
															</tr>
														</thead>
														<tbody>
															<c:forEach items="${quotation.taxVos }" var="tax">
																<tr id="${tax.taxId }">
																	<td><input type="hidden" value="${tax.taxId }"
																		id="">${tax.tax }</td>
																	<td>${tax.percentage }</td>
																	<td><input type="text" class="form-control double"
																		onblur="findTaxSum()" value="${tax.amount }"
																		id="tax_amnt${tax.taxId }"></td>
																</tr>
															</c:forEach>
														</tbody>
													</table>
												</div>
											</c:if>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">
													Tax Total </label>
												<div class="col-sm-9">
													<label id="taxTotal">${quotation.taxTotal }</label>
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">
													Net Total </label>
												<div class="col-sm-9">
													<label id="netTotal">${quotation.netTotal }</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<c:if test="${! quotation.isDeleted }">
									<div class="row">
										<div class="col-md-6">
											<button type="button" onclick="saveQuotation('true')"
												class="btn btn-primary erp-btn ">
												<i style="font-size: 20px;" class="fa fa-print"></i>&nbsp;&nbsp;Save&Print
											</button>
											<button type="button" onclick="saveQuotation('false')"
												class="btn btn-primary erp-btn ">
												<i style="font-size: 20px;" class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save
											</button>
										</div>
									</div>
								</c:if>
							</form:form>
							<div>
								<c:if test="${! quotation.isDeleted }">
									<a href="bill.do"><button type="button"
											class="btn btn-primary erp-btn ">New</button></a>
								</c:if>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="errror-msg" style="display: none;">
		<div class="modal " data-backdrop="static" data-keyboard="false"
			tabindex="-1" aria-hidden="false" style="display: block;"
			role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Alert</h4>
					</div>
					<div class="modal-body"></div>
					<input type="hidden" id="id-hid">
					<div class="modal-footer clearfix">
						<div class="btn-toolbar pull-right">
							<button type="button" onclick="closeError()"
								class="btn btn-default">ok</button>
							<!-- <button type="button" onclick="deleteJournal('true')"
								class="btn btn-primary">Yes</button> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="notification-panl successMsg" style="display: none">
		<div class="notfctn-cntnnt"></div>
		<span id="close-msg"><i class="fa fa-times"></i></span>
	</div>
	<div id="load-erp">
		<div id="spinneeer-erp"></div>
	</div>
	<c:if test="${! empty msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${msg }</div>
			<span class="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>
	<div id="pdtPop" style="display: none;"></div>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script
		src="<c:url value='/resources/assets/vijayaPrakash/js/vp.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/bootstrap/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/js/validation/bootstrap.js' />"></script>
	<script src="<c:url value='/resources/assets/js/moment/moment.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js' />"></script>
	<script
		src="<c:url value='/resources/assets/js/plugins/bootstrap-chosen/chosen.jquery.js' />"></script>
	<c:if test="${! quotation.isDeleted }">
		<script type="text/javascript">
			$(document).ready(function() {
				changeDocumentTitle('billL	','Bill');
			});
			$(window)
					.focus(
							function() {
								if ($("#quotationId").val() != '') {
									window.history
											.pushState(
													{
														path : "bill.do?msg=Your bill has been saved Successfully"
													}, '',
													"bill.do?msg=Your bill has been saved Successfully");
									location.reload();
								}
							});
		</script>
	</c:if>
</body>
</html>