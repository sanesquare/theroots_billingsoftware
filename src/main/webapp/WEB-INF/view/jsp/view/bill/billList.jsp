<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:if test="${! empty msg }">
	<div class="notification-panl successMsg">
		<div class="notfctn-cntnnt">${msg }</div>
		<span class="close-msg"><i class="fa fa-times"></i></span>
	</div>
</c:if>
<table cellpadding="0" cellspacing="0" border="0"
	class="table table-striped table-bordered erp-tbl" id="basic-datatable">
	<thead>
		<tr>
			<th width="10%">Ref No</th>
			<th width="20%">Customer</th>
			<th width="10%">Mobile</th>
			<th width="10%">Date</th>
			<th width="15%">Total</th>
			<th width="15%">Discount</th>
			<th width="15%">Taxable Value</th>
			<th width="15%">Tax</th>
			<th width="15%">Net</th>
			<th width="15%">Service Provider's Contribution</th>
			<th width="10%">Payment Mode</th>
			<th width="5%">Print</th>
			<th width="5%">Delete</th>

		</tr>
	</thead>
	<tbody>
		<c:forEach items="${bills }" var="bill" varStatus="s">
			<tr>
				<c:choose>
					<c:when test="${bill.isPrinted }">
						<td><a href="#"
							onclick="editThisBill('bill.do?id=${bill.id}','edit')">${bill.referenceNumber }</a></td>
						<td><a href="#"
							onclick="editThisBill('bill.do?id=${bill.id}','edit')">${bill.customerName }</a></td>
						<td><a href="#"
							onclick="editThisBill('bill.do?id=${bill.id}','edit')">${bill.mobile }</a></td>
						<td><a href="#"
							onclick="editThisBill('bill.do?id=${bill.id}','edit')">${bill.date }</a></td>
						<td><a href="#"
							onclick="editThisBill('bill.do?id=${bill.id}','edit')"
							style="float: right;">${bill.grandTotal }</a></td>
						<td><a href="#"
							onclick="editThisBill('bill.do?id=${bill.id}','edit')"
							style="float: right;"> ${bill.discountAmount }</a></td>
						<td><a href="#"
							onclick="editThisBill('bill.do?id=${bill.id}','edit')"
							style="float: right;">${bill.taxableValue }</a></td>
						<td><a href="#"
							onclick="editThisBill('bill.do?id=${bill.id}','edit')"
							style="float: right;">${bill.taxTotal }</a></td>
						<td><a href="#"
							onclick="editThisBill('bill.do?id=${bill.id}','edit')"
							style="float: right;">${bill.netTotal }</a></td>
						<td><a href="#"
							onclick="editThisBill('bill.do?id=${bill.id}','edit')"
							style="float: right;">${bill.serviceProviderContribution }</a></td>
						<td><a href="#"
							onclick="editThisBill('bill.do?id=${bill.id}','edit')">${bill.paymentMode }</a></td>
						<td><i class="fa fa-print"
							onclick="printThisBill('${bill.id}')"></i></td>
					</c:when>
					<c:otherwise>
						<td><a href="bill.do?id=${bill.id}">${bill.referenceNumber }</a></td>
						<td><a href="bill.do?id=${bill.id}">${bill.customerName }</a></td>
						<td><a href="bill.do?id=${bill.id}">${bill.mobile }</a></td>
						<td><a href="bill.do?id=${bill.id}">${bill.date }</a></td>
						<td><a href="bill.do?id=${bill.id}" style="float: right;">${bill.grandTotal }</a></td>
						<td><a href="bill.do?id=${bill.id}" style="float: right;">
								${bill.discountAmount }</a></td>
						<td><a href="bill.do?id=${bill.id}" style="float: right;">${bill.taxableValue }</a></td>
						<td><a href="bill.do?id=${bill.id}" style="float: right;">${bill.taxTotal }</a></td>
						<td><a href="bill.do?id=${bill.id}" style="float: right;">${bill.netTotal }</a></td>
						<td><a href="bill.do?id=${bill.id}" style="float: right;">${bill.serviceProviderContribution }</a></td>
						<td><a href="bill.do?id=${bill.id}">${bill.paymentMode }</a></td>
						<td><i class="fa fa-print"
							onclick="printThisBill('${bill.id}')"></i></td>
					</c:otherwise>
				</c:choose>
				<td><a onclick="editThisBill('${bill.id}','delete')"><i
						class="fa fa-times"></i></a></td>
			</tr>
		</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4"></td>
			<td><b style="float: right;">${total }</b></td>
			<td><b style="float: right;">${discoundTotal }</b></td>
			<td><b style="float: right;">${taxableTotal }</b></td>
			<td><b style="float: right;">${taxTotal }</b></td>
			<td><b style="float: right;">${netTotal }</b></td>
			<td><b style="float: right;">${serviceProviderContributionTotal }</b></td>
			<td colspan="3"></td>
		</tr>
	</tfoot>
</table>

<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/DT_bootstrap.js' />"></script>
<script
	src="<c:url value='/resources/assets/js/plugins/datatables/jquery.dataTables-conf.js' />"></script>
<script src="<c:url value='/resources/assets/vijayaPrakash/js/vp.js' />"></script>