<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:if test="${!empty taxVos }">
	<div class="form-group">
		<table cellpadding="0" cellspacing="0" border="0"
			class="table table-striped table-bordered erp-tbl tax_tbl">
			<thead>
				<tr>
					<th width="20%">Tax</th>
					<th width="20%">Percentage</th>
					<th width="30%">Amount</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${taxVos }" var="tax">
					<tr id="${tax.taxId }">
						<td><input type="hidden" value="${tax.taxId }" id="">${tax.tax }</td>
						<td>${tax.percentage }</td>
						<td><input type="text" class="form-control double" onblur="findTaxSum()"
							value="${tax.amount }" id="tax_amnt${tax.taxId }"></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</c:if>
<div class="form-group">
	<label for="inputPassword3" class="col-sm-3 control-label"> Tax
		Total </label>
	<div class="col-sm-9">
		<label id="taxTotal">${taxTotal}</label>
	</div>
</div>
<div class="form-group">
	<label for="inputPassword3" class="col-sm-3 control-label"> Net
		Total </label>
	<div class="col-sm-9">
		<label id="netTotal">${netTotal}</label>
	</div>
</div>
<script src="<c:url value='/resources/assets/vijayaPrakash/js/vp.js' />"></script>