<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="modal prodModal" data-backdrop="static"
	data-keyboard="false" tabindex="-1" aria-hidden="false"
	style="display: block;" role="dialog">
	<div class="prodModal-dialogue">
		<div class="modal-content" style="min-width: 200%;">
			<div class="modal-header">
				<h4 class="modal-title">Customer</h4>
			</div>
			<div class="modal-body prod-mod">
				<form action="#">
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Name<span
							class="stars">*</span>
						</label>
						<div class="col-sm-9">
							<input type="hidden" id="customerId"> <input type="text"
								class="form-control" id="customerName">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Address
						</label>
						<div class="col-sm-9">
							<input type="text" id="custAddress" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Mobile<span
							class="stars">*</span>
						</label>
						<div class="col-sm-9">
							<input type="text" class="form-control numbersonly"
								id="customerMobile">
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Email
						</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="customerEmail">
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer clearfix">
				<div class="btn-toolbar pull-right">
					<button type="button" onclick="saveCustomerFromPop()"
						class="btn btn-primary erp-btn">Save</button>

					<button type="button" onclick="javascript:$('#pdtPop').hide();"
						class="btn btn-primary erp-btn right">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<c:url value='/resources/assets/vijayaPrakash/js/vp.js' />"></script>