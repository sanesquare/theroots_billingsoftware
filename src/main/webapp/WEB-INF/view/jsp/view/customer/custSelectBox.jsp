<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<select class="form-control chosen-select" id="billCust">
	<option value="">-Customer-</option>
	<c:forEach items="${customers }" var="cust">
		<c:choose>
			<c:when test="${cust.id eq id}">
				<option value="${cust.id }" selected="selected">${cust.name }</option>
			</c:when>
			<c:otherwise>
				<option value="${cust.id }">${cust.name }</option>
			</c:otherwise>
		</c:choose>
	</c:forEach>
	<option value="new"
		style="background-color: rgba(198, 218, 177, 0.98);">New
		Customer</option>
</select>
<script src="<c:url value='/resources/assets/vijayaPrakash/js/vp.js' />"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#billCust').chosen().trigger("chosen:updated");
	});
</script>