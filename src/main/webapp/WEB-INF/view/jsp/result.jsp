<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>Karuna Lab</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" />
<link href='<c:url value="/resources/asset/css/bootstrap.css"></c:url>'
	rel='stylesheet' type='text/css' />
<link href='<c:url value="/resources/asset/css/style.css"></c:url>'
	rel="stylesheet" type="text/css" media="all" />
<link
	href='<c:url value="/resources/asset/css/footable.core.css"></c:url>'
	rel="stylesheet" type="text/css">
<link
	href='<c:url value="/resources/asset/css/footable-demos.css"></c:url>'
	rel="stylesheet" type="text/css">
<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900'
	rel='stylesheet' type='text/css'>
<body>
	<div class="header text-center">
		<div class="container">
			<div class="logo">
				<a href="signout"><span><img
						src='<c:url value="/resources/asset/images/logo.png"></c:url>' /></span></a>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="navigation-strip effect2">
			<div class="navigation">
				<div class="top-menu">
					<span class="menu"> </span>
					<ul class="hd">Results </ul>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="content">
			<div class="banner text-center">
				<a class="log_out" href="signout"><input type="button"  id="logOut" value="Log Out" /></a>
				<div class="banner-head">
					<div class="row">
						<div class="col-md-12">
							<table class="table demo">
								<thead>
									<tr>
										<th data-toggle="true">#</th>
										<th>Test</th>
										<th>Date of Test</th>
										<th>Date of Report</th>
										<th>Download</th>
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${empty customerVo.reportUrl }">
											<tr>
											<td></td>
											<td>No reports to download </td>
											<td></td>
											<td></td>
											<td></td>
											</tr>
										</c:when>
										<c:otherwise>
											<tr>
											<td>1</td>
											<td>${customerVo.receiptCode }</td>
											<td>${customerVo.testDate }</td>
											<td>${customerVo.reportDate }</td>
											<td><a href="downloadReport?rurl=${ customerVo.reportUrl}"><img src='<c:url value="/resources/asset/images/download.png"></c:url>' /></a></td>
									</tr>
										</c:otherwise>
									</c:choose>
									
									<%-- </c:forEach> --%>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="5"><div
												class="pagination pagination-centered"></div></td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="contact-section">
			<div class="contact-section-head text-center"></div>
		</div>
	</div>
	<div class="footer effect2">
		<div class="footer-bg">
			<div class="copyright text-center">
				<p>Copyright &copy; 2015 All rights reserved |</p>
			</div>
		</div>
	</div>
	</div>
	<script src='<c:url value="/resources/asset/js/jquery.min.js"></c:url>'></script>
	<script src='<c:url value="/resources/asset/js/footable.js"></c:url>'
		type="text/javascript"></script>
	<script
		src='<c:url value="/resources/asset/js/footable.paginate.js"></c:url>'
		type="text/javascript"></script>
	<script src='<c:url value="/resources/asset/js/js/demos.js"></c:url>'
		type="text/javascript"></script>
	<script type="text/javascript">
		$(function() {
			$('table').footable();

			$('.clear-filter').click(function(e) {
				e.preventDefault();
				$('table.demo').trigger('footable_clear_filter');
				$('.filter-status').val('');
			});

			$('.filter-status').change(function(e) {
				e.preventDefault();
				var filter = $(this).val();
				$('#filter').val($(this).text());
				$('table.demo').trigger('footable_filter', {
					filter : filter
				});
			});
		});
	</script>
</body>
</html>