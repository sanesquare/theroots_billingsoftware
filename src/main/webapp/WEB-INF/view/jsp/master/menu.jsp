<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>

</head>
<body>
	<aside class="left-panel ">

		<a href="homePage.do">
			<div class="user text-center">
				<img src="resources/assets/images/snogol.png" class="img-circle">
			</div>
		</a>

		<nav class="navigation">
			<ul class="list-unstyled">
				<li id="dashbordli"><a href="homePage.do" title="Home"><i
						class="fa fa-home"></i><span class="nav-label">Home</span></a></li>
				<li id="billL" class="has-submenu"><a href="bill.do"
					title="Bill"><i class="fa fa-money"></i> <span
						class="nav-label">Bill</span></a></li>
				<%-- <c:if test="${username eq 'Anand' }"> --%>
					<li id="deleteli" class="has-submenu"><a
						href="deletedBills.do" title="Bill"><i class="fa fa-money"></i>
							<span class="nav-label">Deleted Bills</span></a></li>
				<%-- </c:if> --%>
				<li id="productsli" class="has-submenu"><a
					href="products.do" title="Products"><i class="fa fa-cubes"></i><span
						class="nav-label">Products</span></a></li>
				<li id="taxli" class="has-submenu"><a href="tax.do"
					title="Tax"><i class="fa fa-dollar"></i><span class="nav-label">Tax</span></a></li>
				<li id="customerLi" class="has-submenu"><a
					href="customer.do" title="Customer"><i class="fa fa-users"></i>
						<span class="nav-label">Customer</span></a></li>
				<li id="service" class="has-submenu"><a
					href="serviceProvider.do" title="Service Provider"><i
						class="fa fa-users"></i> <span class="nav-label">Service
							Provider</span></a></li>
				<!-- 	<li id="organzationli" class="has-submenu"><a
					href="smsHome.do" title="Send SMS"><i class="fa fa-envelope"></i>
						<span class="nav-label">SMS</span></a></li> -->
				<li id="settings" class="has-submenu"><a
					href="settings.do" title="Settings"><i class="fa fa-cogs"></i>
						<span class="nav-label">Settings</span></a></li>
			</ul>
		</nav>
	</aside>
	<!-- Aside Ends-->
</body>
</html>