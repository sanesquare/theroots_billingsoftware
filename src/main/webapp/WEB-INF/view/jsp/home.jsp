<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>Karuna Lab</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" />
<link href='<c:url value="/resources/asset/css/bootstrap.css"></c:url>'
	rel='stylesheet' type='text/css' />
<link href='<c:url value="/resources/asset/css/style.css"></c:url>'
	rel="stylesheet" type="text/css" media="all" />

<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900'
	rel='stylesheet' type='text/css'>
<body>

	<div class="header text-center">
		<div class="container">
			<div class="logo">
				<a href="signout"><span><img
						src='<c:url value="/resources/asset/images/logo.png"></c:url>' /></span></a>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="navigation-strip effect2">
			<div class="navigation">
				<div class="top-menu">
					<span class="menu"> </span>
					<ul class="hd">Patient Details
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="content">
			<div class="banner text-center">
				<a class="log_out" href="signout"><input type="button"
					id="logOut" value="Log Out" /></a>
				<div class="banner-head">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-4 control-label">Receipt
									Number </label>
								<div class="col-sm-8">${customerVo.receiptCode }</div>
							</div>
							<!-- <div class="form-group">
								<label for="inputPassword3" class="col-sm-4 control-label">Patient
									Id </label>
								<div class="col-sm-8"></div>
							</div> -->
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-4 control-label">Name
								</label>
								<div class="col-sm-8">${customerVo.name }</div>
							</div>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-4 control-label">Mobile</label>
								<div class="col-sm-8">${customerVo.mobile }</div>
							</div>


							<!-- <div class="form-group">
								<label for="inputPassword3" class="col-sm-4 control-label">Lab
									Number </label>
								<div class="col-sm-8"></div>
							</div> -->



							<!-- <div class="form-group">
								<label for="inputPassword3" class="col-sm-4 control-label">Last
									Name </label>
								<div class="col-sm-8"></div>
							</div> -->


							<div class="form-group">
								<label for="inputPassword3" class="col-sm-4 control-label">Address
								</label>
								<div class="col-sm-8">${customerVo.address }</div>
							</div>

						</div>



						<div class="col-md-6">


							<div class="form-group">
								<label for="inputPassword3" class="col-sm-4 control-label">Test
									Date </label>
								<div class="col-sm-8">${customerVo.testDate }</div>
							</div>
							<!-- <div class="form-group">
								<label for="inputPassword3" class="col-sm-4 control-label">Residence
								</label>
								<div class="col-sm-8"></div>
							</div> -->


							<!-- <div class="form-group">
								<label for="inputPassword3" class="col-sm-4 control-label">E-mail
								</label>
								<div class="col-sm-8"></div>
							</div> -->


							<div class="form-group">
								<label for="inputPassword3" class="col-sm-4 control-label">Reference
									Consultant </label>
								<div class="col-sm-8">${customerVo.referenceDr }</div>
							</div>

							<div class="form-group">
								<label for="inputPassword3" class="col-sm-4 control-label">Test</label>
								<div class="col-sm-8">
									<c:forEach items="${customerVo.tests }" var="test">
										${test } ,
									</c:forEach>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="contact-section">
				<div class="contact-section-head text-center">
					<h4>
						<a href="resultsPage">Result</a>
					</h4>
				</div>
			</div>
		</div>
		<div class="footer effect2">
			<div class="footer-bg">
				<div class="copyright text-center">
					<p>Copyright &copy; 2015 All rights reserved</p>
				</div>
			</div>
		</div>
	</div>
	<script src='<c:url value="/resources/asset/js/jquery.min.js"></c:url>'></script>
	<script
		src="<c:url value='/resources/assets/vijayaPrakash/js/vp.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			changeDocumentTitle('dashbordli', 'Home');
		});
		function logOut() {
			alert("Inside function");

		}
	</script>
</body>
</html>